import java.util.LinkedHashSet;
import java.util.List;

public class Engineer extends Private{

    private Corps corps;
    private LinkedHashSet<Repair> currentRepairs;
    public Engineer(int id, String firstName, String lastName, double salary, Corps corps, LinkedHashSet<Repair> repairs) {
        super(id, firstName, lastName, salary);
        this.corps = corps;
        this.currentRepairs = repairs;
    }

    //Name: Peter Penchev Id: 7 Salary: 12.23
    //Corps: Marines
    //Repairs:
    //  Part Name: Boat Hours Worked: 2
    //  Part Name: Crane Hours Worked: 17
        //o	public void addRepair(Repair repair)
    //o	public Collection<Repair> getRepairs()
    public void addRepair(Repair repair){
        currentRepairs.add(repair);
    }

    public LinkedHashSet<Repair> getRepairs(){
        return currentRepairs;
    }

    @Override
    public String toString() {
        String formatedSalary = String.format("%.2f", salary());
        StringBuilder builder = new StringBuilder();
        builder.append("Name: ").append(firstName()).append(" ").append(lastName()).append(" Id: ").append(id())
                .append(" Salary: ").append(formatedSalary).append(System.lineSeparator())
                .append("Corps: ").append(corps.getValue()).append(System.lineSeparator())
                .append("Repairs:");
        boolean first = true;
        int size =  getSizeWithoutRepeats();
        String keepCurrentName = "";

        for (Repair repair : currentRepairs) {
            if(first){
                builder.append(System.lineSeparator());
                first = false;
            }
            String repairCurrentName = repair.getPartName();
            if(!repairCurrentName.equals(keepCurrentName)){
                if(size-->1){
                    builder.append(" ").append(repair.toString()).append(System.lineSeparator());
                }else{
                    builder.append(" ").append(repair.toString());
                }
                keepCurrentName = repairCurrentName;
            }


        }



        return builder.toString();
    }

    private int getSizeWithoutRepeats() {
        int count = currentRepairs.size();
        String keepCurrentName = "";
        boolean first = true;
        for (Repair currentRepair : currentRepairs) {
            if(first){
                keepCurrentName = currentRepair.getPartName();
                first = false;
                continue;
            }
            if(currentRepair.getPartName().equals(keepCurrentName)){
                count--;
                keepCurrentName = currentRepair.getPartName();
            }

        }

            return count;
        }

}

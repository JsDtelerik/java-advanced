import java.util.LinkedHashSet;
import java.util.TreeSet;

public interface LieutenantGeneralImpl {

    LinkedHashSet<Soldier> privates = new LinkedHashSet<>();

     void addPrivate(Soldier priv);
}

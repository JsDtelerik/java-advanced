package aquarium.entities.decorations;

public class Plant extends BaseDecoration{

    private static final int PLANT_COMFORT_VALUE = 5;
    private static final double PLANT_PRICE_VALUE = 10;
    public Plant() {
        super(PLANT_COMFORT_VALUE, PLANT_PRICE_VALUE);
    }
}

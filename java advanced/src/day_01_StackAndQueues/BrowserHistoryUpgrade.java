package day_01_StackAndQueues;

import java.util.ArrayDeque;
import java.util.Scanner;

public class BrowserHistoryUpgrade {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String command = scan.nextLine();

        ArrayDeque<String> browserHistory = new ArrayDeque<>();
        ArrayDeque<String> forwardPages = new ArrayDeque<>();
        String currentPage = "";
        while (!command.equals("Home")){
            if(!command.equals("back") && !command.equals("forward")){
                browserHistory.push(command);
                forwardPages.clear();
                System.out.println(command);

            }else if(command.equals("back")){
                if(browserHistory.size()>1){
                   currentPage = browserHistory.pop();
                   forwardPages.addFirst(currentPage);
                    System.out.println(browserHistory.peek());
                }else{
                    System.out.println("no previous URLs");
                }
            }else if (command.equals("forward")){
                if(!forwardPages.isEmpty()){
                    currentPage = forwardPages.poll();
                    System.out.println(currentPage);
                    browserHistory.push(currentPage);
                }else{
                    System.out.println("no next URLs");
                }
            }

            command = scan.nextLine();
        }
    }
}

package day_04_Multidimensional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ParkingSystem {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] rowAndCols = Arrays.stream(scan.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();
        int row = rowAndCols[0];
        int col = rowAndCols[1];

        List<List<String>> parkingLot = createParking(row, col);

        String[] command = scan.nextLine().split("\\s+");
        while (!command[0].equals("stop")){
            int currentCarWay = 0;
            int entryPoint = Integer.parseInt(command[0]);
            int parkingRow = Integer.parseInt(command[1]);
            int parkingCol = Integer.parseInt(command[2]);
            if(parkingLot.get(parkingRow).contains("V")){
                if(entryPoint<=parkingRow){
                    for (int rowEntry = entryPoint; rowEntry <= parkingRow; rowEntry++) {
                        currentCarWay++;
                    }
                }else {
                    for (int rowEntry = entryPoint; rowEntry >= parkingRow; rowEntry--) {
                        currentCarWay++;
                    }
                }

                if(parkingLot.get(parkingRow).get(parkingCol).equals("V")){
                    for (int i = 1; i <= parkingCol; i++) {
                        currentCarWay++;
                    }
                    parkingLot.get(parkingRow).set(parkingCol, "P");
                }else{
                    for (int i = 1; i <= parkingLot.get(parkingRow).size()-1; i++) {
                        currentCarWay++;
                        if(parkingLot.get(parkingRow).get(i).equals("V")){
                            parkingLot.get(parkingRow).set(i, "P");
                            break;
                        }
                    }
                }
                System.out.println(currentCarWay);
            }else{
                System.out.printf("Row %d full%n", parkingRow);
            }



            command = scan.nextLine().split("\\s+");
        }
    }

    private static List<List<String>> createParking(int row, int col) {
        List<List<String>> parking = new ArrayList<>();
        for (int rows = 0; rows < row; rows++) {
            parking.add(rows, new ArrayList<>());
            for (int cols = 0; cols < col; cols++) {
                if(cols == 0){
                    parking.get(rows).add(cols, "R");
                }else{
                    parking.get(rows).add(cols, "V");
                }

            }
        }
        return parking;
    }
}

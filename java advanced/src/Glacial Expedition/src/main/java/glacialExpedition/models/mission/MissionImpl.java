package glacialExpedition.models.mission;

import glacialExpedition.models.explorers.Explorer;
import glacialExpedition.models.states.State;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MissionImpl implements Mission {

    private int retired;


    public MissionImpl() {
        retired = 0;
    }

    @Override
    public void explore(State state, List<Explorer> explorers) {

        for (Explorer explorer : explorers) {
            while (explorer.canSearch()) {
                if (state.getExhibits().isEmpty()) {
                    break;
                }
                if (!state.getExhibits().isEmpty()) {
                    explorer.search();
                    String item = ((List<String>) state.getExhibits()).get(0);
                    explorer.getSuitcase().getExhibits().add(item);
                    state.getExhibits().remove(item);
                }
                if(!explorer.canSearch()){
                    setRetired(getRetired()+1);
                }
            }
        }
    }


        private void setRetired ( int retired){
            this.retired = retired;
        }

        public int getRetired () {
            return retired;
        }

}
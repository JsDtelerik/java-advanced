package day_6_Sets_and_Maps;

import java.util.*;

public class UserLogs {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        TreeMap<String, LinkedHashMap<String, Integer>> data = new TreeMap<>();
        String[] command = scan.nextLine().split("user=");
        while(!command[0].equals("end")){
            String userName = command[1];
           String ip = command[0].substring(3, command[0].indexOf(" "));
           if(!data.containsKey(userName)){
               data.put(userName, new LinkedHashMap<>());
               data.get(userName).put(ip, 1);
           }else{

               if(data.get(userName).containsKey(ip)){
                   data.get(userName).put(ip, data.get(userName).get(ip)+1);
               }else{
                   data.get(userName).put(ip, 1);
               }

           }
            command =scan.nextLine().split("user=");
        }

        data.entrySet().stream().forEach(k -> {
            System.out.printf("%s: %n", k.getKey());
            int count = k.getValue().size();
            LinkedHashMap<String, Integer> inside = k.getValue();
            for (var insideLoop : inside.entrySet()) {
                count--;
                if(count == 0){
                    System.out.printf("%s => %d.%n", insideLoop.getKey(), insideLoop.getValue());
                }else{
                    System.out.printf("%s => %d, ", insideLoop.getKey(), insideLoop.getValue());
                }
            }
                }
        );
    }
}

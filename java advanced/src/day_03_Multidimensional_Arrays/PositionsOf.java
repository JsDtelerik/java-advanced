package day_03_Multidimensional_Arrays;

import java.util.Scanner;

public class PositionsOf {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int rows = scan.nextInt();
        int cols = scan.nextInt();
        scan.nextLine();
        int[][] matrix = new int[rows][cols];
        matrix = fillTheMatrix(rows, cols, scan);

        int specialNumber = scan.nextInt();
        boolean isFound = false;
        for (int row = 0; row < matrix.length; row++) {
            int[] column =  matrix[row];
            for (int col = 0; col < column.length; col++) {
                if (matrix[row][col] == specialNumber){
                    System.out.println(row + " " + col);
                    isFound = true;
                }
            }
        }
    if(!isFound){
        System.out.println("not found");
    }
    }

    public static int[][] fillTheMatrix(int rows, int cols, Scanner scan) {
        int[][] matrix = new int[rows][cols];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                matrix[row][col] = scan.nextInt();
            }
            scan.nextLine();
        }
        return matrix;
    }
}

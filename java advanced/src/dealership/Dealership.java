package dealership;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Dealership {
    public static int currentCarYear = 0 ;
    private List<Car> data;
    private String name;
    private int capacity;


    public Dealership(String name, int capacity) {
        this.name = name;
        this.capacity = capacity;
        this.data = new ArrayList<>();
    }


    //•	Method add(Car car) – adds an entity to the data if there is an empty cell for the car.

    public void add(Car car){
        if(this.data.size() < this.capacity){
            this.data.add(car);
        }
    }

    public boolean buy(String manufacturer, String model){
        boolean bought = false;
        for (Car cars : data) {
            if(cars.getManufacturer().equals(manufacturer) && cars.getModel().equals(model)){
                bought = true;
                data.remove(cars);
                break;
            }
        }

        return bought;
    }


    //getLatestCar() – returns the latest car (by year) or null if have no cars

    public Car getLatestCar(){
        Car car = null;

            for (Car cars : data) {
                if(cars.getYear() > currentCarYear){
                    currentCarYear = cars.getYear();
                    car = cars;
                }
            }
        return car;
    }
    //getCar(String manufacturer, String model)
    // – returns the car with the given manufacturer and model or null if there is no such car.

    public Car getCar(String manufacturer, String model){
        for (Car cars : data) {
            if(cars.getManufacturer().equals(manufacturer) && cars.getModel().equals(model)){
                return cars;
            }
        }
        return null;
    }


    //•	Getter getCount() – returns the number of cars.

    public int getCount(){
       return this.data.size();
    }

    //•	getStatistics() – returns a String in the following format:
    //o	" The cars are in a car dealership {name}:
    //{Car1}
    //{Car2}
    //(…)"

    public String getStatistics(){
        StringBuilder builder = new StringBuilder();

        builder.append("The cars are in a car dealership ").append(this.name)
                .append(":").append(System.lineSeparator());
        for (Car cars : data) {
            builder.append(cars).append(System.lineSeparator());
        }


        return builder.toString();
    }
}

package day_04_Multidimensional;

import java.util.Scanner;

public class MatrixShuffling {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int rows = scan.nextInt();
        int cols = scan.nextInt();
        String[][] matrix = fillMatrix(rows, cols, scan);

        String[] command = scan.nextLine().split("\\s+");
        while (!command[0].equals("END")){

            if(!validateCommand(command, rows, cols)){
                System.out.println("Invalid input!");

            }else {
                String original = matrix[Integer.parseInt(command[1])][Integer.parseInt(command[2])];
                String replace = matrix[Integer.parseInt(command[3])][Integer.parseInt(command[4])];
                matrix[Integer.parseInt(command[1])][Integer.parseInt(command[2])] = replace;
                matrix[Integer.parseInt(command[3])][Integer.parseInt(command[4])] = original;
                printMatrix(matrix);
            }
            command = scan.nextLine().split("\\s+");

        }


    }

    private static boolean validateCommand(String[] command, int rows, int cols) {
        if(command.length != 5){
            return false;
        }
        int originalRow = Integer.parseInt(command[1]);
        int originalCol = Integer.parseInt(command[2]);
        int swapRow = Integer.parseInt(command[3]);
        int swapCol = Integer.parseInt(command[4]);
        if((originalRow <0 || originalRow > rows-1) || (originalCol <0 || originalCol > cols-1)
        || (swapRow <0 || swapRow > rows-1) || (swapCol <0 || swapCol > cols-1)){
            return false;
        }
        return true;
    }

    private static String[][] fillMatrix(int rows, int cols, Scanner scan) {
        String[][] matrix = new String[rows][cols];
        scan.nextLine();
        for (int row = 0; row < rows; row++) {
            String[] input = scan.nextLine().split("\\s+");
            for (int col = 0; col < cols; col++) {
                matrix[row][col] = input[col];
            }

        }
        return matrix;
    }
    public static void printMatrix(String[][] matrix) {
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {
                System.out.print(matrix[row][col] + " ");
            }
            System.out.println();
        }
    }

}
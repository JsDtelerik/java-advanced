package day_05_Sets_and_Maps;

import java.util.*;

public class CountRealNumbers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        LinkedHashMap<Double, Integer> numbers = new LinkedHashMap<>();

        double[] readLine = Arrays.stream(scan.nextLine().split("\\s+")).mapToDouble(Double::parseDouble).toArray();

        for (int i = 0; i < readLine.length; i++) {
            if(numbers.containsKey(readLine[i])){
                numbers.put(readLine[i], numbers.get(readLine[i]) +1);
            }else{
                numbers.put(readLine[i], 1);
            }
        }

        numbers.forEach((key, value) -> System.out.printf("%.1f -> %d%n", key, value));
    }
}

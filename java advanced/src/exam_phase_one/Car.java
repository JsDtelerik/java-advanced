package exam_phase_one;

public class Car {

    public String model;
    public String year;

    public Car(String model){
        this.model = model;
    }

    public Car(String model, String year){
        this.model = model;
        this.year = year;
    }
}

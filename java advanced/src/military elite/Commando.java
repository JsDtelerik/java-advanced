import java.util.List;

public class Commando extends Private{

    private Corps corps;
    private List<Mission> missionList;
    public Commando(int id, String firstName, String lastName, double salary, Corps corps, List<Mission> missionList) {
        super(id, firstName, lastName, salary);
        this.corps = corps;
        this.missionList = missionList;
    }

    //Commando <id> <firstName> <lastName> <salary> <corps>
    // <mission1CodeName>  <mission1state> … <missionNCodeName> <missionNstate>

//Name: Petra Ivanova Id: 19 Salary: 150.15
//Corps: Airforces
//Missions:
//  Code Name: Freedom State: inProgress
    @Override
    public String toString() {
        String salaryFormated = String.format("%.2f", salary());
        StringBuilder builder = new StringBuilder();
        builder.append("Name: ").append(firstName()).append(" ").append(lastName()).append(" Id: ").append(id())
                .append(" Salary: ").append(salaryFormated).append(System.lineSeparator())
                .append("Corps: ").append(corps.getValue()).append(System.lineSeparator())
                .append("Missions:");
        boolean first = true;
        int size = missionList.size();
        for (Mission mission : missionList) {
            if(first){
                builder.append(System.lineSeparator());
                first = false;
            }
            if(size-->1){
                builder.append(" ").append(mission.toString()).append(System.lineSeparator());
            }else{
                builder.append(" ").append(mission.toString());
            }


        }

        return builder.toString();
    }
}

public interface Car {
    //+	brakes() : String
    //+	gas() : String
    String brakes();
    String gas();
}

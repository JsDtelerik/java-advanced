import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] input = reader.readLine().split("\\s+");

        List<Mammal> animals = new ArrayList<>();

        while (!input[0].equals("End")){
            String[] foodInput = reader.readLine().split("\\s+");

            Food food = null;
            if(foodInput[0].equals("Vegetable")){
                food = new Vegetable(Integer.parseInt(foodInput[1]));
            }else if (foodInput[0].equals("Meat")){
                food = new Meat(Integer.parseInt(foodInput[1]));
            }

            switch (input[0]){
                case"Cat":
                    //{AnimalType} {AnimalName} {AnimalWeight} {AnimalLivingRegion} [{CatBreed} = Only if its cat]
                    // animalType,  animalName, Double animalWeight, Integer foodEaten,  livingRegion,  breed

                    Mammal cat = new Cat(input[0], input[1], Double.parseDouble(input[2]), 0, input[3], input[4]);
                    cat.makeSound();
                    cat.eat(food);
                    animals.add(cat);
                    break;
                case "Tiger":
                    Mammal tiger = new Tiger(input[0], input[1], Double.parseDouble(input[2]), 0, input[3]);
                    tiger.makeSound();
                    tiger.eat(food);
                    animals.add(tiger);
                    break;
                case "Zebra":
                    Mammal zebra = new Zebra (input[0], input[1], Double.parseDouble(input[2]), 0, input[3]);
                    zebra.makeSound();
                    zebra.eat(food);
                    animals.add(zebra);
                    break;
                case "Mouse":
                    Mammal mouse = new Mouse (input[0], input[1], Double.parseDouble(input[2]), 0, input[3]);
                    mouse.makeSound();
                    mouse.eat(food);
                    animals.add(mouse);
                    break;
            }
            input = reader.readLine().split("\\s+");

        }


        animals.forEach(a -> System.out.println(a.toString()));
    }
}

package day_6_Sets_and_Maps;

import java.util.*;
import java.util.stream.Collectors;

public class PopulationCounter {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String[] command = scan.nextLine().split("\\|");
        LinkedHashMap<String, LinkedHashMap<String, Integer>> data = new LinkedHashMap<>();
        LinkedHashMap<String, Integer> countryPopulation = new LinkedHashMap<>();
        while (!command[0].equals("report")){
            String country = command[1];
            String city = command[0];
            int population = Integer.parseInt(command[2]);

            if(!data.containsKey(country)){
                data.put(country, new LinkedHashMap<>());
                countryPopulation.put(country, population);

            }else{
                countryPopulation.put(country, countryPopulation.get(country)+population);


            }
            data.get(country).put(city, population);


            command = scan.nextLine().split("\\|");
        }

        countryPopulation.entrySet().stream().sorted((k1, k2) -> k2.getValue().compareTo(k1.getValue()))
                .forEach(k -> {
                    System.out.printf("%s (total population: %d)%n", k.getKey(), k.getValue());
                    data.get(k.getKey()).entrySet().stream()
                            .sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue()))
                            .forEach(e -> System.out.printf("=>%s: %d%n", e.getKey(), e.getValue()));
                });

       /* data.entrySet().stream()
                .sorted((country1, country2) -> country2.getValue().values().stream().reduce(0L, Long::sum)
                        .compareTo(country1.getValue().values().stream().reduce(0L, Long::sum)))
                .forEach(country -> {
                    System.out.printf("%s (total population: %d)%n", country.getKey(),
                            country.getValue().values().stream().reduce(0L, Long::sum));
                    country.getValue().entrySet().stream()
                            .sorted((city1, city2) -> city2.getValue().compareTo(city1.getValue()))
                            .forEach(city -> System.out.printf("=>%s: %d%n", city.getKey(), city.getValue()));
                });*/

    }
}
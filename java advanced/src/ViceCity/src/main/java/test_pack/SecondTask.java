package test_pack;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SecondTask {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String[] input = scan.nextLine().split(", ");
        String topLine = "qwertyuiop";
        String middleLine = "asdfghjkl";
        String lastLine = "zxcvbnm";
        boolean topLineBoolean = false;
        boolean midLineBoolean = false;
        boolean lastLineBoolean = false;

        List<String> result = new ArrayList<>();
        for (int i = 0; i < input.length; i++) {
            String currentWordForTest = input[i].toLowerCase();
            String currentWord = input[i];
            for (int j = 0; j < currentWordForTest.length(); j++) {
                String wordChar = String.valueOf(currentWordForTest.charAt(j));
                if(j == 0){
                    if(topLine.contains(wordChar)){
                        topLineBoolean = true;
                    }else if(middleLine.contains(wordChar)){
                        midLineBoolean = true;
                    }else if (lastLine.contains(wordChar)){
                        lastLineBoolean = true;
                    }
                    continue;
                }

                if(topLineBoolean && !topLine.contains(wordChar)){
                    topLineBoolean = false;
                    break;
                }else if (midLineBoolean && !middleLine.contains(wordChar)){
                    midLineBoolean = false;
                    break;
                }else if(lastLineBoolean && !lastLine.contains(wordChar)){
                    lastLineBoolean = false;
                    break;
                }
            }
            if(topLineBoolean || midLineBoolean || lastLineBoolean){
                result.add(currentWord);
                lastLineBoolean = false;
                topLineBoolean = false;
                midLineBoolean = false;
            }


        }

        for (int i = 0; i < result.size(); i++) {
            if(i<result.size()-1){
                System.out.print(result.get(i) + ", ");
            }else{
                System.out.print(result.get(i));
            }
        }

    }
}

package day_04_Multidimensional.Additional;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Snake {
    public static int snakeRow = 0, snakeCol = 0, foodEaten = 0;
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());
        char[][] field = fillField(n, reader);

        boolean isInBound = true;

        while (foodEaten < 10 && isInBound){
            String command = reader.readLine();

            switch (command){
                case "up":
                    isInBound = moveSnake(field, -1, 0);
                    break;
                case "down":
                    isInBound = moveSnake(field, +1, 0);
                    break;
                case "left":
                    isInBound = moveSnake(field, 0, -1);
                    break;
                case "right":
                    isInBound = moveSnake(field, 0, +1);
                    break;
            }



        }

        if(!isInBound){
            System.out.println("Game over!");
        }else{
            System.out.println("You won! You fed the snake.");
        }
        System.out.printf("Food eaten: %d%n", foodEaten);
        System.out.println(getField(field));


    }

    private static boolean moveSnake(char[][] field, int additionalRow, int additionalCol) {
        boolean isInFiled = checkSnakePositionAfterMove(field, snakeRow + additionalRow, snakeCol+additionalCol);
        field[snakeRow][snakeCol] = '.';
        if(isInFiled){
            if(field[additionalRow+snakeRow][snakeCol+additionalCol] == 'B' ){
                field[additionalRow+snakeRow][snakeCol+additionalCol] = '.';
                changeSnakeCoordinate(field);
                field[snakeRow][snakeCol] = 'S';
               return isInFiled;
            }else if(field[additionalRow+snakeRow][snakeCol+additionalCol] == '*' ){
                foodEaten +=1;
                field[additionalRow+snakeRow][snakeCol+additionalCol] = 'S';
            }else{
                field[additionalRow+snakeRow][snakeCol+additionalCol] = 'S';

            }
            snakeRow += additionalRow;
            snakeCol += additionalCol;
        }

        return isInFiled;
    }

    private static void changeSnakeCoordinate(char[][] field) {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if(field[i][j] == 'B'){
                    snakeRow = i;
                    snakeCol = j;
                }
            }
        }
    }

    private static boolean checkSnakePositionAfterMove(char[][] field, int r, int c) {
        return r >= 0 && r < field.length && c >= 0 && c < field[r].length;
    }

    private static String getField(char[][] field) {
        StringBuilder output = new StringBuilder();
        for (char[] chars : field) {
            for (char s : chars) {
                output.append(s);
            }
            output.append(System.lineSeparator());
        }
        return output.toString();
    }

    private static char[][] fillField(int n, BufferedReader reader) throws IOException {
        char [][] filed = new char[n][n];
        for (int i = 0; i < filed.length; i++) {
            char[] line = reader.readLine().toCharArray();
            for (int j = 0; j < line.length; j++) {
                filed[i][j] = line[j];
                if(line[j] == 'S'){
                    snakeRow = i;
                    snakeCol = j;
                }
            }
            filed[i] = line;

        }
        return filed;
    }
}


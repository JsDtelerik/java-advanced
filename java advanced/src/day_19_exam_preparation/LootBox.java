package day_19_exam_preparation;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

public class LootBox {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        ArrayDeque<Integer> firstBox = Arrays.stream(scan.nextLine().split("\\s+")).map(Integer::parseInt)
                .collect(Collectors.toCollection(ArrayDeque::new));

        ArrayDeque<Integer> secondBox = new ArrayDeque<>();

        Arrays.stream(scan.nextLine().split("\\s+")).map(Integer::parseInt).forEach(secondBox::push);

        int sumValue = 0;
        while (firstBox.size() > 0 && secondBox.size() > 0){
            int firstNumber = firstBox.peek();
            int secondNumber = secondBox.pop();

            int currentSum = firstNumber+secondNumber;

            if(currentSum % 2 == 0){
                sumValue += currentSum;
                firstBox.poll();
            }else{
                firstBox.offer(secondNumber);
            }

        }
        if(firstBox.isEmpty()){
            System.out.println("First lootbox is empty");
        }else{
            System.out.println("Second lootbox is empty");
        }


        if(sumValue >= 100 ){
            System.out.printf("Your loot was epic! Value: %d", sumValue);
        }else{
            System.out.printf("Your loot was poor... Value: %d", sumValue);
        }
    }
}

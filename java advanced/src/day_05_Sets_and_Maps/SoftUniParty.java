package day_05_Sets_and_Maps;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Scanner;

public class SoftUniParty {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String guest = scan.nextLine();

        LinkedHashSet<String> guestList = new LinkedHashSet<>();

        while (!guest.equals("PARTY")){
            guestList.add(guest);
            guest = scan.nextLine();
        }

        String guestWhoCame = scan.nextLine();
        while (!guestWhoCame.equals("END")){
            guestList.remove(guestWhoCame);
            guestWhoCame = scan.nextLine();
        }

        System.out.println(guestList.size());
        guestList.stream().sorted().forEach(System.out::println);
    }


}

package day_03_Multidimensional_Arrays;

import java.util.Scanner;

public class CompareMatrices {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int rows1 = scan.nextInt();
        int cols1 = scan.nextInt();
        scan.nextLine();

        int[][] firstMatrix = new int[rows1][cols1];

        firstMatrix = fillMatrix(rows1, cols1, scan);
        int rows2 = scan.nextInt();
        int cols2 = scan.nextInt();
        scan.nextLine();
        int[][] secondMatrix = new int[rows2][cols2];
        secondMatrix = fillMatrix(rows2, cols2, scan);

       boolean isEqual = compareBothMatrices(firstMatrix, secondMatrix);
       if(isEqual){
           System.out.print("equal");
       }else {
           System.out.print("not equal");
       }

    }

    public static boolean compareBothMatrices(int[][] firstMatrix, int[][] secondMatrix) {
        if(firstMatrix.length != secondMatrix.length){
            return false;
        }
        for (int row = 0; row < firstMatrix.length; row++) {
            int[] firstMatrixCheck = firstMatrix[row];
            int[] secondMatrixCheck = secondMatrix[row];
            if(firstMatrixCheck.length != secondMatrixCheck.length){
                return false;
            }
            for (int col = 0; col <firstMatrixCheck.length ; col++) {
                if(firstMatrixCheck[col] != secondMatrixCheck[col]){
                    return false;
                }
            }

        }
        return true;
    }

    public static int[][] fillMatrix(int rows, int cols, Scanner scan) {
        int[][] matrix = new int[rows][cols];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                matrix[row][col] = scan.nextInt();
            }
            scan.nextLine();
        }
        return matrix;
    }
}

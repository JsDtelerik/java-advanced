package day_out_exam;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

public class TaskTwo {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        ArrayDeque<Integer> tasks = new ArrayDeque<>();

        Arrays.stream(scan.nextLine().split(", ")).map(Integer::parseInt).forEach(tasks::push);



        ArrayDeque<Integer> threadValues = Arrays.stream(scan.nextLine().split("\\s+")).map(Integer::parseInt)
                .collect(Collectors.toCollection(ArrayDeque::new));

        int kill = Integer.parseInt(scan.nextLine());
        boolean killed = false;
        int keepLastThread = 0;
        while (!killed){
            int first = threadValues.peek();
            int second = tasks.peek();

          /* if(second == kill){
               killed = true;
               keepLastThread = first;
               threadValues.offer(first);
               break;
           }else if (first>=second){
                tasks.pop();
           }*/
            if(second == kill){
                keepLastThread = first;
                killed = true;
                break;
            }
            if(first>=second){
                threadValues.poll();
                tasks.pop();
            }else if (second>first){
                threadValues.poll();
            }



        }

        //Thread with value 20 killed task 54
        //20 34

        System.out.printf("Thread with value %d killed task %d%n", keepLastThread, kill);

        while (!threadValues.isEmpty()){
            System.out.print(threadValues.poll() + " ");
        }




    }
}

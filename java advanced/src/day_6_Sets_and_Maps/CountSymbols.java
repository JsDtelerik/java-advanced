package day_6_Sets_and_Maps;

import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.TreeMap;

public class CountSymbols {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        TreeMap<Character, Integer> charCount = new TreeMap<>();

        String word = scan.nextLine();
        for (int i = 0; i < word.length(); i++) {
            char symbol = word.charAt(i);
            if(charCount.containsKey(symbol)){
                charCount.put(symbol, charCount.get(symbol)+1);

            }else{
                charCount.put(symbol, 1);
            }
        }

        charCount.entrySet().stream().forEach(k -> System.out.printf("%c: %d time/s%n", k.getKey(), k.getValue()));
    }
}

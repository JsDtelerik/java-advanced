package day_6_Sets_and_Maps;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class Phonebook {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String[] command = scan.nextLine().split("-");

        HashMap<String, String> phoneBook = new HashMap<>();

        while (!command[0].equals("search")){

            phoneBook.put(command[0], command[1]);

            command = scan.nextLine().split("-");
        }

        command = scan.nextLine().split("\\s+");

        while (!command[0].equals("stop")){

            if(phoneBook.containsKey(command[0])){

                System.out.println(command[0] + " -> " + phoneBook.get(command[0]));
            }else{
                System.out.printf("Contact %s does not exist.%n", command[0]);
            }



            command = scan.nextLine().split("\\s+");
        }

    }
}

package day_04_Multidimensional;

import javax.print.DocFlavor;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TheMatrix {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] dimensions = Arrays.stream(reader.readLine().split("\\s+")).mapToInt(Integer:: parseInt).toArray();
        int row = dimensions[0];
        int col = dimensions[1];
        char[][] matrix = fillMatrix(row, col, reader);
       char specialSymbol = reader.readLine().charAt(0);
       dimensions = Arrays.stream(reader.readLine().split("\\s+")).mapToInt(Integer:: parseInt).toArray();

       int startRow = dimensions[0];
       int startCol = dimensions[1];
       char symbol = matrix[startRow][startCol];
        changeSymbols(matrix, specialSymbol, symbol, startRow, startCol);
        printMatrix(matrix);

    }

    private static void changeSymbols(char[][] matrix, char specialSymbol, char symbol, int startRow, int startCol) {

        if(isOutOfBound(matrix, startRow, startCol)){
            return;
        }
        if(matrix[startRow][startCol] != symbol){
            return;
        }

        matrix[startRow][startCol] = specialSymbol;


        changeSymbols(matrix, specialSymbol, symbol, startRow-1, startCol );
        changeSymbols(matrix, specialSymbol, symbol, startRow+1, startCol );
        changeSymbols(matrix, specialSymbol, symbol, startRow, startCol -1 );
        changeSymbols(matrix, specialSymbol, symbol, startRow, startCol +1 );

    }

    public static void printMatrix(char[][] matrix){
        for (char[] chars : matrix) {
            for (char symbol : chars) {
                System.out.print(symbol);
            }
            System.out.println();
        }
    }


    public static boolean isOutOfBound(char[][] matrix, int row, int col){

        return row  < 0 || row >= matrix.length || col < 0 || col >= matrix[row].length;
    }

    public static char[][] fillMatrix(int row, int col, BufferedReader reader) throws IOException {
       char[][] matrix = new char[row][col];
        for (int rows = 0; rows < row; rows++) {
            String[] line = reader.readLine().split("\\s+");
            for (int cols = 0; cols < line.length; cols++) {
                matrix[rows][cols] = line[cols].charAt(0);
            }

        }

        return matrix;
    }
}

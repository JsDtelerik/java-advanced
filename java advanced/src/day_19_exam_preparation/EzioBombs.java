package day_19_exam_preparation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.stream.Collectors;

public class EzioBombs {
    public static int daturaBombs = 0, cherryBombs = 0, smokeDecoyBombs = 0;
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        ArrayDeque<Integer> bombEffect = Arrays.stream(reader.readLine().split(", ")).map(Integer::parseInt)
                .collect(Collectors.toCollection(ArrayDeque::new));

        ArrayDeque<Integer> bombCasing = new ArrayDeque<>();

        Arrays.stream(reader.readLine().split(", ")).map(Integer::parseInt).forEach(bombCasing::push);
        boolean readyForExplosion = false;
        while(!(bombEffect.isEmpty() || bombCasing.isEmpty())){


            int first = bombEffect.poll();
            int second = bombCasing.pop();
            int sum = first+second;
            if(sum == 120){
                smokeDecoyBombs++;
            }else if (sum == 60){
                cherryBombs++;
            }else if (sum == 40){
                daturaBombs++;
            }else{
                second = second-5;
                if(second <0){
                    second = 0;

                }
                bombEffect.addFirst(first);
                bombCasing.push(second);
            }

            if(daturaBombs >=3 && cherryBombs>= 3 && smokeDecoyBombs >=3){
                readyForExplosion = true;
                break;
            }

        }


        if(readyForExplosion){
            System.out.println("Bene! You have successfully filled the bomb pouch!");
        }else{
            System.out.println("You don't have enough materials to fill the bomb pouch.");
        }

        if(bombEffect.isEmpty()){
            System.out.println("Bomb Effects: empty");
        }else{
            System.out.print("Bomb Effects: ");
            int effects = bombEffect.size()-1;
            for (Integer effect : bombEffect) {
                if(effects>0){
                    System.out.print(effect + ", ");
                }else{
                    System.out.print(effect);
                }
                effects--;
            }
            System.out.println();
        }

        if(bombCasing.isEmpty()){
            System.out.println("Bomb Casings: empty");
        }else{
            System.out.print("Bomb Casings: ");
            int casing = bombCasing.size()-1;
            for (Integer effect : bombCasing) {
                if(casing>0){
                    System.out.print(effect + ", ");
                }else{
                    System.out.print(effect);
                }
                casing--;
            }
            System.out.println();
        }

        System.out.printf("Cherry Bombs: %d%n", cherryBombs);
        System.out.printf("Datura Bombs: %d%n", daturaBombs);
        System.out.printf("Smoke Decoy Bombs: %d%n", smokeDecoyBombs);

    }
}

import java.util.List;

public class Smartphone implements Browsable, Callable {
//numbers: List<String>
//urls: List<String>

    private List<String> numbers;
    private List<String> urls;

    public Smartphone(List<String> numbers, List<String> urls) {
        this.numbers = numbers;
        this.urls = urls;
    }


    @Override
    public String call() {
        StringBuilder builder = new StringBuilder();
        boolean isPhoneNumber = true;
        for (String number : numbers) {
            isPhoneNumber = true;
            for (int i = 0; i < number.length(); i++) {
                if (Character.isDigit(number.charAt(i))) {
                    continue;
                } else {
                    isPhoneNumber = false;
                    break;
                }
            }
            if (isPhoneNumber) {
                builder.append("Calling... ").append(number).append(System.lineSeparator());
            }else{
                builder.append("Invalid number!").append(System.lineSeparator());
            }

        }

        return builder.toString();
    }

    @Override
    public String browse() {
        StringBuilder builder = new StringBuilder();
        boolean isUrl = true;
        for (String url : urls) {
            isUrl = true;
            for (int i = 0; i < url.length(); i++) {
                if (Character.isDigit(url.charAt(i))) {
                    isUrl = false;
                    break;
                }
            }
            if (isUrl) {
                builder.append("Browsing: ").append(url).append("!").append(System.lineSeparator());
            }else{
                builder.append("Invalid URL!").append(System.lineSeparator());
            }

        }

        return builder.toString();
    }
}

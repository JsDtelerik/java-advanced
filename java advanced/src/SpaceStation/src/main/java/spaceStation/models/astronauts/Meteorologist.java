package spaceStation.models.astronauts;

public class Meteorologist extends BaseAstronaut{

        private  final double INITIAL_OXYGEN = 90;

    protected Meteorologist(String name) {
        super(name);
        setOxygen(INITIAL_OXYGEN);
    }

    @Override
    public void breath() {
        double decrease = INITIAL_OXYGEN - 0;
        setOxygen(decrease);
    }
}

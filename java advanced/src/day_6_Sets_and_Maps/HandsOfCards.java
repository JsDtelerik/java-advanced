package day_6_Sets_and_Maps;

import java.util.*;

public class HandsOfCards {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        LinkedHashMap<String, LinkedHashSet<String>> playerDecks = new LinkedHashMap<>();

        String[] command = scan.nextLine().split(": ");

        while (!command[0].equals("JOKER")){
            String name = command[0];
            String[] cards = command[1].split(", ");

            if(!playerDecks.containsKey(name)){
                playerDecks.put(name, new LinkedHashSet<>());
            }

            for (int i = 0; i < cards.length; i++) {

                playerDecks.get(name).add(cards[i]);
            }

            command = scan.nextLine().split(": ");
        }

        for (var playerEntry : playerDecks.entrySet()) {
            System.out.print(playerEntry.getKey()+ ": ");
            int sumOfValues = 0;
            LinkedHashSet<String> playerDeck = playerEntry.getValue();

            for (String s : playerDeck) {
                String cardPower = s.substring(0, 1);
                String cardColor = s.substring(1, 2);
                if(s.contains("10")){
                     cardPower = s.substring(0, 2);
                     cardColor = s.substring(2, 3);
                }

                    int power = 0;
                    int color = 1;
                    if(!(cardPower.equals("A") || cardPower.equals("K") || cardPower.equals("Q")
                            || cardPower.equals("J"))){
                        power= Integer.parseInt(cardPower);
                    }else{
                        switch (cardPower){
                            case "A":
                                power = 14;
                                break;
                            case "K":
                                power = 13;
                                break;
                            case "Q":
                                power = 12;
                                break;
                            case "J":
                                power = 11;
                                break;
                        }
                    }

                    switch (cardColor){
                        case "S":
                            color = 4;
                            break;
                        case "H":
                            color = 3;
                            break;
                        case "D":
                            color = 2;
                            break;
                        case "C":
                            color = 1;
                            break;

                    }

                    sumOfValues += color*power;

                }
            System.out.print(sumOfValues);
            System.out.println();

            }


    }
}

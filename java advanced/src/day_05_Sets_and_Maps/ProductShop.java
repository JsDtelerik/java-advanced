package day_05_Sets_and_Maps;

import java.util.*;

public class ProductShop {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        TreeMap<String, LinkedHashMap<String, Double>> shops = new TreeMap<>();
        String[] command = scan.nextLine().split(", ");
        while (!command[0].equals("Revision")) {
            if (shops.containsKey(command[0])) {
                shops.get(command[0]).put(command[1], Double.parseDouble(command[2]));
            } else {
                shops.put(command[0], new LinkedHashMap<>());
                shops.get(command[0]).put(command[1], Double.parseDouble(command[2]));
            }
            command = scan.nextLine().split(", ");
        }

        for (var entry : shops.entrySet()) {
            String shopName = entry.getKey();
            System.out.printf("%s->%n", shopName);

            LinkedHashMap<String, Double> innerMap = entry.getValue();

            for (var inner : innerMap.entrySet()) {

                System.out.printf("Product: %s, Price: %.1f%n", inner.getKey(), inner.getValue());
            }


        }
    }
}
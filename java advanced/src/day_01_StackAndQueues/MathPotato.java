package day_01_StackAndQueues;

import java.util.ArrayDeque;
import java.util.Scanner;

public class MathPotato {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] names = scan.nextLine().split("\\s+");
        int n = Integer.parseInt(scan.nextLine());
        ArrayDeque<String> players = new ArrayDeque<>();
        for (int i = 0; i < names.length; i++) {
            players.offer(names[i]);
        }

        int cycles = 1;
        while (!players.isEmpty()) {
            for (int i = 1; i <= n; i++) {
                if (i == n) {
                    if (players.size() > 1) {
                        if (checkCycles(cycles) && cycles>1){
                            System.out.println("Prime " + players.peek());
                        }else{
                            System.out.println("Removed " + players.poll());
                        }

                    } else if (players.size() == 1) {
                        System.out.println("Last is " + players.poll());
                    }
                } else {
                    String movePlayerAtTheEnd = players.poll();
                    players.offer(movePlayerAtTheEnd);
                }
            }
            cycles++;
        }
    }
    private static boolean checkCycles(int cyclesCount) {
        boolean isPrime = true;
        for (int i = 2; i <=cyclesCount/2 ; ++i) {
            if(cyclesCount %i == 0){
                isPrime = false;
                break;
            }
        }
        return isPrime;
    }
}

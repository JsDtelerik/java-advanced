package day_02_Stack;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Scanner;

public class ReverseNumbersWithAStack {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int[] numbers = Arrays.stream(scan.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();
        ArrayDeque<Integer> num = new ArrayDeque<>();
        for (int number : numbers) {
            num.push(number);
        }
        for (Integer integer : num) {
            System.out.print(integer + " ");

        }

    }
}

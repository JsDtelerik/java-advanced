package bakery.entities.tables.interfaces;

import bakery.entities.bakedFoods.interfaces.BakedFood;
import bakery.entities.drinks.interfaces.Drink;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;

import static bakery.common.ExceptionMessages.*;

public abstract class BaseTable implements Table{

    private Collection<BakedFood> foodOrders;
    private Collection<Drink> 	drinkOrders;
    private int tableNumber;
    private int capacity;
    private int numberOfPeople;
    private double pricePerPerson;
    private boolean isReserved;
    private double price;



    protected BaseTable(int tableNumber, int capacity, double pricePerPerson) {
        this.tableNumber = tableNumber;
        this.setCapacity(capacity);
        this.pricePerPerson = pricePerPerson;
        this.foodOrders = new ArrayList<>();
        this.drinkOrders = new ArrayList<>();
        this.isReserved = false;
        this.price = 0;
    }


    //int tableNumber, int capacity, double pricePerPerson


    private void setPrice(double price) {
        this.price = price;
    }

    @Override
    public int getTableNumber() {
        return this.tableNumber;
    }

    private void setCapacity(int capacity) {
        if(capacity <= 0){
            throw new IllegalArgumentException(INVALID_TABLE_CAPACITY);
        }
        this.capacity = capacity;
    }

    @Override
    public int getCapacity() {
        return this.capacity;
    }

    private void setNumberOfPeople(int numberOfPeople) {
        if(numberOfPeople <= 0){
            if(this.isReserved){
                this.numberOfPeople = numberOfPeople;
                return;
            }
                throw new IllegalArgumentException(INVALID_NUMBER_OF_PEOPLE);



        }

        this.numberOfPeople = numberOfPeople;
    }

    @Override
    public int getNumberOfPeople() {
        return this.numberOfPeople;
    }

    private void setPricePerPerson(double pricePerPerson) {
        this.pricePerPerson = pricePerPerson;
    }

    @Override
    public double getPricePerPerson() {

        return this.pricePerPerson;
    }

    private void setReserved(boolean reserved) {


        this.isReserved = reserved;
    }

    @Override
    public boolean isReserved() {

           return this.isReserved;


    }

    @Override
    public double getPrice() {

        return this.price;
    }

    @Override
    public void reserve(int numberOfPeople) {
        if(isReserved()){
            return;
        }

       this.setNumberOfPeople(numberOfPeople);
        this.price = numberOfPeople*this.getPricePerPerson();
        this.setReserved(true);
    }

    @Override
    public void orderFood(BakedFood food) {
        this.foodOrders.add(food);
    }

    @Override
    public void orderDrink(Drink drink) {
        this.drinkOrders.add(drink);
    }

    @Override
    public double getBill() {
        double sum = foodOrders.stream().mapToDouble(f -> f.getPrice()).sum()
                + drinkOrders.stream().mapToDouble(d -> d.getPrice()).sum() + getPrice();
        return sum;
    }

    @Override
    public void clear() {
        this.drinkOrders.clear();
        this.foodOrders.clear();
        this.setNumberOfPeople(0);
        this.setReserved(false);
        this.setPrice(0);
    }





    @Override
    public String getFreeTableInfo() {

        DecimalFormat df = new DecimalFormat("#.##");
        String pricePer = df.format(this.pricePerPerson);

        StringBuilder builder = new StringBuilder();


        builder.append("Table: ").append(this.tableNumber).append(System.lineSeparator())
                .append("Type: ").append(this.getClass().getSimpleName()).append(System.lineSeparator())
                .append("Capacity: ").append(getCapacity()).append(System.lineSeparator())
                .append("Price per Person: ").append(pricePer);

        return builder.toString();
    }



}

public class Private extends Soldier implements PrivateImpl, SoldierImpl{


    private double salary;

    public Private(int id, String firstName, String lastName, double salary) {
        super(id, firstName, lastName);
        this.setSalary(salary);
    }

    public void setSalary(double salary) {
        if(salary >= 0){
            this.salary = salary;
        }else{
            throw new IllegalArgumentException();
        }

    }

    @Override
    public double salary() {
        return this.salary;
    }

    @Override
    public int id() {
        return getId();
    }

    @Override
    public String firstName() {
        return getFirstName();
    }

    @Override
    public String lastName() {
        return getLastName();
    }


    @Override
    public String toString() {
        return String.format("Name: %s %s Id: %d Salary: %.2f", firstName(), lastName(), id(), salary());
    }


}

package day_04_Multidimensional;

import java.io.CharConversionException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class RadioactiveMutantVampireBunnies {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] rowAndCols = Arrays.stream(scan.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();

        int rows = rowAndCols[0];
        int cols = rowAndCols[1];

        List<List<Character>> matrix = fillMatrix(rows, cols, scan);
        int playerRow = getPlayerRow(matrix);
        int playerCol = getPlayerCol(matrix, playerRow);
        char[] command = scan.nextLine().toCharArray();
        boolean won = false;
        for (int i = 0; i < command.length; i++) {
            char currentCommand = command[i];
            int movePlayerByRow = 0;
            int movePlayerByCol = 0;
            switch (currentCommand){
                case 'U':
                    movePlayerByRow = playerRow-1;
                    movePlayerByCol = playerCol;
                    break;
                case 'D':
                    movePlayerByRow = playerRow+1;
                    movePlayerByCol = playerCol;
                    break;
                case 'L':
                    movePlayerByCol = playerCol-1;
                    movePlayerByRow = playerRow;
                    break;
                case 'R':
                    movePlayerByCol = playerCol+1;
                    movePlayerByRow = playerRow;
                    break;
            }
            if( movePlayerByRow >=0 && movePlayerByRow<matrix.size()
                    && movePlayerByCol < matrix.get(movePlayerByRow).size()-1 && movePlayerByCol>=0){
                if(matrix.get(movePlayerByRow).get(movePlayerByCol).equals('.')){
                    matrix.get(movePlayerByRow).set(movePlayerByCol, 'P');
                    matrix.get(playerRow).set(playerCol, '.');
                    playerRow = movePlayerByRow;
                    playerCol = movePlayerByCol;
                }else if (matrix.get(movePlayerByRow).get(movePlayerByCol).equals('B')){
                    won = false;
                    playerRow = movePlayerByRow;
                    playerCol = movePlayerByCol;

                }
            }else{
                matrix.get(playerRow).set(playerCol, '.');
                won = true;
            }


                matrix = addRabbits(matrix);
                matrix = tranform(matrix);
                if(won){
                    break;
                }
            if((playerRow >= 0 && playerRow< matrix.size()) && (playerCol>=0 && playerCol<matrix.size())){
                if(!matrix.get(playerRow).get(playerCol).equals('P')){
                    won = false;
                    break;
                }
            }


        }

        printMatrix(matrix);
        if(won){
            System.out.printf("won: %d %d", playerRow, playerCol);
        }else{
            System.out.printf("dead: %d %d", playerRow, playerCol);
        }
    }

    private static List<List<Character>> tranform(List<List<Character>> matrix) {
        for (int i = 0; i <matrix.size() ; i++) {
            for (int j = 0; j < matrix.get(i).size(); j++) {
                if(matrix.get(i).get(j).equals('C')){
                    matrix.get(i).set(j, 'B');
                }
            }
        }
        return matrix;
    }

    private static void printMatrix(List<List<Character>> matrix) {
        for (List<Character> characters : matrix) {
            for (Character character : characters) {

                System.out.print(character);
            }
            System.out.println();
        }
    }


    private static List<List<Character>> addRabbits(List<List<Character>> matrix) {
        for (int row = 0; row < matrix.size(); row++) {
            for (int col = 0; col < matrix.get(row).size(); col++) {
                if (matrix.get(row).get(col).equals('B')) {
                    if (row + 1 < matrix.size()) {
                        if (!matrix.get(row + 1).get(col).equals('B')) {
                            matrix.get(row + 1).set(col, 'C');
                        }
                    }
                    if (row - 1 >= 0) {
                        if (!matrix.get(row - 1).get(col).equals('B')) {
                            matrix.get(row - 1).set(col, 'C');
                        }
                    }
                    if (col - 1 >= 0) {
                        if (!matrix.get(row).get(col - 1).equals('B')) {
                            matrix.get(row).set(col - 1, 'C');
                        }
                    }
                    if (col + 1 < matrix.get(row).size()) {
                        if (!matrix.get(row).get(col + 1).equals('B')) {
                            matrix.get(row).set(col + 1, 'C');
                        }
                    }


                }
            }
        }
        return matrix;
    }
    private static int getPlayerCol(List<List<Character>> matrix, int playerRow) {
        for (int i = 0; i < matrix.get(playerRow).size(); i++) {
            if(matrix.get(playerRow).get(i).equals('P')){
                return i;
            }
        }
        return -1;
    }

    private static int getPlayerRow(List<List<Character>> matrix) {

        for (int i = 0; i < matrix.size(); i++) {
            if(matrix.get(i).contains('P')){
                return i;
            }
        }
        return -1;
    }

    private static List<List<Character>> fillMatrix(int rows, int cols, Scanner scan) {
        List<List<Character>> matrix = new ArrayList<>();
        for (int i = 0; i < rows; i++) {
            char[] line = scan.nextLine().toCharArray();
            matrix.add(i, new ArrayList<>());
            for (int j = 0; j < cols; j++) {

                matrix.get(i).add(j, line[j]);
            }
        }

        return matrix;
    }
}

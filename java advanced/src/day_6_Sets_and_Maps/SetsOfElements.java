package day_6_Sets_and_Maps;

import java.util.*;

public class SetsOfElements {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        LinkedHashSet<Integer> setOne = new LinkedHashSet<>();
        int[] l = Arrays.stream(scan.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();
        LinkedHashSet<Integer> n1 = fillN(l[0], scan);
        LinkedHashSet<Integer> n2 = fillN(l[1], scan);


        //int n = Arrays.stream(l).min().getAsInt();

        for (Integer integer : n1) {
            if(n2.contains(integer)){
                setOne.add(integer);
            }
        }

        setOne.forEach(e -> System.out.print(e + " "));
    }

    private static LinkedHashSet<Integer> fillN(int i, Scanner scan) {
        LinkedHashSet<Integer> numbers = new LinkedHashSet<>();
        for (int j = 0; j < i; j++) {
            int addThisNumber = Integer.parseInt(scan.nextLine());
            numbers.add(addThisNumber);
        }
        return numbers;
    }
}

package bakery.entities.tables.interfaces;

public class OutsideTable extends BaseTable{

    private static final double pricePerPerson = 3.50;

    public OutsideTable(int tableNumber, int capacity) {
        super(tableNumber, capacity, pricePerPerson);
    }


    @Override
    public String getFreeTableInfo() {
        StringBuilder builder = new StringBuilder();


        builder.append("Table: ").append(getTableNumber()).append(System.lineSeparator())
                .append("Type: ").append("OutsideTable").append(System.lineSeparator())
                .append("Capacity: ").append(getCapacity()).append(System.lineSeparator())
                .append("Price per Person: ").append(String.format("%.02f", getPricePerPerson()));

        return builder.toString();
    }

}

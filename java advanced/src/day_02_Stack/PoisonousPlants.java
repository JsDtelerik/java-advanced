package day_02_Stack;

import java.time.LocalDateTime;
import java.util.*;

public class PoisonousPlants {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        System.out.println(LocalDateTime.now());
        LinkedHashMap<String, Integer> plants = new LinkedHashMap<>();
        for (int i = 1; i <= n; i++) {

            plants.put("plant"+i, scan.nextInt());
            //2021-05-29T19:55:55.009165400
            //2021-05-29T19:55:55.968145

        }
        String[] plantsToRemove = addAllPlantsFromPlantsMap(plants);
        boolean noPlantDie = false;
        int dayCounter = 0;
        while (!noPlantDie) {
            List<String> remove = new ArrayList<>();
            for (int i = 0; i < plantsToRemove.length; i++) {
                if (i > 0 && i < plantsToRemove.length) {
                    if (plants.containsKey(plantsToRemove[i - 1]) && plants.containsKey(plantsToRemove[i])) {
                        int valueFirstPlant = plants.get(plantsToRemove[i - 1]);
                        int valueSecondPlant = plants.get(plantsToRemove[i]);
                        if (valueSecondPlant > valueFirstPlant) {
                            remove.add(plantsToRemove[i]);

                        }
                    }
                }

            }
            if(!remove.isEmpty()){
                for (String s : remove) {
                    plants.remove(s);
                }
                plantsToRemove = addAllPlantsFromPlantsMap(plants);
            }else{
                break;
            }
            dayCounter++;

        }

        System.out.println(dayCounter);
        System.out.println(LocalDateTime.now());
    }

    private static String[] addAllPlantsFromPlantsMap(LinkedHashMap<String, Integer> plants) {
            String[] plant = new String[plants.size()];
            int index =0;
        for (String s : plants.keySet()) {
            plant[index++] = s;

        }
        return plant;
    }
}


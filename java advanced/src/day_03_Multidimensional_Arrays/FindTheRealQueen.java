package day_03_Multidimensional_Arrays;

import java.util.Scanner;

public class FindTheRealQueen {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        char[][] matrix = fillMatrix(8, 8, scan);

        findTheRealQueenAndPrintCoordinate(matrix);

    }

    public static void findTheRealQueenAndPrintCoordinate(char[][] matrix) {
        int[] queenCoordinate = new int[2];
        label:
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {

                if(matrix[row][col] == 'q'){
                    int currentRow = row;
                    int currentCol = col;
                    boolean byRow = searchByRow(matrix, currentRow, currentCol);
                    boolean byCol = searchByCol(matrix, currentRow, currentCol);
                    boolean byDiagonalLeft = searchByDiagonalLeft(matrix, currentRow, currentCol);
                    boolean byDiagonalRight = searchByDiagonalRight(matrix, currentRow, currentCol);
                    if (byRow && byCol && byDiagonalLeft && byDiagonalRight){
                        queenCoordinate[0] = currentRow;
                        queenCoordinate[1] = currentCol;
                        break label;
                    }
                }
            }
        }
        System.out.print(queenCoordinate[0] + " " + queenCoordinate[1]);

    }

    private static boolean searchByDiagonalRight(char[][] matrix, int currentRow, int currentCol) {
        boolean clear = true;
        if(currentRow>0){
            int col = currentCol;
            for (int i = currentRow-1; i >=0 ; i--) {
                col++;
                if(col<matrix[currentCol].length){

                    if(matrix[i][col]=='q'){
                        return false;
                    }
                }

            }
        }
        if(currentRow<matrix.length){
            int col = currentCol;
            for (int i = currentRow+1; i <=matrix.length-1 ; i++) {
                col++;
                if(col<matrix[currentCol].length){

                    if(matrix[i][col]=='q'){
                        return false;
                    }
                }

            }
        }
        return clear;
    }

    private static boolean searchByDiagonalLeft(char[][] matrix, int currentRow, int currentCol) {
        boolean clear = true;
        if(currentRow>0){
            int col = currentCol;
            for (int i = currentRow-1; i >=0 ; i--) {
                col--;
                if(col>=0){
                    if(matrix[i][col]=='q'){
                        return false;
                    }
                }

            }
        }
        if(currentCol<matrix.length){
            int row = currentRow;
            for (int col = currentCol-1; col >=0 ; col--) {
                row++;
                if(row<matrix.length){


                    if(matrix[row][col]=='q'){
                        return false;
                    }
                }

            }
        }
        return clear;
    }

    public static boolean searchByCol(char[][] matrix, int currentRow, int currentCol) {
        boolean clear = true;
        if(currentRow>0){
            for (int i = currentRow-1; i >=0 ; i--) {
                if(matrix[i][currentCol] == 'q'){
                    return false;
                }
            }
        }
        if(currentRow<=matrix[currentRow].length){
            for (int i = currentRow+1; i <matrix[currentRow].length ; i++) {
                if(matrix[i][currentCol] == 'q'){
                    return false;
                }
            }
        }
        return clear;
    }

    public static boolean searchByRow(char[][] matrix, int currentRow, int currentCol) {
        boolean clear = true;
        if(currentCol>0){
            for (int i = currentCol-1; i >= 0 ; i--) {
                if(matrix[currentRow][i] == 'q'){
                    return false;
                }
            }
        }
        if(currentCol<matrix.length){
            for (int i = currentCol+1; i < matrix.length ; i++) {
                if(matrix[currentRow][i] == 'q'){
                    return false;
                }
            }
        }
        return clear;
    }

    public static char[][] fillMatrix(int rows, int cols, Scanner scan) {
        char[][] matrix = new char[rows][cols];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                matrix[row][col] = scan.next().charAt(0);
            }
            scan.nextLine();
        }
        return matrix;
    }
    public static void printMatrix(char[][] matrix) {
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {
                System.out.print(matrix[row][col]+ " ");
            }
            System.out.println();
        }
    }

}
public class Car extends Vehicle{
   private final double airConditioner = 0.9;

    protected Car(String type, double fuel, double litersPerKm, double tankCapacity) {

        super(type, fuel, litersPerKm, tankCapacity);
    }

    @Override
    public boolean drive(double distance, String value) {
        //0.9 liters for the car
        //Car 15 0.3
        //Drive Car 9

        double neededFuel = (distance*getLitersPerKm())+(distance*airConditioner);

        if(neededFuel <= getFuel()){
           double littersLeft = getFuel()-neededFuel;
            setFuel(littersLeft);

            return true;
        }


        return false;
    }

    @Override
    public void refuel(double litters) {
      /*  if(litters == 0){
            setFuel(-1);
            return;
        } */
        double refueled = litters;
        if(litters > 0){
            refueled =   getFuel()+litters;
        }

        setFuel(refueled);
    }

}

package aquarium.entities.aquariums;

public class SaltwaterAquarium extends BaseAquarium{


    private static final int SALT_WATER_CAPACITY = 25;
    public SaltwaterAquarium(String name) {
        super(name, SALT_WATER_CAPACITY);
    }


  /* @Override
    public int getCapacity() {
        return this.SALT_WATER_CAPACITY;
    }*/
}

package CounterStriker.core;

import CounterStriker.models.field.Field;
import CounterStriker.models.field.FieldImpl;
import CounterStriker.models.guns.Gun;
import CounterStriker.models.guns.Pistol;
import CounterStriker.models.guns.Rifle;
import CounterStriker.models.players.CounterTerrorist;
import CounterStriker.models.players.Player;
import CounterStriker.models.players.Terrorist;
import CounterStriker.repositories.GunRepository;
import CounterStriker.repositories.PlayerRepository;
import CounterStriker.repositories.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static CounterStriker.common.ExceptionMessages.*;
import static CounterStriker.common.OutputMessages.*;

public class ControllerImpl implements Controller {

    private GunRepository guns;
    private PlayerRepository players;
    private Field fields;

    public ControllerImpl() {
        this.guns = new GunRepository();
        this.players = new PlayerRepository();
        this.fields = new FieldImpl();
    }


    @Override
    public String addGun(String type, String name, int bulletsCount) {

        Gun gun;
      /*  if(gun != null){
            throw new IllegalArgumentException(INVALID_GUN_TYPE);
        }*/

        if(type.equals("Pistol")){
            gun = new Pistol(name, bulletsCount);
        }else if(type.equals("Rifle")){
            gun = new Rifle(name, bulletsCount);
        }else{
            throw new IllegalArgumentException(INVALID_GUN_TYPE);
        }
        guns.add(gun);

        return String.format(SUCCESSFULLY_ADDED_GUN, name);
    }

    @Override
    public String addPlayer(String type, String username, int health, int armor, String gunName) {

       /* if(player != null){
            throw  new NullPointerException(INVALID_PLAYER_NAME);
        }*/

        Gun gun = guns.findByName(gunName);
        if(gun == null){
            throw  new NullPointerException(GUN_CANNOT_BE_FOUND);
        }

        Player player;
        if(type.equals("Terrorist")){
            player = new Terrorist(username, health, armor, gun);
        }else if (type.equals("CounterTerrorist")){
            player = new CounterTerrorist(username, health, armor, gun);
        }else{
            throw new IllegalArgumentException(INVALID_PLAYER_TYPE);
        }


        players.add(player);

        return String.format(SUCCESSFULLY_ADDED_PLAYER, username);
    }

    @Override
    public String startGame() {


        return fields.start(players.getModels());
    }

    @Override
    public String report() {
        StringBuilder builder = new StringBuilder();
        List<Player> listWithCT = separatePlayer(players);
        List<Player> listWithT = separatePlayerT(players);

        listWithCT = listWithCT.stream().sorted(Comparator.comparing(Player::getHealth).reversed().thenComparing(Player::getUsername)).collect(Collectors.toList());
        listWithT = listWithT.stream().sorted(Comparator.comparing(Player::getHealth).reversed().thenComparing(Player::getUsername)).collect(Collectors.toList());
        for (Player player : listWithCT) {
            builder.append(player.toString()).append(System.lineSeparator());

        }

        for (Player player : listWithT) {
            builder.append(player.toString()).append(System.lineSeparator());
        }
        return builder.toString().trim();
    }

    private List<Player> separatePlayerT(PlayerRepository players) {
        List<Player> newList = new ArrayList<>();
        for (Player model : players.getModels()) {
            if(model.getClass().getSimpleName().equals("Terrorist")){
                newList.add(model);
            }
        }
     return newList;
       // return newList.stream().sorted(Comparator.comparing(p -> p.getUsername())).collect(Collectors.toList());
    }


    private List<Player> separatePlayer(PlayerRepository players) {
        List<Player> newList = new ArrayList<>();
        for (Player model : players.getModels()) {
            if(model.getClass().getSimpleName().equals("CounterTerrorist")){
                newList.add(model);
            }
        }
        return newList;
        //return newList.stream().sorted(Comparator.comparing(p -> p.getUsername())).collect(Collectors.toList());
    }
}

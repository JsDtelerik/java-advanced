package day_01_StackAndQueues;

import java.util.ArrayDeque;
import java.util.Scanner;

public class PrinterQueue {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String command = scan.nextLine();
        ArrayDeque<String> printer = new ArrayDeque<>();
        while (!command.equals("print")){
            if(!command.equals("cancel")){
                printer.offer(command);
            }else{
                if(!printer.isEmpty()){
                    System.out.println("Canceled " + printer.poll());
                }else{
                    System.out.println("Printer is on standby");
                }
            }

            command = scan.nextLine();
        }
        for (String s : printer) {
            System.out.println(s);
        }
    }
}

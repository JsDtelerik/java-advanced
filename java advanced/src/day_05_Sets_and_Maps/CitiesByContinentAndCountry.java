package day_05_Sets_and_Maps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class CitiesByContinentAndCountry {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        LinkedHashMap<String, LinkedHashMap<String, List<String>>> atlas = new LinkedHashMap<>();

        int n = Integer.parseInt(reader.readLine());
        for (int i = 0; i < n; i++) {
            String[] input = reader.readLine().split("\\s+");

            if(atlas.containsKey(input[0])){
                if(atlas.get(input[0]).containsKey(input[1])){
                    atlas.get(input[0]).get(input[1]).add(input[2]);
                }else{
                    atlas.get(input[0]).put(input[1], new ArrayList<>());
                    atlas.get(input[0]).get(input[1]).add(input[2]);
                }
            }else{
                atlas.put(input[0], new LinkedHashMap<>());
                atlas.get(input[0]).put(input[1], new ArrayList<>());
                atlas.get(input[0]).get(input[1]).add(input[2]);
            }
        }

        for (var entry : atlas.entrySet()) {
            String continent = entry.getKey();
            System.out.printf("%s:%n", continent);
            LinkedHashMap<String, List<String>> mapOfValues = entry.getValue();

            for (var inner : mapOfValues.entrySet()) {

                System.out.println("  " + inner.getKey() + " -> " + String.join(", ", inner.getValue()));
            }
        }

    }
}

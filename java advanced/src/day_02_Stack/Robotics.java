package day_02_Stack;

import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Robotics {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        LinkedHashMap<String, Integer> robots = new LinkedHashMap<>();

        String[] input = scan.nextLine().split(";");
        robots = addRobotsInfo(input);
        ArrayDeque<String> assemblyLIneRobots = addRobotsToTheLine(robots);
        ArrayDeque<String> products = new ArrayDeque<>();
        String[] time = scan.nextLine().split(":");
        int timeInSeconds = calculatedTimeInSeconds(time);
        String command = scan.nextLine();
        while (!command.equals("End")){
            products.offer(command);
            command = scan.nextLine();
        }
        int updateTimer = 1;
        LinkedHashMap<String, Integer> engagedRobotsList = new LinkedHashMap<>();
        while(!products.isEmpty()){
            String currentProduct = products.poll();
            if(!assemblyLIneRobots.isEmpty()){

                String engagedRobot = assemblyLIneRobots.poll();
                engagedRobotsList.put(engagedRobot, robots.get(engagedRobot));
                String timeReversedToRealHours = reCalculateTime(timeInSeconds+updateTimer);
                System.out.printf("%s - %s [%s]%n", engagedRobot, currentProduct, timeReversedToRealHours);
            }else{
                products.addLast(currentProduct);
            }

            for (String key : engagedRobotsList.keySet()) {
                engagedRobotsList.put(key, engagedRobotsList.get(key)-1);
                if(engagedRobotsList.get(key) == 0){
                    assemblyLIneRobots.push(key);

                }
            }
            updateTimer++;
        }

    }

    private static String reCalculateTime(int secondsU) {
        int hours = (secondsU /3600) %24;
        int minutes = (secondsU /60) %60;
        int seconds = (int) (TimeUnit.SECONDS.toSeconds(secondsU) - (TimeUnit.SECONDS.toMinutes(secondsU)*60));;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds).trim();
    }

    private static ArrayDeque<String> addRobotsToTheLine(LinkedHashMap<String, Integer> robots) {
        ArrayDeque<String> assemblyLine = new ArrayDeque<>();
        for (String robot : robots.keySet()) {
            assemblyLine.offer(robot);
        }
        return assemblyLine;
    }

    private static int calculatedTimeInSeconds(String[] time) {

        return Integer.parseInt(time[0])*3600 + Integer.parseInt(time[1])*60 + Integer.parseInt(time[2]);
    }

    private static LinkedHashMap<String, Integer> addRobotsInfo(String[] input) {
        LinkedHashMap<String, Integer> informationForRobots = new LinkedHashMap<>();
        for (String s : input) {
            String[] info = s.split("-");
            informationForRobots.put(info[0], Integer.parseInt(info[1]));
        }
        return informationForRobots;
    }
}
public abstract class Vehicle {

    //"Car {fuel quantity} {liters per km}"
    private String type;
    private double fuel;
    private double litersPerKm;
    private double tankCapacity;


    protected Vehicle(String type, double fuel, double litersPerKm, double tankCapacity) {
        this.type = type;

      this.fuel = fuel;
        // this.setFuel(fuel);
        this.litersPerKm = litersPerKm;
        this.tankCapacity = tankCapacity;
       // this.setTankCapacity(tankCapacity);
    }


    public abstract boolean drive(double distance, String value);
    public abstract void refuel(double litters);

    public double getLitersPerKm() {
        return litersPerKm;
    }

    public double getFuel() {
        return fuel;
    }

    protected void setFuel(double fuel) {
        if(fuel <= 0 ){
            throw new IllegalArgumentException("Fuel must be a positive number");
        }
        if((this.tankCapacity - fuel) < 0){
            throw new IllegalArgumentException("Cannot fit fuel in tank");
        }

        this.fuel = fuel;
    }
   // public void setLitersPerKm(double litersPerKm) {

   //     this.litersPerKm = litersPerKm;
   // }

    public String getType() {
        return type;
    }

    protected void setTankCapacity(double tankCapacity) {

        this.tankCapacity = tankCapacity;
    }

    public double getTankCapacity() {
        return tankCapacity;
    }
}

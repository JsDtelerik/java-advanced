import java.util.LinkedHashSet;

public class Mission implements CommandoImpl{
//holds code name
// and a state (Enumeration: "inProgress" or "finished").

    private String codeName;
    private State state;



    public  Mission (String codeName, State state){
        this.codeName = codeName;
        this.state = state;
    }

    public void completeMission(){
        this.setState(State.FINISHED);

    }

    public String getCodeName() {
        return codeName;
    }

    public String getState() {
        return state.getValue();
    }





    public void addMission(Mission mission){
        missions.add(mission);
        if(mission.getState().equals("Finished")){
            mission.completeMission();
        }

    }

    public LinkedHashSet<Mission> getMissions(){

        return this.missions;
    }



    private void setState(State state) {

        this.state = state;
    }

    //Missions:
    //  Code Name: Freedom State: inProgress
   @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
       /* builder.append("Missions:");
        boolean first = true;
        for (Mission mission : missions) {
            if(first){
               builder.append(System.lineSeparator());
               first = false;
            }*/
        builder.append(" Code Name: ").append(getCodeName()).append(" State: ").append(getState());

       // }


        return builder.toString();
    }
}

package day_19_exam_preparation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Bomb {
    public static int sapperRow = 0, sapperCol = 0, bombs = 0;
    public static boolean atTheExit = false;
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int  size = Integer.parseInt(reader.readLine());
        List<String> directions = Arrays.stream(reader.readLine().split(",")).collect(Collectors.toList());
        String[][] matrix = fillMatrix(size, reader);

        for (int i = 0; i < directions.size(); i++) {
            String direction = directions.get(i);
            switch (direction){
                case "up":
                    moveSapper(matrix, sapperRow-1, sapperCol);
                    break;
                case "down":
                    moveSapper(matrix, sapperRow+1, sapperCol);
                    break;
                case "left":
                    moveSapper(matrix, sapperRow, sapperCol-1);
                    break;
                case "right":
                    moveSapper(matrix, sapperRow, sapperCol+1);
                    break;
            }
            if(atTheExit){
                break;
            }
            if(bombs<=0){
                break;
            }
        }

        if(atTheExit){
            System.out.printf("END! %d bombs left on the field", bombs);
        }else if(bombs<=0){
            System.out.print("Congratulations! You found all bombs!");
        }else{
            System.out.printf("%d bombs left on the field. Sapper position: (%d,%d)", bombs, sapperRow, sapperCol);
        }

    }

    private static void moveSapper(String[][] matrix, int row, int col) {
        boolean outOfBound = checkBorders(row, col, matrix);
        if(outOfBound){
            return;
        }
        if(matrix[row][col].equals("B")){
            bombs--;
            matrix[row][col] = "+";
            System.out.println("You found a bomb!");

        }else if (matrix[row][col].equals("e")){
            atTheExit =true;

        }
        sapperRow = row;
        sapperCol = col;
    }

    private static boolean checkBorders(int row, int col, String[][] matrix) {
        return row < 0 || row > matrix.length-1 || col < 0 || col > matrix.length-1;
    }

    private static String[][] fillMatrix(int size, BufferedReader reader) throws IOException {
        String[][] matrix = new String[size][size];
        for (int i = 0; i < size; i++) {
            String[] line = reader.readLine().split("\\s+");
            for (int j = 0; j < line.length; j++) {
                matrix[i][j] = line[j];
                if(line[j].equals("s")){
                sapperRow = i;
                sapperCol = j;
                }else if (line[j].equals("B")){
                    bombs++;
                }
            }
        }
return matrix;

    }
}

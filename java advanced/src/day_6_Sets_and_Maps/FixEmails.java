package day_6_Sets_and_Maps;

import java.util.LinkedHashMap;
import java.util.Scanner;

public class FixEmails {
    public static void main(String[] args) {
         Scanner scan = new Scanner(System.in);
         LinkedHashMap<String, String> data = new LinkedHashMap<>();
         while(true){
            String name = scan.nextLine();
            if (name.equals("stop")){
                break;
            }

            String email = scan.nextLine();

            if(!(email.contains(".com") || email.contains(".us") || email.contains(".uk"))){
                data.put(name, email);
            }
         }

         data.entrySet().stream().forEach(k -> System.out.printf("%s -> %s%n", k.getKey(), k.getValue()));
    }
}

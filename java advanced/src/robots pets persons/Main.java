import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String[] command = scan.nextLine().split("\\s+");
        List<Identifiables> citizens = new ArrayList<>();
        while (!command[0].equals("End")){

           //Citizen Peter 22 9010101122 10/10/1990
            //Pet Sharo 13/11/2005
            //Robot
            switch (command[0]){
                case "Citizen":
                    citizens.add(new Citizen(command[1], Integer.parseInt(command[2]), command[3], command[4]));
                    break;
                case "Pet":
                    citizens.add(new Pet(command[1], command[2]));
                    break;
                case "Robot":
                    citizens.add(new Robot(command[1], command[2]));
                    break;
            }



            command = scan.nextLine().split("\\s+");
        }

        String dateOfBirth = scan.nextLine();
        boolean found = false;
        for (Identifiables citizen : citizens) {
            if(citizen.getBirthDate().endsWith(dateOfBirth)){
                System.out.println(citizen.getBirthDate());
                found = true;
            }

        }
        if(!found){
            System.out.println("<no output>");
        }

    }
}

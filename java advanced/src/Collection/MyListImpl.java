import java.util.ArrayList;
import java.util.List;

public class MyListImpl extends Collection implements MyList{



    //private List<String> myListCollection;
   // protected MyListImpl() {

   //     super();
       // this.myListCollection = new ArrayList<>();
   // }




//•	An add(String) method which adds an item to the start of the collection.
//•	A remove() method which removes the first element in the collection.
//•	A used field which displays the size of elements currently in the collection.

    @Override
    public int getUsed() {

        return super.getItems().size();
    }

    @Override
    public String remove() {

        return super.getItems().remove(0);
    }

    @Override
    public int add(String element) {
        super.getItems().add(0, element);
        return 0;
    }
}

import java.util.*;

public class LieutenantGeneral extends Private implements LieutenantGeneralImpl {

        private List<Integer> soldiersIds;
        private LinkedHashSet<Soldier> currentPrivates;
    public LieutenantGeneral(int id, String firstName, String lastName, double salary, List<Integer> soldiersIds) {
        super(id, firstName, lastName, salary);
        this.soldiersIds = soldiersIds;
        this.currentPrivates = new LinkedHashSet<>();
    }

    @Override
    public void addPrivate(Soldier priv) {

        this.currentPrivates.add(priv);

        //privates.stream().sorted();
    }



    public List<Integer> getSoldiersIds() {

        return soldiersIds;
    }

        //Name: John Jonson Id: 3 Salary: 100.00
        //Privates:
        //  Name: Tom Tomson Id: 222 Salary: 80.08
        //  Name: Peter Petrov Id: 1 Salary: 22.22
    @Override
    public String toString() {
        String editedSalary = String.format("%.2f", salary());
        StringBuilder builder = new StringBuilder();
        builder.append("Name: ").append(firstName()).append(" ").append(lastName()).append(" Id: ").append(id()).append(" Salary: ")
                .append(editedSalary).append(System.lineSeparator());
        builder.append("Privates:");
        boolean first = true;
        LinkedHashSet<Soldier> sortedPrivates = reversePrivateByValue();
        int size = currentPrivates.size();
        for (Soldier subordinateTroops : sortedPrivates) {
            if(first){
                builder.append(System.lineSeparator());
                first = false;
            }
            if(size-- > 1){
                builder.append("  ").append(subordinateTroops.toString()).append(System.lineSeparator());
            }else{
                builder.append("  ").append(subordinateTroops.toString());
            }


        }

        return builder.toString();
    }

    private LinkedHashSet<Soldier> reversePrivateByValue() {
        List<Integer> value = new ArrayList<>();
        for (Soldier pr : currentPrivates) {
            value.add(pr.getId());
        }
        Collections.sort(value);
        Collections.reverse(value);
        LinkedHashSet<Soldier> reversed = new LinkedHashSet<>();

        for (Integer integer : value) {
            for (Soldier aPrivate : currentPrivates) {
                if(integer == aPrivate.getId()){
                    reversed.add(aPrivate);
            }
        }
        }

        return reversed;

    }
}

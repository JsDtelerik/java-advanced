public class Robot implements Identifiables, Identifiable{

    private String id;
    private String model;

    public Robot(String id, String model) {
        this.id = id;
        this.model = model;
    }

    @Override
    public String getName() {
        return "Robots do not have a name!";
    }

    @Override
    public String getBirthDate() {
        return "Robots do not have a birth day!";
    }


    @Override
    public String getId() {
        return this.id;
    }

    public String getModel() {
        return this.model;
    }
}

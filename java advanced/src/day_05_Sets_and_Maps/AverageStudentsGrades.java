package day_05_Sets_and_Maps;

import java.util.*;

public class AverageStudentsGrades {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        TreeMap<String, List<Double>> students = new TreeMap<>();

        int n = Integer.parseInt(scan.nextLine());
        for (int i = 0; i < n; i++) {
            String[] input = scan.nextLine().split("\\s+");
            if(students.containsKey(input[0])){
                 students.get(input[0]).add(Double.parseDouble(input[1]));
            }else{
                students.put(input[0], new ArrayList<>());
                students.get(input[0]).add(Double.parseDouble(input[1]));
            }

        }
    //.sorted(Comparator.comparing(Map.Entry::getKey))
      /*  students.entrySet().stream().forEach((k) -> {
          System.out.printf("%s -> ", k.getKey());
          for (Double mark : k.getValue()) {
              System.out.printf("%.2f ", mark);
          }
          ;
          System.out.printf("(avg: %.2f)%n", k.getValue().stream().mapToDouble(d -> d).average().orElse(0.00));
      });*/

        for (var student : students.entrySet()) {
           //Stephan -> 5.20 3.20 (avg: 4.20)
            System.out.print(student.getKey()+ " -> ");
            double scores = 0.0;

            for (int i = 0; i < student.getValue().size(); i++) {
                scores  += student.getValue().get(i);
                System.out.printf("%.2f ", student.getValue().get(i));
            }


            double result = scores/student.getValue().size();
            System.out.printf("(avg: %.2f)%n", result);
        }



    }
}

package day_02_Stack;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class TestT {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int seconds = Integer.parseInt(scan.nextLine());

        int hours = (seconds/3600)%24;

        int minutes = (seconds/60)%60;

        int sec = (int) (TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds)*60));
                System.out.printf("%2d:%02d:%02d", hours, minutes, sec);
    }
}

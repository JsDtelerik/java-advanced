package day_19_exam_preparation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.stream.Collectors;

public class MagicBox {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));


        ArrayDeque<Integer> firstBox = Arrays.stream(reader.readLine().split("\\s+")).map(Integer::parseInt)
                .collect(Collectors.toCollection(ArrayDeque::new));

        ArrayDeque<Integer> secondBox = new ArrayDeque<>();

        Arrays.stream(reader.readLine().split("\\s+")).map(Integer::parseInt).forEach(secondBox::push);

        int claimedItemsValue = 0;
        while (!(firstBox.isEmpty() || secondBox.isEmpty())){
            int firstNumber = firstBox.peek();
            int secondNumber = secondBox.pop();
            int sum = firstNumber+secondNumber;

            if(sum % 2 == 0){
                claimedItemsValue +=sum;
                firstBox.poll();
            }else{
                firstBox.offer(secondNumber);
            }

        }
        if(firstBox.isEmpty()){
            System.out.println("First magic box is empty.");
        }else{
            System.out.println("Second magic box is empty.");
        }

        if(claimedItemsValue>=90){
            System.out.printf("Wow, your prey was epic! Value: %d", claimedItemsValue);
        }else{
            System.out.printf("Poor prey... Value: %d", claimedItemsValue);
        }


    }
}

package restaurant.entities.tables;


import restaurant.common.enums.TableType;
import restaurant.entities.drinks.interfaces.Beverages;
import restaurant.entities.healthyFoods.interfaces.HealthyFood;
import restaurant.entities.tables.interfaces.Table;
import static restaurant.common.ExceptionMessages.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class BaseTable implements Table {
    private static boolean isReservedTable = false;
    private static double allPeople = 0;
    private static int numberOfPeople = 0;



    protected Collection<HealthyFood> healthyFood;
    protected Collection<Beverages> beverage;
    private int number;
    private int size;

    private double pricePerPerson;
   // private boolean isReservedTable;
    //private double allPeople;
//int number, int size, double pricePerPerson
    protected BaseTable( int number, int size , double pricePerPerson) {

        this.number = number;
        this.setSize(size);
        this.pricePerPerson = pricePerPerson;
        this.healthyFood = new ArrayList<>();
        this.beverage = new ArrayList<>();


    }

    private void setSize(int size) {

        //o	It can’t be less than zero. In these cases, t
        // hrow an IllegalArgumentException with message "Size has to be greater than 0!".

        if(size < 0){
            throw new IllegalArgumentException(INVALID_TABLE_SIZE);
        }
        this.size = size;
    }

    protected void setNumberOfPeople(int numberOfPeople) {
//o	It can’t be less than or equal to 0. In these cases,
// throw an IllegalArgumentException with message "Cannot place zero or less people!".
        if(numberOfPeople <= 0){
            throw new IllegalArgumentException(INVALID_NUMBER_OF_PEOPLE);
        }

        this.numberOfPeople = numberOfPeople;
    }

    @Override
    public int getTableNumber() {
        return this.number;
    }

    @Override
    public int getSize() {
        return this.size;
    }

    @Override
    public int numberOfPeople() {
        return this.numberOfPeople;
    }

    @Override
    public double pricePerPerson() {
        return this.pricePerPerson;
    }

    @Override
    public boolean isReservedTable() {
        return false;
    }

    @Override
    public double allPeople() {
        double priceForAllPeople = this.numberOfPeople*this.pricePerPerson;

        return priceForAllPeople;
    }

    @Override
    public void reserve(int numberOfPeople) {
      //  this.setNumberOfPeople(numberOfPeople);
        //this.isReservedTable = true;
    }

    @Override
    public void orderHealthy(HealthyFood food) {

    }

    @Override
    public void orderBeverages(Beverages beverages) {

    }

    @Override
    public double bill() {
        return 0;
    }

    @Override
    public void clear() {

    }

    public String tableType(){
        return null;
    }
    @Override
    public String tableInformation() {
        StringBuilder builder = new StringBuilder();

        builder.append("Table - ").append(this.number).append(System.lineSeparator())
                .append("Size -").append(this.size).append(System.lineSeparator())
                .append("Type - ").append(tableType()).append(System.lineSeparator())
                .append("All price - ").append(bill());

        return builder.toString();
    }


}

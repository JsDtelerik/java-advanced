public interface SoldierImpl {

    //general class for soldiers, holding id (int), first name and last name

    int id();
    String firstName();
    String lastName();
}

package CounterStriker.models.field;

import CounterStriker.models.guns.Gun;
import CounterStriker.models.players.Player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import static CounterStriker.common.OutputMessages.*;

public class FieldImpl implements Field{
    public static int countDeadsT = 0, countDeadCT = 0;
   private Collection <Player> terrorist;
   private Collection <Player> counterTerrorist;

    public FieldImpl() {

        this.terrorist = new ArrayList<>();
        this.counterTerrorist = new ArrayList<>();
    }


    @Override
    public String start(Collection<Player> pl) {
        separateTeams(pl);
        while(true){
            shootingRange(terrorist, counterTerrorist);

            if(countDeadCT == counterTerrorist.size()){
                break;
            }

            shootingRange(counterTerrorist, terrorist);

            if(countDeadsT == terrorist.size()){
                break;
            }

        }


        long couneterCT = counterTerrorist.stream().filter(ct -> ct.getHealth() <= 0).count();
        long counterT = terrorist.stream().filter(t -> t.getHealth() <= 0).count();
        if(couneterCT > counterT){
            return String.format(TERRORIST_WINS);
        }



        return String.format(COUNTER_TERRORIST_WINS);
    }

    private void shootingRange(Collection<Player> shooters, Collection<Player> defenders) {
        for (Player shooter : shooters) {
            for (Player defender : defenders) {
                if(shooter.getGun().getBulletsCount() > 0){
                    if(shooter.isAlive()){

                        if(defender.isAlive()){
                            int damage = shooter.getGun().fire();
                            defender.takeDamage(damage);
                            if(defender.getHealth() <= 0){
                                if(defender.getClass().getSimpleName().equals("Terrorist")){
                                    countDeadsT++;
                                }else if(defender.getClass().getSimpleName().equals("CounterTerrorist")){
                                    countDeadCT++;
                                }
                            }
                           // defender.isAlive();
                        }
                    }

                }
            }
        }
    }


    private void separateTeams(Collection<Player> pl) {

        for (Player player : pl) {
            if(player.getClass().getSimpleName().equals("Terrorist")){
                terrorist.add(player);
            }else{
                counterTerrorist.add(player);
            }
        }

    }


}

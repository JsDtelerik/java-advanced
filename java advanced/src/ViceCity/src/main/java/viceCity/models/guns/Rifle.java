package viceCity.models.guns;

public class Rifle extends BaseGun{

    private static final int BULLETS_PER_BARREL = 50;
    private static final int TOTAL_BULLETS = 500;

    public Rifle(String name) {
        super(name, BULLETS_PER_BARREL, TOTAL_BULLETS);
    }

    @Override
    public int fire() {
        int bulletsLeftInTheMagazine = (getBulletsPerBarrel()-5);
        setBulletsPerBarrel(bulletsLeftInTheMagazine);
        return 5;
    }


    @Override
    public void setMagazineAmmunition(int newAm) {


        setBulletsPerBarrel(newAm);
    }
    @Override
    public void setStorageAmmunition(int newStorageAm) {
        setTotalBullets(newStorageAm);
    }

}

package glacialExpedition.core;

import glacialExpedition.models.explorers.AnimalExplorer;
import glacialExpedition.models.explorers.Explorer;
import glacialExpedition.models.explorers.GlacierExplorer;
import glacialExpedition.models.explorers.NaturalExplorer;
import glacialExpedition.models.mission.Mission;
import glacialExpedition.models.mission.MissionImpl;
import glacialExpedition.models.states.State;
import glacialExpedition.models.states.StateImpl;
import glacialExpedition.repositories.ExplorerRepository;
import glacialExpedition.repositories.StateRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static glacialExpedition.common.ConstantMessages.*;
import static glacialExpedition.common.ExceptionMessages.*;

public class ControllerImpl implements Controller {
    private static int explored = 0;

    private ExplorerRepository explorerRepository;
    private StateRepository stateRepository;

    public ControllerImpl() {
        this.explorerRepository = new ExplorerRepository();
        this.stateRepository = new StateRepository();
    }


    @Override
    public String addExplorer(String type, String explorerName) {
        Explorer explorer;
        if(type.equals("AnimalExplorer")){
            explorer = new AnimalExplorer(explorerName);
        }else if(type.equals("GlacierExplorer")){
            explorer = new GlacierExplorer(explorerName);
        }else if(type.equals("NaturalExplorer")){
            explorer = new NaturalExplorer(explorerName);
        }else{
            throw new IllegalArgumentException(EXPLORER_INVALID_TYPE);
        }

        explorerRepository.add(explorer);
        return String.format(EXPLORER_ADDED, type, explorerName);
    }

    @Override
    public String addState(String stateName, String... exhibits) {


        State state = new StateImpl(stateName);
        for (String exhibit : exhibits) {
            state.getExhibits().add(exhibit);
        }

        this.stateRepository.add(state);

        return String.format(STATE_ADDED, stateName);
    }

    @Override
    public String retireExplorer(String explorerName) {
        Explorer removeExpl = explorerRepository.byName(explorerName);
        if(removeExpl == null){
            throw new IllegalArgumentException(String.format(EXPLORER_DOES_NOT_EXIST, explorerName));
        }

        this.explorerRepository.remove(removeExpl);

        return String.format(EXPLORER_RETIRED, removeExpl.getName());
    }

    @Override
    public String exploreState(String stateName) {

        List<Explorer> suitable = new ArrayList<>();
        for (Explorer explorer : explorerRepository.getCollection()) {
            if(explorer.getEnergy() > 50){
                suitable.add(explorer);
            }
        }

        if(suitable.isEmpty()){
            throw new IllegalArgumentException(STATE_EXPLORERS_DOES_NOT_EXISTS);
        }

       State state =  stateRepository.byName(stateName);
        MissionImpl mission = new MissionImpl();
        mission.explore(state, suitable);
        explored++;
        stateRepository.remove(state);
        return String.format(STATE_EXPLORER, stateName, mission.getRetired());
    }

    @Override
    public String finalResult() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format(FINAL_STATE_EXPLORED, explored)).append(System.lineSeparator())
                .append(FINAL_EXPLORER_INFO).append(System.lineSeparator());
        for (Explorer explorer : explorerRepository.getCollection()) {
            if(explorer.getEnergy()>0){
                builder.append(String.format(FINAL_EXPLORER_NAME, explorer.getName())).append(System.lineSeparator())
                        .append(String.format(FINAL_EXPLORER_ENERGY, explorer.getEnergy())).append(System.lineSeparator());
                if(explorer.getSuitcase().getExhibits().size() == 0){
                    builder.append(String.format(FINAL_EXPLORER_SUITCASE_EXHIBITS, "None")).append(System.lineSeparator());
                }else{
                    int count = 0;
                    for (String exhibit : explorer.getSuitcase().getExhibits()) {
                        if(count == 0 && explorer.getSuitcase().getExhibits().size() == 1){
                            builder.append(String.format(FINAL_EXPLORER_SUITCASE_EXHIBITS, exhibit)).append(System.lineSeparator());
                            count++;
                        }else if(count == 0 && explorer.getSuitcase().getExhibits().size() > 1){
                            builder.append(String.format(FINAL_EXPLORER_SUITCASE_EXHIBITS, exhibit)).append(FINAL_EXPLORER_SUITCASE_EXHIBITS_DELIMITER);
                            count++;
                        }else{
                            if(count < explorer.getSuitcase().getExhibits().size()-1){
                                builder.append(String.format("%s", exhibit)).append(FINAL_EXPLORER_SUITCASE_EXHIBITS_DELIMITER);
                            }else{
                                builder.append(String.format("%s", exhibit)).append(System.lineSeparator());
                            }

                            count++;

                        }

                    }
                }
            }

        }

        return builder.toString().trim();
    }
}

package aquarium.entities.decorations;

public class Ornament extends BaseDecoration{

    private static final int ORNAMENT_COMFORT_VALUE = 1;
    private static final double ORNAMENT_PRICE_VALUE = 5;
    public Ornament() {
        super(ORNAMENT_COMFORT_VALUE, ORNAMENT_PRICE_VALUE);
    }
}

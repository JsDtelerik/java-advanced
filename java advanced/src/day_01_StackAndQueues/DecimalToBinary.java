package day_01_StackAndQueues;

import java.util.ArrayDeque;
import java.util.Scanner;

public class DecimalToBinary {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int num = Integer.parseInt(scan.nextLine());
        if(num == 0){
            System.out.println("0");
        }else{
            ArrayDeque<Integer> binaryNum = new ArrayDeque<>();
            while (num != 0){
                binaryNum.push(num % 2);
                num /= 2;
            }
            while (!binaryNum.isEmpty()){
                System.out.print(binaryNum.pop());
            }

        }

    }
}

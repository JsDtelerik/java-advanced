public enum Season {

    SUMMER("summer"),
    WINTER("winter"),
    SPRING("spring"),
    AUTUMN("autumn");

    private String value;

    Season(String value) {
        this.value = value;
    }

    public String getValue(){
        return value;
    }

}

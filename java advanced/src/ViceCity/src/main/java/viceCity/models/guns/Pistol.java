package viceCity.models.guns;

import static viceCity.common.ExceptionMessages.BULLETS_LESS_THAN_ZERO;
import static viceCity.common.ExceptionMessages.TOTAL_BULLETS_LESS_THAN_ZERO;

public class Pistol extends BaseGun{

    private static final int BULLETS_PER_BARREL = 10;
    private static final int TOTAL_BULLETS = 100;

    public Pistol(String name) {
        super(name, BULLETS_PER_BARREL, TOTAL_BULLETS);
    }

    @Override
    public int fire() {
       int bulletsLeftInTheMagazine = (getBulletsPerBarrel()-1);
       setBulletsPerBarrel(bulletsLeftInTheMagazine);
        return 1;
    }

@Override
    public void setMagazineAmmunition(int newAm) {


        setBulletsPerBarrel(newAm);
    }
    @Override
    public void setStorageAmmunition(int newStorageAm) {
       setTotalBullets(newStorageAm);
    }


}

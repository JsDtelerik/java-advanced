package day_04_Multidimensional;

import java.util.Arrays;
import java.util.Scanner;

public class MatrixOfPalindromes {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int row = scan.nextInt();
        int col = scan.nextInt();
        String[][] matrix = fillMatrixWithPalindromes(row, col);
        printPalindromesMatrix(matrix);

    }

    public static void printPalindromesMatrix(String[][] matrix) {
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {
                System.out.print(matrix[row][col] + " ");
            }
            System.out.println();
        }
    }

    public static String[][] fillMatrixWithPalindromes(int rows, int cols) {
        String[][] matrix = new String[rows][cols];

        for (int row = 0; row < rows; row++) {
            char startEnd = (char) ('a'+row);
            for (int col = 0; col <cols ; col++) {
                char middle = (char) (startEnd+col);

                matrix[row][col] = ""+startEnd+middle+startEnd;
            }
        }
        return matrix;
    }
}

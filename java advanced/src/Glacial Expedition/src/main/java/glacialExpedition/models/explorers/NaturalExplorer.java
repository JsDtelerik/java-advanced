package glacialExpedition.models.explorers;

public class NaturalExplorer extends BaseExplorer{

    private static final double INITIAL_ENERGY = 60;
    public NaturalExplorer(String name) {
        super(name, INITIAL_ENERGY);
    }

    @Override
    public void search() {
        if(getEnergy() >= 7){
            setEnergy(getEnergy()-7);
        }else {
            setEnergy(0);
        }
    }
    @Override
    public boolean canSearchReal() {
        if(this.getEnergy() >=7 ){
            return true;
        }
        return false;
    }

    @Override
    public boolean canSearchSet() {
        if(this.getEnergy() >= 7){
            return true;
        }else{
            setEnergy(0);
            return false;
        }

    }

}

package viceCity.models.guns;

public interface Gun {
    String getName();

    int getBulletsPerBarrel();

    boolean canFire();

    int getTotalBullets();

    int fire();
     void setMagazineAmmunition(int newAm);

     void setStorageAmmunition(int newStorageAm) ;
     void setCanFire(boolean canFire);



}

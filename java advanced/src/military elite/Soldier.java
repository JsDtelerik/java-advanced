public abstract class Soldier {

    //Soldier should hold getId, getFirstName and getLastName).

    private int id;
    private String firstName;
    private String lastName;

    public Soldier(int id, String firstName, String lastName) {
        setId(id);
        setFirstName(firstName);
        setLastName(lastName);
    }

    protected void setLastName(String lastName) {
     //   if(validation(lastName)){
            this.lastName = lastName;
       // }else{
         //   throw new IllegalArgumentException("invalid name");
       // }

    }

   // protected abstract boolean validation(String name);

    protected void setFirstName(String firstName) {
       // if(validation(firstName)){
            this.firstName = firstName;
       //}else{
       //    throw new IllegalArgumentException("invalid name");
       //}
    }



    public void setId(int id) {
       if(id >= 0){
           this.id = id;
       }else{
           throw new IllegalArgumentException();
       }

    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }




    //@Override
    public boolean validation(String name) {

        if(name.length()<2){
            return false;
        }
        if(name.charAt(0)<65 && name.charAt(0)>90){
            return false;
        }
        for (int i = 0; i < name.length(); i++) {
            if(!Character.isAlphabetic(name.charAt(i))){
                return false;
            }

        }
        return true;
    }

}

package day_01_StackAndQueues;

import java.util.ArrayDeque;
import java.util.Scanner;

public class MatchingBrackets {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String mathExpression = scan.nextLine();

        ArrayDeque<Integer> bracketsIndex = new ArrayDeque<>();
        for (int i = 0; i < mathExpression.length(); i++) {
            char symbol = mathExpression.charAt(i);
            if(symbol == '('){
                bracketsIndex.push(i);
            }else if (symbol == ')'){
                int startIndex = bracketsIndex.pop();
                String contents = mathExpression.substring(startIndex, i+1);
                System.out.println(contents);
        }


        }


    }
}

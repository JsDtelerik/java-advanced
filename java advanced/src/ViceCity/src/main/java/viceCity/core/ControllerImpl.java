package viceCity.core;

import viceCity.core.interfaces.Controller;
import viceCity.models.guns.Gun;
import viceCity.models.guns.Pistol;
import viceCity.models.guns.Rifle;
import viceCity.models.neighbourhood.GangNeighbourhood;
import viceCity.models.players.CivilPlayer;
import viceCity.models.players.MainPlayer;
import viceCity.models.players.Player;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;

import static viceCity.common.ConstantMessages.*;

public class ControllerImpl extends GangNeighbourhood implements Controller{
    private MainPlayer mainPlayer;
    private Collection<Player> civilPlayers;
    private ArrayDeque<Gun> queue;
    public ControllerImpl(){
       this.mainPlayer = new MainPlayer();
        this.civilPlayers = new ArrayList<>();
        this.queue = new ArrayDeque<>();
    }


    @Override
    public String addPlayer(String name) {
        CivilPlayer newPlayer = new CivilPlayer(name);
        civilPlayers.add(newPlayer);
        return String.format(PLAYER_ADDED, name);
    }

    @Override
    public String addGun(String type, String name) {

        Gun gun = null;
        if(type.equals("Pistol")){
            gun = new Pistol(name);
        }else if (type.equals("Rifle")){
            gun = new Rifle(name);
        }else {
            return GUN_TYPE_INVALID;
        }
        queue.offer(gun);
        return String.format(GUN_ADDED, name, type);
    }

    @Override
    public String addGunToPlayer(String name) {
    if(queue.isEmpty()){
        return GUN_QUEUE_IS_EMPTY;
    }
    Gun gunToAdd = null;
    if(name.equals("Vercetti")){
        gunToAdd = queue.poll();
        mainPlayer.getGunRepository().getModels().add(gunToAdd);
        return String.format(GUN_ADDED_TO_MAIN_PLAYER, gunToAdd.getName(), "Tommy Vercetti");
    }

        for (Player civilPlayer : civilPlayers) {
            if(civilPlayer.getName().equals(name)){
                gunToAdd = queue.poll();
                civilPlayer.getGunRepository().getModels().add(gunToAdd);
                return String.format(GUN_ADDED_TO_CIVIL_PLAYER, gunToAdd.getName(), civilPlayer.getName());
            }

        }



        return CIVIL_PLAYER_DOES_NOT_EXIST;
    }

    @Override
    public String fight() {
       action(mainPlayer, civilPlayers);

        if(mainPlayer.isAlive() && getDeadCivilians() == 0){
            return FIGHT_HOT_HAPPENED;
        }

        StringBuilder builder = new StringBuilder();
        builder.append(FIGHT_HAPPENED).append(System.lineSeparator())
                .append(String.format(MAIN_PLAYER_LIVE_POINTS_MESSAGE, mainPlayer.getLifePoints())).append(System.lineSeparator())
                .append(String.format(MAIN_PLAYER_KILLED_CIVIL_PLAYERS_MESSAGE, getDeadCivilians())).append(System.lineSeparator())
                .append(String.format(CIVIL_PLAYERS_LEFT_MESSAGE, civilPlayers.size()-getDeadCivilians()));


        return builder.toString();
    }





}

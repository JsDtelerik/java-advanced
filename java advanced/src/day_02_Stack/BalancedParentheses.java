package day_02_Stack;

import java.util.ArrayDeque;
import java.util.Scanner;

public class BalancedParentheses {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String brackets = scan.nextLine();
        ArrayDeque<Character> openingBrackets = new ArrayDeque<>();
        boolean isBalanced = true;
        for (int i = 0; i < brackets.length(); i++) {
            char symbol = brackets.charAt(i);
            if(symbol == '(' || symbol == '{' || symbol == '['){
                openingBrackets.push(symbol);
            }else if ( symbol == ')' || symbol == ']' || symbol == '}'){
                if(openingBrackets.isEmpty()){
                    isBalanced =false;
                    break;
                }
                char lastOpeningBracket = openingBrackets.pop();
                if(lastOpeningBracket == '(' && symbol != ')'){
                    isBalanced =false;
                    break;
                } else if (lastOpeningBracket == '{' && symbol != '}'){
                    isBalanced =false;
                    break;
                } else if(lastOpeningBracket == '[' && symbol != ']'){
                    isBalanced =false;
                    break;
                }
            }
        }

       // boolean isBalanced = compareBothSequences(openingBrackets, closingBrackets);
if(isBalanced){
    System.out.println("YES");
}else{
    System.out.println("NO");
}

    }

    private static boolean compareBothSequences(ArrayDeque<Character> openingBrackets, ArrayDeque<Character> closingBrackets) {

        if(openingBrackets.size() != closingBrackets.size()){
            return false;
        }else{
            for (int i = 0; i < openingBrackets.size(); i++) {
                    char opening = openingBrackets.pop();
                    char closing = closingBrackets.poll();
                    if(opening == '(' && closing != ')'){
                        return false;
                     }
                    if (opening == '{' && closing != '}'){
                        return false;
                    }
                       if(opening == '[' && closing != ']'){
                        return false;
                    }
                       i= -1;
            }
        }

        return true;


    }

    private static ArrayDeque<Character> addClosingBrackets(String brackets) {
        ArrayDeque<Character> addBrackets = new ArrayDeque<>();
        for (int i = 0; i < brackets.length(); i++) {
            char symbol = brackets.charAt(i);
            if(symbol == ')' || symbol == '}' || symbol == ']'){
                addBrackets.offer(symbol);
            }

        }
        return addBrackets;
    }

    private static ArrayDeque<Character> addOpeningBrackets(String brackets) {
        ArrayDeque<Character> addBrackets = new ArrayDeque<>();
        for (int i = 0; i < brackets.length(); i++) {
            char symbol = brackets.charAt(i);
            if(symbol == '(' || symbol == '[' || symbol == '{'){
                addBrackets.push(symbol);
            }

        }
        return addBrackets;
    }
}

package guild;

import java.util.ArrayList;

import java.util.List;
import java.util.stream.Collectors;

public class Guild {
    private List<Player> rooster;
    private String name;
    private int capacity;


    public Guild(String name, int capacity) {
        this.name = name;
        this.capacity = capacity;
        this.rooster = new ArrayList<>();
    }

    public void addPlayer(Player player){
        if(this.rooster.size()< this.capacity){
            this.rooster.add(player);
        }
    }

    public boolean removePlayer(String name){
        for (Player player : rooster) {
            if(player.getName().equals(name)){
                rooster.remove(player);
                return true;
            }

        }

        return false;
    }

    public void promotePlayer(String name){
      /*  Player player = rooster.stream().filter(p ->p.getName().equals(name)).findFirst().orElse(null);
        if(player != null){
            player.setRank("Member");
        } */
        for (Player player : rooster) {
            if(player.getName().equals(name)){
                player.setRank("Member");

            }
        }

    }

    public void demotePlayer(String name){
     /*   Player player = rooster.stream().filter(p ->p.getName().equals(name)).findFirst().orElse(null);
        if(player != null){
            player.setRank("Trial");
        }*/

        for (Player player : rooster) {
            if(player.getName().equals(name)){
                player.setRank("Trial");
                break;
            }

        }
    }

    public Player[] kickPlayersByClass(String clazz){
     List<Player> removedPlayers = new ArrayList<>();
        for (Player player : rooster) {
            if(player.getClazz().equals(clazz)){
                removedPlayers.add(player);
            }
        }

        rooster.removeAll(removedPlayers);
        Player[] removedPlayersToArr = new Player[removedPlayers.size()];
        for (int i = 0; i < removedPlayers.size(); i++) {
            removedPlayersToArr[i] = removedPlayers.get(i);
        }
        return removedPlayersToArr;
    }


    public int count(){

        return this.rooster.size();
    }


    public String report(){

        return String.format("Players in the guild: %s:%n%s", name, this.rooster.stream()
                .map(Player::toString).collect(Collectors.joining(System.lineSeparator()))).trim();


    }

}

package day_6_Sets_and_Maps;

import java.util.Scanner;
import java.util.TreeSet;

public class PeriodicTable {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        scan.nextLine();
        TreeSet<String> elements = new TreeSet<>();

        for (int i = 0; i < n; i++) {
            String[] scannedElements = scan.nextLine().split("\\s+");
            for (String scannedElement : scannedElements) {
                elements.add(scannedElement);
            }


        }

        elements.forEach( e -> System.out.print( e + " " ));
    }
}

package day_04_Multidimensional;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class ReverseMatrixDiagonals {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
      int[] line = Arrays.stream(reader.readLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();
       int row = line[0];
       int col = line[1];

       int[][] matrix = fillMatrix(row, col, reader);
       matrix = reverseMatrix(matrix, row, col);


       printMatrix(matrix);

    }

    private static int[][] reverseMatrix(int[][] oldMatrix, int rowIn, int colIn) {
        int colSize = getColSize(oldMatrix);


        int[][] newMatrix = new int[colSize+oldMatrix.length][oldMatrix.length];

        int indexCols = 0;
        int indexRows = -1;
        int startIndexCols = 0;
        for (int rows = oldMatrix.length-1; rows >=0 ; rows--) {
            indexRows++;
            startIndexCols++;
            for (int cols = oldMatrix[rows].length-1; cols >=(oldMatrix.length-1)-rows ; cols--) {
                newMatrix[indexCols++][indexRows] = oldMatrix[rows][cols];
            }
            indexCols = startIndexCols;
        }

        int indexRow = (oldMatrix.length-1)-1;
        int rowFor = colIn;


        for (int col = 0; col < colIn; col++) {

            for (int row = indexRow; row >= 0; row--) {

                    newMatrix[rowFor++][col] = oldMatrix[row][col];

            }
             rowFor = colIn;

            indexRow--;

        }



        return newMatrix;
    }



    private static int getColSize(int[][] oldMatrix) {

        int longerCol = Integer.MIN_VALUE;
        for (int rows = 0; rows < oldMatrix.length; rows++) {
            if(oldMatrix[rows].length >longerCol){
                longerCol=oldMatrix[rows].length-1;
            }
        }

        return longerCol;
    }

    private static void printMatrix(int[][] matrix) {
        StringBuilder output = new StringBuilder();
        for (int rows = 0; rows < matrix.length; rows++) {
            for (int cols = 0; cols < matrix[rows].length; cols++) {
               if(matrix[rows][cols]==0){
                   output.append(" ");
               }else{
                   output.append(matrix[rows][cols]).append(" ");
               }



            }
            output.append("\n");
        }
        System.out.println(output.toString());
    }

    private static int[][] fillMatrix(int row, int col, BufferedReader reader) throws IOException {
        int[][] matrix = new int[row][col];


        for (int i = 0; i < row; i++) {

            int[] line = Arrays.stream(reader.readLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = line[j];
            }
        }


        return matrix;
    }
}

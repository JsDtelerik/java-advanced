package exam_phase_one;

import java.util.Objects;
import java.util.Random;

public class TaskOne {

    public static void main(String[] args) {

        Car car1 = new Car("Mustang");
        Car car2 = new Car("Mustang", "1969");

        if(Objects.equals(car1.model, car2.model)){
            System.out.println("Models are identical");
        } else {
            System.out.println("Models are NOT identical");
        }
    }
}


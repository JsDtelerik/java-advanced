package day_04_Multidimensional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Crossfire {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] rowAndCol = Arrays.stream(scan.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();
        int rows = rowAndCol[0];
        int cols = rowAndCol[1];

        List<List<String>> matrix = fillMatrix(rows, cols);
        String[] command = scan.nextLine().split("\\s+");
        while (!command[0].equals("Nuke")) {

            matrix = returnEditedMatrix(matrix, command);

            matrix.removeIf(List::isEmpty);
            command = scan.nextLine().split("\\s+");
        }


        printListMatrix(matrix);

    }

    private static void printListMatrix(List<List<String>> matrix) {
        for (List<String> row : matrix) {
            for (String element : row) {
                System.out.print(element + " ");
            }
            System.out.println();
        }

    }


    private static List<List<String>> fillMatrix(int rows, int cols) {
        List<List<String>> matrix = new ArrayList<>();
        int number = 1;
        for (int row = 0; row < rows; row++) {
            matrix.add(row, new ArrayList<>());
            for (int col = 0; col < cols; col++) {
                matrix.get(row).add(String.valueOf(number++));
            }
        }
        return matrix;
    }

    private static List<List<String>> returnEditedMatrix(List<List<String>> matrix, String[] command) {
        // List<List<String>> matrix = oldMatrix;
        int row = Integer.parseInt(command[0]);
        int col = Integer.parseInt(command[1]);
        int count = Integer.parseInt(command[2]);


            for (int cols = col+count; cols >= col-count; cols--) {
                if(  row >= 0 && row < matrix.size()
                        && cols >=0 && matrix.get(row).size()>cols  ){
                    matrix.get(row).remove(cols);

            }
        }




            for (int rows = row+count; rows >= row-count; rows--) {
                if( rows>=0 && matrix.size()>rows && col>=0
                        && col<matrix.get(rows).size()){
                    if(rows !=row){
                        matrix.get(rows).remove(col);
                    }

                }
            }


        return matrix;
    }
}

package CounterStriker.models.players;

import CounterStriker.models.guns.Gun;

import static CounterStriker.common.ExceptionMessages.*;

public abstract class PlayerImpl implements Player {

    private String username;
    private int health;
    private int armor;
    private boolean isAlive;
    private Gun gun;

    public PlayerImpl(String username, int health, int armor, Gun gun) {
        this.setUsername(username);
        this.setHealth(health);
        this.setArmor(armor);
        this.setAlive();
        this.setGun(gun);

    }


    private void setUsername(String username) {
        if (username == null || username.trim().isEmpty()) {
            throw new NullPointerException(INVALID_PLAYER_NAME);
        }

        this.username = username;
    }

    private void setHealth(int health) {
        if (health < 0) {
            throw new IllegalArgumentException(INVALID_PLAYER_HEALTH);
        }

        this.health = health;
        
    }

    private void setArmor(int armor) {
        if (armor < 0) {
            throw new IllegalArgumentException(INVALID_PLAYER_ARMOR);
        }

        this.armor = armor;
    }


    private void setGun(Gun gun) {
        if (gun == null) {
            throw new NullPointerException(INVALID_GUN);
        }

        this.gun = gun;
    }

    private void setAlive() {
    /*    if (this.health > 0) {
            this.isAlive = isAlive();
            return;
        }*/

        this.isAlive = isAlive();
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public int getHealth() {
        return this.health;
    }

    @Override
    public int getArmor() {
        return this.armor;
    }

    @Override
    public Gun getGun() {
        return this.gun;
    }

    @Override
    public boolean isAlive() {
        if(getHealth() > 0 ){
            return true;
        }
        return false;
    }

    @Override
    public void takeDamage(int points) {
        int currentPoint = points;

        if (this.getArmor() > 0) {

            if (this.getArmor() < points) {
                currentPoint =  points - getArmor() ;
                this.setArmor(0);
            } else if (this.getArmor() >= points) {
                this.setArmor(this.getArmor() - points);
                currentPoint = 0;
            }

        }

        if (this.getHealth() > 0 && currentPoint != 0) {
            if(currentPoint > getHealth()){
                this.setHealth(0);
            }else{
                this.setHealth(this.getHealth() - points);
            }

        }

        this.setAlive();
    }


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        //"{player type}: {player username}
        //--Health: {player health}
        //--Armor: {player armor}
        //--Gun: {player gun name}"


        builder.append(getClass().getSimpleName()).append(": ").append(getUsername()).append(System.lineSeparator())
                .append("--Health: ").append(getHealth()).append(System.lineSeparator())
                .append("--Armor: ").append(getArmor()).append(System.lineSeparator())
                .append("--Gun: ").append(getGun().getName());

        return builder.toString();
    }


}

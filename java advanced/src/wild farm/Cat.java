import java.text.DecimalFormat;

public class Cat extends Felime {

    private String breed;

    protected Cat(String animalType, String animalName, Double animalWeight, Integer foodEaten, String livingRegion, String breed) {
        super(animalType, animalName, animalWeight, foodEaten, livingRegion);
        this.breed = breed;
    }


    @Override
    protected void makeSound() {
        System.out.println("Meowwww");
    }

    @Override
    protected void eat(Food food) {
    this.setFoodEaten(this.getFoodEaten() + food.getQuantity());

    }

   // public String getBreed() {
   //     return breed;
   // }

    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat("#.##");
        String editedWeight = df.format(getAnimalWeight());
//{AnimalType} [{AnimalName}, {CatBreed}, {AnimalWeight}, {AnimalLivingRegion}, {FoodEaten}]

        StringBuilder builder = new StringBuilder();

        builder.append(this.getAnimalType()).append("[").append(this.getAnimalName()).append(", ")
                .append(this.breed).append(", ").append(editedWeight).append(", ")
                .append(this.getLivingRegion()).append(", ").append(this.getFoodEaten()).append("]");



        return builder.toString();
    }


}

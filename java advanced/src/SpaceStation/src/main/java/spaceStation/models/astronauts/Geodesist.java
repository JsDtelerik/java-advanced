package spaceStation.models.astronauts;

public class Geodesist extends BaseAstronaut{
    private final double INITIAL_OXYGEN = 50;

    protected Geodesist(String name) {
        super(name);
        setOxygen(INITIAL_OXYGEN);
    }

    @Override
    public void breath() {
        double decrease = INITIAL_OXYGEN - 0;
        setOxygen(decrease);
    }
}

package day_6_Sets_and_Maps;

import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.TreeMap;

public class AMinerTask {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        LinkedHashMap<String, Integer> resources =  new LinkedHashMap<>();

        String commandResource = scan.nextLine();
        int quantity = Integer.parseInt(scan.nextLine());

        while (true){

            if(resources.containsKey(commandResource)){
                resources.put(commandResource, resources.get(commandResource)+quantity);
            }else{
                resources.put(commandResource, quantity);
            }
            commandResource = scan.nextLine();
            if(commandResource.equals("stop")){
                break;
            }
            quantity = Integer.parseInt(scan.nextLine());
        }

        resources.entrySet().stream().forEach(k -> System.out.printf("%s -> %d%n", k.getKey(), k.getValue()));
    }
}

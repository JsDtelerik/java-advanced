package day_02_Stack;

import java.lang.reflect.Array;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class BasicQueueOperations {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int[] commands = Arrays.stream(scan.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();

        ArrayDeque<Integer> queue = offerElements(scan, commands[0]);
        queue = pollElements(queue, commands[1]);

        if(queue.isEmpty()){
            System.out.println(0);
        }else if(queue.contains(commands[2])){
            System.out.println("true");
        }else{
            //System.out.println(printMinQueueMinValue(queue));
            System.out.println(Collections.min(queue));
        }

    }

    private static int printMinQueueMinValue(ArrayDeque<Integer> queue) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < queue.size(); i++) {
            int currentValue = queue.poll();
            if(min>currentValue){
                min = currentValue;
            }
        }
        return min;
    }

    private static ArrayDeque<Integer> pollElements(ArrayDeque<Integer> queue, int n) {
        for (int i = 1; i <=n ; i++) {
            queue.poll();
        }
        return queue;
    }

    private static ArrayDeque<Integer> offerElements(Scanner scan, int n) {
        ArrayDeque<Integer> queue = new ArrayDeque<>();
        int[] elements = Arrays.stream(scan.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();
        for (int i = 0; i <n ; i++) {
            queue.offer(elements[i]);
        }
        return queue;
    }
}

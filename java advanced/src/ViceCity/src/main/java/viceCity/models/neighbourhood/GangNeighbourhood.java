package viceCity.models.neighbourhood;

import viceCity.models.guns.Gun;
import viceCity.models.players.Player;

import java.util.Collection;
import java.util.List;

public class GangNeighbourhood implements Neighbourhood {

    private int deadCivilians;

    // boolean shootingStarted;
    public GangNeighbourhood() {
        this.deadCivilians = 0;
        //this.shootingStarted = false;
    }


    @Override
    public void action(Player mainPlayer, Collection<Player> civilPlayers) {
        //  Collection<Gun> mainPlayerAvailableGuns = mainPlayer.getGunRepository().getModels();

        playerStartShooting(mainPlayer, civilPlayers);
        civiliansReturnTheFire(mainPlayer, civilPlayers);

    }

    private void civiliansReturnTheFire(Player mainPlayer, Collection<Player> civilPlayers) {
        for (Player civilPlayer : civilPlayers) {
            for (Gun model : civilPlayer.getGunRepository().getModels()) {
                if(!model.canFire()){
                    continue;
                }
                while (model.getTotalBullets() > 0 && mainPlayer.getLifePoints() > 0) {
                    if (!model.canFire()) {
                        reload(model);
                    }
                    int dmg = model.fire();
                    mainPlayer.takeLifePoints(dmg);
                    //shootingStarted = true;
                    if (mainPlayer.getLifePoints() <= 0) {

                        mainPlayer.setAlive(mainPlayer.isAlive());
                    }


                }


            }
        }
    }


    private void playerStartShooting(Player mainPlayer, Collection<Player> civilPlayers) {
        if (mainPlayer.getGunRepository().getModels().size() <= 0) {
            return;
        }
        for (Gun gun : mainPlayer.getGunRepository().getModels()) {
            if(!gun.canFire()){
                continue;
            }
            for (Player civilPlayer : civilPlayers) {
                if(!civilPlayer.isAlive()){
                    continue;
                }
                while (gun.getTotalBullets() > 0  && civilPlayer.isAlive()) {
                    if (!gun.canFire()) {
                        reload(gun);
                    }

                    int dmg = gun.fire();
                    civilPlayer.takeLifePoints(dmg);
                    // shootingStarted = true;
                    if (civilPlayer.getLifePoints() <= 0) {
                        this.setDeadCivilians(this.deadCivilians + 1);
                        civilPlayer.setAlive(civilPlayer.isAlive());

                    }


                }




            }
        }

    }

    private void reload(Gun gun) {
        if (gun.getClass().getSimpleName().equals("Pistol")) {
            if (gun.getTotalBullets() >= 10) {
                gun.setMagazineAmmunition(10);
                gun.setStorageAmmunition(gun.getTotalBullets() - 10);
                gun.setCanFire(gun.canFire());
            }
        } else if (gun.getClass().getSimpleName().equals("Rifle")) {
            if (gun.getTotalBullets() >= 50) {
                gun.setMagazineAmmunition(50);
                gun.setStorageAmmunition(gun.getTotalBullets() - 50);
                gun.setCanFire(gun.canFire());
            }
        }

    }


    public int getDeadCivilians() {
        return deadCivilians;
    }

    public void setDeadCivilians(int deadCivilians) {
        this.deadCivilians = deadCivilians;
    }
    // public boolean getShootingStarted() {
    //     return shootingStarted;
    // }
}

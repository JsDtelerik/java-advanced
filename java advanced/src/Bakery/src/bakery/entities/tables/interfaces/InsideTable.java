package bakery.entities.tables.interfaces;

import java.text.DecimalFormat;

public class InsideTable extends BaseTable{

    private static final double pricePerPerson = 2.50;

    public InsideTable(int tableNumber, int capacity ) {
        super(tableNumber, capacity, pricePerPerson);
    }


    @Override
    public String getFreeTableInfo() {
       // DecimalFormat df = new DecimalFormat("#.##");
       // String pricePer = df.format(getPricePerPerson());

        StringBuilder builder = new StringBuilder();


        builder.append("Table: ").append(getTableNumber()).append(System.lineSeparator())
                .append("Type: ").append("InsideTable").append(System.lineSeparator())
                .append("Capacity: ").append(getCapacity()).append(System.lineSeparator())
                .append("Price per Person: ").append(String.format("%.02f", getPricePerPerson()));

        return builder.toString();
    }


}

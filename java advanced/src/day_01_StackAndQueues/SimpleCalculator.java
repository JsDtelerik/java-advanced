package day_01_StackAndQueues;

import java.util.ArrayDeque;
import java.util.Scanner;

public class SimpleCalculator {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] line = scan.nextLine().split("\\s+");

        ArrayDeque<String> stack = new ArrayDeque<>();
        for (int i = line.length-1; i >= 0; i--) {
            stack.push(line[i]);
        }

        while (stack.size() > 1){
            int firstNum = Integer.parseInt(stack.pop());
            String operator = stack.pop();
            int secondNum = Integer.parseInt(stack.pop());

            int sum =  operator.equals("+")
                    ? firstNum+secondNum
                    : firstNum-secondNum;

            stack.push(String.valueOf(sum));

        }
        System.out.println(stack.pop());
    }
}

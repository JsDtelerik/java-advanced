package test_pack;

import java.util.Arrays;
import java.util.Scanner;

public class ThirdTask {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String[] input = scan.nextLine().split("");
        //String[] moderatedInput = new String[input.length];

        for (int i = 0; i < input.length; i++) {
            if(i == 0){
                continue;
            }
            if(i < input.length - 1){
                if(input[i].equals("0") && input[i-1].equals("1") && input[i+1].equals("1")){
                    input[i] = "1";
                }else if (input[i].equals("1") && input[i-1].equals("0") && input[i+1].equals("0")){
                    input[i] = "0";
                }else if (input[i].equals("1") && input[i-1].equals("1") && input[i+1].equals("1")){
                    input[i]="0";
                }
            }

        }

        Arrays.stream(input).forEach(System.out::print);

    }

}

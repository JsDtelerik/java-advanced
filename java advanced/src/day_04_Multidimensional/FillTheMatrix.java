package day_04_Multidimensional;

import java.util.Scanner;

public class FillTheMatrix {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] input = scan.nextLine().split(", ");
        int size = Integer.parseInt(input[0]);
        String pattern = input[1];

        switch (pattern){
            case "A":
                fillAndPrintMatrixByPatternA(size);
                break;
            case "B":
                fillAndPrintMatrixByPatternB(size);
                break;
        }
    }

    private static void fillAndPrintMatrixByPatternB(int size) {
        int[][] matrix = new int[size][size];
        int element = 0;
        for (int col = 0; col <size ; col++) {
            if(col % 2 == 0){
                for (int row = 0; row < size; row++) {
                    element++;
                    matrix[row][col]= element;
                }
            }else{
                for (int row = size-1; row >= 0; row--) {
                    element++;
                    matrix[row][col]= element;
                }
            }

        }
        printMatrix(matrix);
    }

    private static void fillAndPrintMatrixByPatternA(int size) {
        int[][] matrix = new int[size][size];
        int element = 0;
        for (int col = 0; col <size ; col++) {
            for (int row = 0; row < size; row++) {
                element++;
                matrix[row][col]= element;
            }
        }
        printMatrix(matrix);
    }


    public static void printMatrix(int[][] matrix) {
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {
                System.out.print(matrix[row][col] + " ");
            }
            System.out.println();
        }
    }
}

import java.util.ArrayList;
import java.util.List;

public class AddRemoveCollection extends Collection implements AddRemovable {

    //private List<String> listCollection;

   // protected AddRemoveCollection() {

     //   super();
   //    // this.listCollection = new ArrayList<>();
  //  }

    //•	An add(String) method – which adds an item to the start of the collection.
    //•	A remove() method which removes the last item in the collection.

    @Override
    public String remove() {
       String returnValue =  super.getItems().remove(super.getItems().size()-1);
        return returnValue;
    }

   // @Override
   // int getUsed() {
       // throw new IllegalArgumentException();
   // }

    @Override
    public int add(String element) {
        super.getItems().add(0, element);
        return 0;
    }
}

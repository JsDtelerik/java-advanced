import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

public class Main {
    public static List<Integer> generalsSubordinateTroops = new ArrayList<>();
    public static List<LieutenantGeneral> generals = new ArrayList<>();
    public static List<Private> privatesList = new ArrayList<>();
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        
//Private 1 Peter Petrov 22.22
//Commando 13 Stam Stamov 13.1 Airforces
//Private 222 Tom Tomson 80.08
//LieutenantGeneral 3 John Johnson 100 222 1
//End
        String[] command = reader.readLine().split("\\s+");
        List<Soldier> soldiers = new ArrayList<>();
        boolean generalOn = false;
        while (!command[0].equals("End")){

            try {
                switch (command[0]) {
                    case "Private":
                        if (privatesList.size() > 0) {
                            if (privatesList.get(0).getId() == Integer.parseInt(command[1])) {
                                break;
                            }
                        }

                        soldiers.add(new Private(Integer.parseInt(command[1]), command[2], command[3], Double.parseDouble(command[4])));
                        privatesList.add(new Private(Integer.parseInt(command[1]), command[2], command[3], Double.parseDouble(command[4])));
                        break;
                    case "Spy":
                        try{
                            soldiers.add(new Spy(Integer.parseInt(command[1]), command[2], command[3], command[4]));
                        }catch (IllegalArgumentException exception){
                           break;
                        }

                        break;
                    case "LieutenantGeneral":
                        List<Integer> generalIdList = new ArrayList<>();
                        for (int i = 5; i < command.length; i++) {
                            generalIdList.add(Integer.parseInt(command[i]));
                        }
                        generalsSubordinateTroops = generalIdList;

                        LieutenantGeneral general = new LieutenantGeneral(Integer.parseInt(command[1]), command[2], command[3],
                                Double.parseDouble(command[4]), generalIdList);
                        generals.add(general);
                        soldiers.add(general);
                        generalOn = true;
                        // addSubordinateTroops(soldiers);
                        break;
                    case "Engineer":
                        //Engineer 7 Peter Penchev 12.23 Marines Boat 2 Crane 17
                        try {
                            Corps corpsCheck = Corps.valueOf(command[5].toUpperCase());
                        } catch (IllegalArgumentException exception) {
                            break;
                        }


                        LinkedHashSet<Repair> repairList = new LinkedHashSet<>();
                        for (int i = 6; i < command.length; i++) {
                            String item = command[i++];
                            int time = Integer.parseInt(command[i]);
                            Repair repair = null;
                            try {
                                repair = new Repair(item, time);
                            } catch (IllegalArgumentException ex) {
                                continue;
                            }

                            repairList.add(repair);
                        }


                        Engineer engineer = new Engineer(Integer.parseInt(command[1]), command[2], command[3], Double.parseDouble(command[4]),
                                Corps.valueOf(command[5].toUpperCase()), repairList);
                        soldiers.add(engineer);
                        break;
                    case "Commando":
                        //Commando 19 Petra Ivanova 150.15 Airforces HairyFoot Finished Freedom inProgress
                        try {
                            Corps corpsCheck = Corps.valueOf(command[5].toUpperCase());
                        } catch (IllegalArgumentException exception) {
                            break;
                        }

                        List<Mission> missionList = new ArrayList<>();
                        for (int i = 6; i < command.length; i++) {
                            String missionName = command[i++];
                            String missionStatus = command[i];
                            String missionChecker = checkMissionStatus(missionStatus);
                            Mission mission = null;
                            try {
                                mission = new Mission(missionName, State.valueOf(missionChecker));
                            } catch (IllegalArgumentException exception) {
                                continue;
                            }

                            //  if (mission.getState().equalsIgnoreCase("finished")) {
                            //      mission.completeMission();
                            //  }
                            missionList.add(mission);


                        }
                        Commando commando = new Commando(Integer.parseInt(command[1]), command[2], command[3], Double.parseDouble(command[4]),
                                Corps.valueOf(command[5].toUpperCase()), missionList);

                        soldiers.add(commando);
                        break;

                }
            }catch (IllegalArgumentException ex){
             //   if(ex.getMessage().equals("invalid name")){
               //     command = reader.readLine().split("\\s+");
                 //   continue;
                //}
                command = reader.readLine().split("\\s+");
                continue;
            }
                if(generalOn){
                    addSubordinateTroops(soldiers);
                }

            command = reader.readLine().split("\\s+");
        }
      //  for (Soldier soldier : soldiers) {
         //   System.out.println(soldier.toString());

      //  }
        soldiers.stream().forEach(soldier -> System.out.println(soldier.toString()));
    }

    private static String checkMissionStatus(String missionStatus) {
        if(missionStatus.equals("inProgress")){
            return missionStatus.toUpperCase();
        }else if (missionStatus.equals("Finished") || missionStatus.equals("finished")){
            return missionStatus.toUpperCase();
        }


        return missionStatus;
    }

    private static void addSubordinateTroops(List<Soldier> soldiers) {
        LieutenantGeneral general = null;
        for (LieutenantGeneral lieutenantGeneral : generals) {
            general = lieutenantGeneral;
          List<Integer> subordinateIds = general.getSoldiersIds();
           /* for (Soldier soldier : soldiers) {
                for (Integer ids : subordinateIds) {
                    if(soldier.getId() == ids){
                        general.addPrivate(soldier);
                    }
                }

            } */
            for (Private soldier : privatesList) {
                for (Integer ids : subordinateIds) {
                    if(soldier.getId() == ids){
                        general.addPrivate(soldier);
                    }
                }
            }
        }


    }
}

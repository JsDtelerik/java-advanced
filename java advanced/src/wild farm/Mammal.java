import java.text.DecimalFormat;

public abstract class Mammal extends Animal{

    private String livingRegion;

    protected Mammal(String animalType, String animalName, Double animalWeight, Integer foodEaten, String livingRegion) {
        super(animalType, animalName, animalWeight, foodEaten);
        this.livingRegion = livingRegion;
    }


    protected String getLivingRegion() {
        return livingRegion;
    }

    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat("#.##");
        String editedWeight = df.format(getAnimalWeight());
//{AnimalType} [{AnimalName}, {CatBreed}, {AnimalWeight}, {AnimalLivingRegion}, {FoodEaten}]

        StringBuilder builder = new StringBuilder();

        builder.append(this.getAnimalType()).append("[").append(this.getAnimalName()).append(", ")
                .append(editedWeight).append(", ")
                .append(this.getLivingRegion()).append(", ").append(this.getFoodEaten()).append("]");



        return builder.toString();
    }


}

package day_04_Multidimensional;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DancingBoss {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int playerHP = 18500;
        double bossHP = 3000000.00;
        List<Integer> playerPosition = new ArrayList<>();
        playerPosition.add(7);
        playerPosition.add(7);
        List<List<String>> battleFiled = fillMatrix();
        boolean playerHasBeenKilled = false;
        boolean bossHasBeenDefeated = false;

        double damageDoneTohHiganPerRound = Double.parseDouble(scan.nextLine());

        String keepLastActivatedSpell = "";

        while (true){
            String[] command = scan.nextLine().split("\\s+");
            String spell = command[0];
            bossHP -=damageDoneTohHiganPerRound;
            if(bossHP<=0){
                spell = "ignored";
                bossHasBeenDefeated = true;
                break;
            }
            if(keepLastActivatedSpell.equals("Cloud")){
                playerHP -= 3500;
            }
            if(playerHP <= 0){
                playerHasBeenKilled = true;
                break;
            }

            switch (spell) {
                case "Cloud":

                    battleFiled = activateSpell(battleFiled, Integer.parseInt(command[1]), Integer.parseInt(command[2]));
                    int pRow = playerPosition.get(0);
                    int pCol = playerPosition.get(1);

                    if (battleFiled.get(pRow).get(pCol).equals("*")){
                        String tryToEscape = returnRunawayDirection(battleFiled,pRow, pCol);
                        if(tryToEscape.equals("Stop")){
                            playerHP -=3500;
                            keepLastActivatedSpell = "Cloud";
                        }else {
                            playerPosition = movePlayer(playerPosition.get(0), playerPosition.get(1), tryToEscape);
                        }
                    }
                    break;
                case "Eruption":
                    if(checkIndex(Integer.parseInt(command[1]), Integer.parseInt(command[2]))){
                        battleFiled.get(Integer.parseInt(command[1])).set(Integer.parseInt(command[2]), "*");
                        int pRow1 = playerPosition.get(0);
                        int pCol1 = playerPosition.get(1);

                        if (battleFiled.get(pRow1).get(pCol1).equals("*")){
                            String tryToEscape = returnRunawayDirection(battleFiled,pRow1, pCol1);
                            if(tryToEscape.equals("Stop")){
                                playerHP -=6000;
                                keepLastActivatedSpell = "Eruption";
                            }else {
                                playerPosition = movePlayer(playerPosition.get(0), playerPosition.get(1), tryToEscape);
                            }
                        }
                    }

                    break;
                case "ignored":
                    break;
            }

            battleFiled = fillMatrix();

        }

        if(bossHasBeenDefeated){
            System.out.println("Heigan: Defeated!");
            System.out.printf("Player: %d %n", playerHP);
        }else if (playerHasBeenKilled){
            System.out.printf("Heigan: %02f%n", bossHP);
            System.out.printf("Player: Killed by %s%n", keepLastActivatedSpell);
        }
        System.out.printf("Final position: %d, %d", playerPosition.get(0), playerPosition.get(1));

    }

    private static List<Integer> movePlayer(Integer row, Integer col, String tryToEscape) {

        switch (tryToEscape){
            case "Up":
                row = row-1;
                break;
            case "Right":
                col = col+1;
                break;
            case "Down":
                row = row+1;
                break;
            case "Left":
                col = col -1;
                break;
        }
        List<Integer> position = new ArrayList<>();
        position.add(row);
        position.add(col);
        return position;
    }

    private static String returnRunawayDirection(List<List<String>> battleFiled, int pRow, int pCol) {
        if(pRow + 1 <= 0){
            return "Up";
        }
        if(pCol + 1 < battleFiled.get(pRow).size()){
            return "Right";
        }
        if( pRow - 1 < battleFiled.size()){
            return "Down";
        }
        if(pCol - 1 >= 0 ){
            return "Left";
        }

        return "Stop";
    }

    private static List<List<String>> activateSpell(List<List<String>> field,  int row, int col) {

        boolean check = checkIndex(row, col);
        if (check) {

            for (int rows = row - 1; rows < row + 1; rows++) {
                if (rows < field.size() && rows >= 0) {
                    for (int cols = col - 1; cols < col + 1; cols++) {
                        if (cols >= 0 && cols < field.get(rows).size()) {
                            field.get(rows).set(cols, "*");

                        }

                    }

                }

            }

        }
        return field;
    }

    private static boolean checkIndex(int row, int col) {
        if((row <0 || row>=15) || (col < 0 || col >=15)) {
            return false;
        }

        return true;
    }

    private static List<List<String>> fillMatrix() {
        List<List<String>> matrix = new ArrayList<>();
        int n = 1;
        for (int rows = 0; rows < 15; rows++) {
            matrix.add(rows, new ArrayList<>());
            for (int cols = 0; cols < 15; cols++) {
                matrix.get(rows).add(String.valueOf(n++) + " ");
            }
            System.out.println();
        }

        return matrix;
    }

}


package day_02_Stack;

import java.util.ArrayDeque;
import java.util.Scanner;

public class SimpleTextEditor {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = Integer.parseInt(scan.nextLine());
        StringBuilder emptyText = new StringBuilder();
        ArrayDeque<String> keepLastEmptyText = new ArrayDeque<>();
        for (int i = 1; i <= n; i++) {
            String[] command = scan.nextLine().split("\\s+");

            switch (command[0]){
                case "1":
                    keepLastEmptyText.push(emptyText.toString());
                    emptyText.append(command[1]);
                    break;
                case "2":
                    int erase = Integer.parseInt(command[1]);
                    keepLastEmptyText.push(emptyText.toString());
                    emptyText.delete(emptyText.length()-erase, emptyText.length());

                    break;
                case "3":
                    int index = Integer.parseInt(command[1]);

                    System.out.println(emptyText.charAt(index-1));
                    break;
                case "4":

                    emptyText.replace(0, emptyText.length(), keepLastEmptyText.pop());
                    break;
            }

        }
    }
}

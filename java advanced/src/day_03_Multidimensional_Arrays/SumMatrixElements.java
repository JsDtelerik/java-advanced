package day_03_Multidimensional_Arrays;

import java.util.Arrays;
import java.util.Scanner;

public class SumMatrixElements {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] rowsAndCols = Arrays.stream(scan.nextLine().split(", ")).mapToInt(Integer::parseInt).toArray();
        int rows = rowsAndCols[0];
        int cols = rowsAndCols[1];

        int[][] matrix = new int[rows][cols];
        matrix = fillMatrix(rows, cols, scan);
        System.out.println(matrix.length);
        System.out.println(matrix[0].length);
        printSum(matrix);

    }

    private static void printSum(int[][] matrix) {
        int sum = 0;
        for (int row = 0; row < matrix.length; row++) {
            //for (int col = 0; col < matrix[row].length; col++) {
              //  sum += matrix[row][col];
           // }
            sum += Arrays.stream(matrix[row]).sum();
        }
        System.out.println(sum);
    }

    public static int[][] fillMatrix(int rows, int cols, Scanner scan) {
        int[][] matrix = new int[rows][cols];
        for (int row = 0; row <rows ; row++) {
            int[] column = Arrays.stream(scan.nextLine().split(", ")).mapToInt(Integer::parseInt).toArray();
            for (int col = 0; col < column.length; col++) {
                matrix[row][col] = column[col];

            }
        }
        return matrix;
    }
}

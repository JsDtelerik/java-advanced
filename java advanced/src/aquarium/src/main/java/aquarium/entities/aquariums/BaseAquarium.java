package aquarium.entities.aquariums;

import aquarium.entities.decorations.Decoration;
import aquarium.entities.fish.Fish;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import static aquarium.common.ConstantMessages.*;
import static aquarium.common.ExceptionMessages.*;

public abstract class BaseAquarium implements Aquarium{
    private String name;
    private int capacity;
    private Collection<Decoration> decorations;
    private Collection<Fish> fish;

    protected BaseAquarium(String name, int capacity) {
        this.setName(name);
        this.capacity = capacity;
        this.decorations = new ArrayList<>();
        this.fish = new ArrayList<>();

    }

   /* @Override
    public int getCapacity() {
        return this.capacity;
    }*/

    public void setName(String name) {
        if(name == null || name.trim().isEmpty()){
            throw new NullPointerException (AQUARIUM_NAME_NULL_OR_EMPTY);
        }

        this.name = name;
    }

    /*public void setCapacity(int capacity) {
        this.capacity = this.capacity-1;
    }*/
    @Override
    public int calculateComfort() {
        return this.decorations.stream().mapToInt(Decoration::getComfort).sum();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void addFish(Fish fish) {
        if(this.fish.size()  >= capacity ){
            throw  new IllegalStateException(NOT_ENOUGH_CAPACITY);
        }

        this.fish.add(fish);

    }

    @Override
    public void removeFish(Fish fish) {
        this.fish.remove(fish);
    }

    @Override
    public void addDecoration(Decoration decoration) {
        this.decorations.add(decoration);
    }

    @Override
    public void feed() {
        this.fish.forEach(Fish::eat);
    }

    @Override
    public String getInfo() {

       String fishOrNone = this.fish.isEmpty()? "none"
               : this.fish.stream().map(Fish::getName).collect(Collectors.joining(" "));


        StringBuilder builder = new StringBuilder();

        //"{aquariumName} ({aquariumType}):
        //Fish: {fishName1} {fishName2} {fishName3} (…) / Fish: none
        //Decorations: {decorationsCount}
        //Comfort: {aquariumComfort}"


        builder.append(this.name).append(" (").append(this.getClass().getSimpleName()).append("):").append(System.lineSeparator())
                .append("Fish: ").append(fishOrNone).append(System.lineSeparator())
                .append("Decorations: ").append(decorations.size()).append(System.lineSeparator())
                .append("Comfort: ").append(calculateComfort());

        return builder.toString();
    }

    @Override
    public Collection<Fish> getFish() {
        return this.fish;
    }

    @Override
    public Collection<Decoration> getDecorations() {
        return this.decorations;
    }



}

package day_19_exam_preparation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Revolt {
    public static int playerRow = 0, playerCol = 0;
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int size = Integer.parseInt(reader.readLine());
        int lines = Integer.parseInt(reader.readLine());
        char[][] matrix = fillMatrix (size, reader);

        boolean playerWon = false;
        while (lines-- >0){
            String command = reader.readLine();
            switch (command){
                case "up":
                   playerWon = movePlayer(matrix, -1, 0);
                    break;
                case "down":
                    playerWon =  movePlayer(matrix, +1, 0);
                    break;
                case "left":
                    playerWon =  movePlayer(matrix, 0, -1);
                    break;
                case "right":
                    playerWon =  movePlayer(matrix, 0, +1);
                    break;
            }
            if(playerWon){
                break;
            }
        }

        if(playerWon){
            System.out.println("Player won!");
        }else{
            System.out.println("Player lost!");
        }
        printMatrix(matrix);
    }

    private static boolean movePlayer(char[][] matrix, int modifierRow, int modifierCol) {
        boolean hasWon = false;
        matrix[playerRow][playerCol] = '-';
        int newRowIndex = checkIndexes(playerRow+modifierRow, matrix);
        int newColIndex = checkIndexes(playerCol+modifierCol, matrix);
        if(matrix[newRowIndex][newColIndex] == 'B'){
             newRowIndex = checkIndexes(newRowIndex+modifierRow, matrix);
             newColIndex = checkIndexes(newColIndex+modifierCol, matrix);
        }else if ( matrix[newRowIndex][newColIndex] == 'T'){
            newRowIndex = playerRow;
            newColIndex = playerCol;
        }
        if(matrix[newRowIndex][newColIndex] == 'F'){
            hasWon = true;
        }
        matrix[newRowIndex][newColIndex] = 'f';
        playerRow = newRowIndex;
        playerCol = newColIndex;

        return  hasWon;
    }

    private static int checkIndexes(int modifiedIndex, char[][] matrix) {
        int index = modifiedIndex;
        if(index < 0){
            index = matrix.length-1;
        }else if (index > matrix.length-1){
            index =0;
        }

        return index;
    }

    private static void printMatrix(char[][] matrix) {
        StringBuilder builder = new StringBuilder();
        for (char[] chars : matrix) {
            for (char symbol : chars) {
                builder.append(symbol);
            }
            builder.append(System.lineSeparator());
        }
        System.out.println(builder.toString());
    }

    private static char[][] fillMatrix(int size, BufferedReader reader) throws IOException {
        char[][] matrix = new char[size][size];
        for (int i = 0; i < size; i++) {
            String line = reader.readLine();
            matrix[i] = line.toCharArray();
            if(line.contains("f")){
                playerRow = i;
                playerCol = line.indexOf("f");
            }
        }
        return matrix;
    }
}

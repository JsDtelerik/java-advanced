package spaceStation.repositories;


import spaceStation.models.astronauts.Astronaut;
import spaceStation.models.planets.Planet;
import spaceStation.models.planets.PlanetImpl;

import java.util.ArrayList;
import java.util.List;

public class PlanetRepository implements Repository{

    private List<PlanetImpl> planets;


    public PlanetRepository() {

        this.planets = new ArrayList<>();
    }

    //void add(Planet planet)
    //•	Adds a planet for exploration.
    //•	Every planet is unique and it is guaranteed that there will not be a planet with the same name.
    //boolean remove(Planet planet)
    //•	Removes a planet from the collection. Returns true if the deletion was sucessful.
    //Planet findByName(String name)
    //•	Returns a planet with that name.
    //•	It is guaranteed that the planet exists in the collection.
    //Collection<Planet> getModels()
    //•	Returns collection of planets (unmodifiable)





    @Override
    public Planet findByName(String name ){
        return planets.stream().filter(a -> a.getName().equals(name)).findFirst().orElse(null);
    }

    public List<PlanetImpl> getModels(){
        return planets;
    }



    @Override
    public void add(Object model) {
        PlanetImpl newModel = (PlanetImpl)model;

        planets.add(newModel);
    }

    @Override
    public boolean remove(Object model) {
        PlanetImpl newModel = (PlanetImpl)model;
        return planets.remove(newModel);
    }


}

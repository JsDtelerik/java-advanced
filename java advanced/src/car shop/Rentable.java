public interface Rentable {
    //+getMinRentDay(): Integer
    //+getPricePerDay(): Double

    Integer getMinRentDay();
    Double getPricePerDay();

}

package spaceStation.repositories;

import spaceStation.models.astronauts.Astronaut;
import spaceStation.models.astronauts.BaseAstronaut;

import java.util.ArrayList;
import java.util.List;

public class AstronautRepository{

    private List<BaseAstronaut> astronauts;


    public AstronautRepository() {
        this.astronauts = new ArrayList<>();
    }

    public void add(BaseAstronaut astronaut){
        astronauts.add(astronaut);
    }

    public boolean remove(BaseAstronaut astronaut){
        return astronauts.remove(astronaut);
    }

    public Astronaut findByName(String name ){
        return astronauts.stream().filter(a -> a.getName().equals(name)).findFirst().orElse(null);
    }

    public List<BaseAstronaut> getModels(){
        return astronauts;
    }
}

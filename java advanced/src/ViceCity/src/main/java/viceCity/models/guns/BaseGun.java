package viceCity.models.guns;

import static viceCity.common.ExceptionMessages.*;

public abstract class BaseGun implements Gun{

    private String name;
    private int bulletsPerBarrel;
    private int totalBullets;
    private boolean canFire;

    protected BaseGun(String name, int bulletsPerBarrel, int totalBullets) {
      this.setName(name);
        this.setBulletsPerBarrel(bulletsPerBarrel);
        this.setTotalBullets(totalBullets);
        this.canFire = true;
    }


    public void setName(String name) {
        if(name == null || name.trim().isEmpty()){
            throw new NullPointerException(NAME_NULL);
        }

        this.name = name;
    }

    public void setBulletsPerBarrel(int bulletsPerBarrel) {
        if(bulletsPerBarrel < 0 ){
            throw new IllegalArgumentException(BULLETS_LESS_THAN_ZERO);
        }

        this.bulletsPerBarrel = bulletsPerBarrel;
    }

    public void setTotalBullets(int totalBullets) {
        if(totalBullets < 0 ){
            throw  new IllegalArgumentException(TOTAL_BULLETS_LESS_THAN_ZERO);
        }
        this.totalBullets = totalBullets;
    }




    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getBulletsPerBarrel() {
        return this.bulletsPerBarrel;
    }

    @Override
    public boolean canFire() {
        if(this.bulletsPerBarrel <= 0){
            this.canFire = false;
            return false;
        }
        return true;
    }

    @Override
    public int getTotalBullets() {
        return this.totalBullets;
    }

    @Override
    public int fire() {
        return 0;
    }

    @Override
    public void setMagazineAmmunition(int newAm) {


        setBulletsPerBarrel(newAm);
    }
    @Override
    public void setStorageAmmunition(int newStorageAm) {
        setTotalBullets(newStorageAm);
    }
@Override
    public void setCanFire(boolean canFire) {
        this.canFire = canFire;
    }
}

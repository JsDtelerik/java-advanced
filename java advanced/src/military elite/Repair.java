import java.util.LinkedHashSet;

public class Repair implements EngineerImpl{

    private String partName;
    private int hoursWorked;

    public Repair(String partName, int hoursWorked) {
        this.partName = partName;
        setHoursWorked(hoursWorked);
    }

    private void setHoursWorked(int hoursWorked) {
        if(hoursWorked<=0){
            throw new IllegalArgumentException();
        }else{
            this.hoursWorked = hoursWorked;
        }
    }

    public void addRepair(Repair repair){
        this.repairs.add(repair);
    }


    public LinkedHashSet<Repair> getRepairs(){
       return this.repairs;
    }



    public String getPartName() {
        return partName;
    }


    public int getHoursWorked() {
        return hoursWorked;
    }


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
      //  builder.append("Repairs:").append(System.lineSeparator());
       // for (Repair repair : repairs) {

            builder.append(" Part Name: ").append(getPartName()).append(" Hours Worked: ").append(getHoursWorked());

       // }

        return builder.toString();
    }
}

package day_04_Multidimensional;

import java.util.Scanner;

public class DiagonalDifference {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int rowsAndCols = scan.nextInt();
        int[][] matrix = fillMatrix(rowsAndCols, scan);
        printDifferenceBetweenTwoDiagonals(matrix);
    }

    private static void printDifferenceBetweenTwoDiagonals(int[][] matrix) {
        int sumFirstDiagonal = 0;
        for (int row = 0; row < matrix.length; row++) {
            sumFirstDiagonal += matrix[row][row];
        }
        int sumSecondDiagonal =0;
        int col = 0;
        for (int row = matrix.length-1; row >=0 ; row--) {

            sumSecondDiagonal += matrix[row][col++];
        }
        System.out.println(Math.abs(sumFirstDiagonal-sumSecondDiagonal));
    }

    private static int[][] fillMatrix(int rowsAndCols, Scanner scan) {
        int[][] matrix = new int[rowsAndCols][rowsAndCols];
        scan.nextLine();
        for (int rows = 0; rows < rowsAndCols; rows++) {
            for (int cols = 0; cols < rowsAndCols; cols++) {
                matrix[rows][cols] = scan.nextInt();
            }
            scan.nextLine();
        }
        return matrix;
    }
}

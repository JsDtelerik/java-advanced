package farmville;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FarmvilleTests {
    private Farm farm;
    private Animal animal;

    @Before
    public void setFarm(){
        farm = new Farm("farm", 100);
        animal = new Animal("cow", 10);
    }

    @Test
    public void testConstructor(){
        Farm newFarm = new Farm("farm1", 10);
        Assert.assertEquals("farm1", newFarm.getName());
        Assert.assertEquals(10, newFarm.getCapacity());
        Assert.assertEquals(0, newFarm.getCount());
    }

    @Test
    public void testAddAnimal(){
        Assert.assertEquals(0, farm.getCount());
        farm.add(animal);
        Assert.assertEquals(1, farm.getCount());
        Animal animal1 = new Animal("dog", 5);
        farm.add(animal1);
        Assert.assertEquals(2, farm.getCount());
        Animal animal2 = new Animal("cat", 7);
        farm.add(animal2);
        Assert.assertEquals(3, farm.getCount());

    }

    @Test(expected = IllegalArgumentException.class)
    public void testTryToAddAnimalWhichExist(){
        farm.add(animal);
        Animal animal2 = new Animal("cat", 7);
        farm.add(animal2);
        farm.add(animal);
    }



    @Test(expected =IllegalArgumentException.class )
    public void testFarmCapacity(){
        Farm testFarm = new Farm("testF", 2);
        Animal animal1 = new Animal("dog", 5);
        testFarm.add(animal1);
        Animal animal2 = new Animal("hen", 5);
        testFarm.add(animal2);
        Animal animal3 = new Animal("sheep", 5);
        testFarm.add(animal3);
    }

    @Test
    public void testRemoveAnimal(){
        Farm testFarm = new Farm("testF", 10);
        Animal animal1 = new Animal("dog", 5);
        testFarm.add(animal1);
        Animal animal2 = new Animal("hen", 5);
        testFarm.add(animal2);
        Animal animal3 = new Animal("sheep", 5);
        testFarm.add(animal3);

        Assert.assertEquals(3, testFarm.getCount());

        //testFarm.remove("dog");
        Assert.assertTrue(testFarm.remove("dog"));
        Assert.assertFalse(testFarm.remove("cow"));
        Assert.assertEquals(2, testFarm.getCount());

    }

    @Test(expected = IllegalArgumentException.class)
    public void testCapacity(){
        Farm testFarm1 = new Farm("farm1", 10);
        Farm testFarm2 = new Farm("farm2", -1);

    }
    @Test(expected = NullPointerException.class)
    public void testName(){
        Farm testFarm1 = new Farm("farm1", 10);
        Farm testFarm2 = new Farm(null, -1);

    }


}

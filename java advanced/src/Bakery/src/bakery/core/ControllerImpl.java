package bakery.core;

import bakery.core.interfaces.Controller;
import bakery.entities.bakedFoods.interfaces.BakedFood;
import bakery.entities.bakedFoods.interfaces.Bread;
import bakery.entities.bakedFoods.interfaces.Cake;
import bakery.entities.drinks.interfaces.Drink;
import bakery.entities.drinks.interfaces.Tea;
import bakery.entities.drinks.interfaces.Water;
import bakery.entities.tables.interfaces.InsideTable;
import bakery.entities.tables.interfaces.OutsideTable;
import bakery.entities.tables.interfaces.Table;
import bakery.repositories.interfaces.*;

import static bakery.common.ExceptionMessages.*;
import static bakery.common.OutputMessages.*;

public class ControllerImpl implements Controller {


    private FoodRepository<BakedFood> foodRepository;
    private DrinkRepository<Drink> drinkRepository;
    private TableRepository<Table> tableRepository;
    private double totalIncomes;

    public ControllerImpl(FoodRepository<BakedFood> foodRepository, DrinkRepository<Drink> drinkRepository, TableRepository<Table> tableRepository) {
        this.foodRepository = foodRepository;
        this.drinkRepository = drinkRepository;
        this.tableRepository = tableRepository;
        this.totalIncomes = 0.0;
    }


    @Override
    public String addFood(String type, String name, double price) {
        BakedFood food =  foodRepository.getByName(name);
        if (food != null) {
            throw new IllegalArgumentException(String.format(FOOD_OR_DRINK_EXIST, type, name));
        }


        if (type.equals("Bread")) {
            food = new Bread(name, price);
        } else if (type.equals("Cake")) {
            food = new Cake(name, price);
        }



        this.foodRepository.add(food);
        return String.format(FOOD_ADDED, name, type);
    }

    @Override
    public String addDrink(String type, String name, int portion, String brand) {

        Drink drink = drinkRepository.getByNameAndBrand(name, brand);
        if (drink != null) {
            throw new IllegalArgumentException(String.format(FOOD_OR_DRINK_EXIST, type, name));
        }

        if (type.equals("Tea")) {
            drink = new Tea(name, portion, brand);
        } else if (type.equals("Water")) {
            drink = new Water(name, portion, brand);
        }



        this.drinkRepository.add(drink);
        return String.format(DRINK_ADDED, name, brand);
    }

    @Override
    public String addTable(String type, int tableNumber, int capacity) {

        Table table = tableRepository.getByNumber(tableNumber);
        if (table != null) {
            throw new IllegalArgumentException(String.format(TABLE_EXIST, tableNumber));
        }

        if (type.equals("InsideTable")) {
            table = new InsideTable(tableNumber, capacity);
        } else if (type.equals("OutsideTable")) {
            table = new OutsideTable(tableNumber, capacity);
        }



        this.tableRepository.add(table);
        return String.format(TABLE_ADDED, tableNumber);
    }

    @Override
    public String reserveTable(int numberOfPeople) {
        for (Table table : tableRepository.getAll()) {
            if (!table.isReserved()) {
                if (table.getCapacity() >= numberOfPeople) {
                    table.reserve(numberOfPeople);
                    return String.format(TABLE_RESERVED, table.getTableNumber(), numberOfPeople);
                }
            }
        }


        return String.format(RESERVATION_NOT_POSSIBLE, numberOfPeople);
    }

    @Override
    public String orderFood(int tableNumber, String foodName) {
        boolean tableFound = false;
        boolean foodFound = false;

        for (Table table : tableRepository.getAll()) {
            if (table.getTableNumber() == tableNumber) {
                if (table.isReserved()) {
                    tableFound = true;
                    for (BakedFood food : foodRepository.getAll()) {
                        if (food.getName().equals(foodName)) {
                            foodFound = true;
                            table.orderFood(food);
                            return String.format(FOOD_ORDER_SUCCESSFUL, tableNumber, foodName);
                        }
                    }
                }


            }

        }

        if (!tableFound) {
            return String.format(WRONG_TABLE_NUMBER, tableNumber);
        } else {
            return String.format(NONE_EXISTENT_FOOD, foodName);
        }
/*
        Table table = tableRepository.getByNumber(tableNumber);
        if(table == null || !table.isReserved()){
            return String.format(WRONG_TABLE_NUMBER, tableNumber);
        }

        BakedFood food = foodRepository.getByName(foodName);

        if(food == null){
            return String.format(NONE_EXISTENT_FOOD, foodName);
        }

        table.orderFood(food);


        return  String.format(FOOD_ORDER_SUCCESSFUL, tableNumber, foodName);*/
    }

    @Override
    public String orderDrink(int tableNumber, String drinkName, String drinkBrand) {

       boolean tableFound = false;
        boolean drinkFound = false;

        for (Table table : tableRepository.getAll()) {
            if (table.getTableNumber() == tableNumber) {
                tableFound = true;
                for (Drink drink : drinkRepository.getAll()) {
                    if (drink.getName().equals(drinkName) && drink.getBrand().equals(drinkBrand)) {
                        drinkFound = true;
                        table.orderDrink(drink);
                        return String.format(DRINK_ORDER_SUCCESSFUL, tableNumber, drinkName, drinkBrand);
                    }
                }
            }

        }

        if (!tableFound) {
            return String.format(WRONG_TABLE_NUMBER, tableNumber);
        } else {
            return String.format(NON_EXISTENT_DRINK, drinkName, drinkBrand);
        }
/*
        Table table = tableRepository.getByNumber(tableNumber);
        if(table == null || !table.isReserved()){
            return String.format(WRONG_TABLE_NUMBER, tableNumber);
        }

        Drink drink = drinkRepository.getByNameAndBrand(drinkName, drinkBrand);
        if(drink == null){
            String.format(NON_EXISTENT_DRINK, drinkName, drinkBrand);
        }

        table.orderDrink(drink);

        return String.format(DRINK_ORDER_SUCCESSFUL, tableNumber, drinkName, drinkBrand);*/
    }

    @Override
    public String leaveTable(int tableNumber) {
        StringBuilder builder = new StringBuilder();
        for (Table table : tableRepository.getAll()) {
            if (table.getTableNumber() == tableNumber) {
                double currentIncome = table.getBill();
                builder.append(String.format(BILL, tableNumber, currentIncome));
                totalIncomes += currentIncome;
                table.clear();
            }
        }


        return builder.toString();
    }

    @Override
    public String getFreeTablesInfo() {
        StringBuilder builder = new StringBuilder();

        for (Table table : tableRepository.getAll()) {
            if (!table.isReserved()) {
                builder.append(table.getFreeTableInfo()).append(System.lineSeparator());
            }
        }
        return builder.toString().trim();
    }

    @Override
    public String getTotalIncome() {

        return String.format(TOTAL_INCOME, totalIncomes);
    }
}

package day_03_Multidimensional_Arrays;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class IntersectionOfTwoMatrices {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int rows = scan.nextInt();
        int cols = scan.nextInt();
        scan.nextLine();
        char[][] firstMatrix = new char[rows][];
        char[][] secondMatrix = new char[rows][];
        firstMatrix = fillMatrix(rows,cols, scan);
        secondMatrix = fillMatrix(rows, cols, scan);

        compareAndPrintNewMatrix (firstMatrix, secondMatrix, rows, cols);

    }

    public static void compareAndPrintNewMatrix(char[][] firstMatrix, char[][] secondMatrix, int rows, int cols) {
        char[][] editedMatrix = new char[rows][cols];
        for (int row = 0; row < firstMatrix.length; row++) {
            char[] column = firstMatrix[row];
            for (int col = 0; col < column.length; col++) {
                if(firstMatrix[row][col] != secondMatrix[row][col]){
                    editedMatrix[row][col] = '*';
                }else{
                    editedMatrix[row][col] = firstMatrix[row][col];
                }
            }
        }
        printMatrix(editedMatrix);
    }

    public static void printMatrix(char[][] editedMatrix) {
        for (int row = 0; row < editedMatrix.length; row++) {
            char[] column = editedMatrix[row];
            for (int col = 0; col < column.length; col++) {
                System.out.print(editedMatrix[row][col] + " ");

            }
            System.out.println();
        }
    }

    public static char[][] fillMatrix(int rows, int cols, Scanner scan) {
        char [][] matrix = new char[rows][cols];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col <cols ; col++) {
                char symbol = scan.next().charAt(0);
                matrix[row][col] = symbol;
            }
           scan.nextLine();
        }
        return matrix;
    }
}

package day_02_Stack;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.Random;
import java.util.Scanner;

public class Plants {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scan = new Scanner(System.in);
        PrintStream printStream = new PrintStream("output.txt");
        Random digit = new Random();
        for (int i = 1; i <= 160000; i++) {
            int nextDigit = digit.nextInt(100000);
            printStream.print(nextDigit + " ");
        }
        
    }
}

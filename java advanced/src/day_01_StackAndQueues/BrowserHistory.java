package day_01_StackAndQueues;

import java.util.ArrayDeque;
import java.util.Scanner;

public class BrowserHistory {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String command = scan.nextLine();
        ArrayDeque<String> browserHistory = new ArrayDeque<>();
        String currentPage = "";
        while (!command.equals("Home")){
            if(command.equals("back")){

                if(!browserHistory.isEmpty()){
                    currentPage = browserHistory.pop();

                }else{
                    System.out.println("no previous URLs");
                    command = scan.nextLine();
                    continue;
                }

            }else{
                if (!currentPage.equals("")){
                    browserHistory.push(currentPage);
                }

            }
            if(!command.equals("back")){
                currentPage = command;

            }

            System.out.println(currentPage);
            command = scan.nextLine();
        }
    }
}

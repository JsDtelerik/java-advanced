import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String[] command = scan.nextLine().split("\\s+");
        List<Identifiable> identifiables = new ArrayList<>();
        while (!command[0].equals("End")){

            if(command.length == 3){
                identifiables.add(new Citizen(command[0], Integer.parseInt(command[1]), command[2]));
            }else if(command.length == 2){
                identifiables.add(new Robot(command[0], command[1]));
            }

            command = scan.nextLine().split("\\s+");
        }
        String fakeID = scan.nextLine();
        printFakeIds(fakeID, identifiables);
    }

    private static void printFakeIds(String fakeID, List<Identifiable> identifiables) {
        for (Identifiable id : identifiables) {
            if(id.getId().endsWith(fakeID)){
                System.out.println(id.getId());
            }
        }
    }
}

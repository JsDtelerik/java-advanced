package day_6_Sets_and_Maps;

import java.util.LinkedHashSet;
import java.util.Scanner;

public class UniqueUsernames {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
    scan.nextLine();
        LinkedHashSet<String> userNames = new LinkedHashSet<>();
        for (int i = 1; i <= n; i++) {
            String name = scan.nextLine();
            userNames.add(name);
        }


        userNames.forEach(System.out::println);
    }
}

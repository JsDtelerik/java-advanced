package day_02_Stack;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Scanner;

public class BasicStackOperations {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int[] commandNumbers = Arrays.stream(scan.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();

        ArrayDeque<Integer> numbers = pushNumbersInside(scan, commandNumbers[0]);
        numbers = popNumbersFromHere(numbers, commandNumbers[1]);

        if(numbers.isEmpty()){
            System.out.println(0);
        }else if (numbers.contains(commandNumbers[2])){
            System.out.println("true");
        }else{

            System.out.println(printSmallestNumberInsideTheStack(numbers));
        }


    }

    private static int printSmallestNumberInsideTheStack(ArrayDeque<Integer> numbers) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < numbers.size(); i++) {
            int currentNum = numbers.pop();
            if(currentNum<min){
                min = currentNum;
            }
        }
        return min;
    }

    public static ArrayDeque<Integer> popNumbersFromHere(ArrayDeque<Integer> numbers, int n) {
        for (int i = 1; i <= n; i++) {
            numbers.pop();

        }
        return numbers;
    }

    public static ArrayDeque<Integer> pushNumbersInside(Scanner scan, int n) {
       ArrayDeque<Integer> nums = new ArrayDeque<>();
       int[] pushThisNumbers = Arrays.stream(scan.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();
        for (int i = 0; i < n; i++) {
            nums.push(pushThisNumbers[i]);
        }
        return nums;
    }
}

package day_03_Multidimensional_Arrays;

import java.util.Arrays;
import java.util.Scanner;

public class MaximumSumOf2x2Submatrix {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] rowsAndCols = Arrays.stream(scan.nextLine().split(", ")).mapToInt(Integer::parseInt).toArray();
        int rows = rowsAndCols[0];
        int cols = rowsAndCols[1];
        int[][] matrix = fillMatrix(rows, cols, scan);
        int[][] twoByTwoBestSubMatrix = findBestSubMatrix(matrix);
        printMatrix(twoByTwoBestSubMatrix);
        printMatrixMaxSum(twoByTwoBestSubMatrix);

    }

    private static void printMatrixMaxSum(int[][] matrix) {
        int sum = 0;
        for (int row = 0; row < matrix.length; row++) {
            sum += Arrays.stream(matrix[row]).sum();
        }
        System.out.println(sum);
    }

    public static void printMatrix(int[][] matrix) {
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {
                System.out.print(matrix[row][col]+ " ");
            }
            System.out.println();
        }
    }

    public static int[][] findBestSubMatrix(int[][] matrix) {
        int maxSum = 0;
        int bestRow = 0;
        int bestCol = 0;

        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {
                if (row < matrix.length - 1 && col < matrix[row].length - 1) {


                    int sum = matrix[row][col] + matrix[row][col + 1] + matrix[row + 1][col] + matrix[row + 1][col + 1];
                    if (sum > maxSum) {
                        maxSum = sum;
                        bestRow = row;
                        bestCol = col;
                    }
                }
            }
        }
        return new int[][] {{matrix[bestRow][bestCol], matrix[bestRow][bestCol+1]},
                {matrix[bestRow+1][bestCol], matrix[bestRow+1][bestCol+1]}};
    }

    public static int[][] fillMatrix(int rows, int cols, Scanner scan) {
        int[][] matrix = new int[rows][cols];
        for (int row = 0; row < rows; row++) {
            int[] column = Arrays.stream(scan.nextLine().split(", ")).mapToInt(Integer::parseInt).toArray();
            for (int col = 0; col < column.length; col++) {
                matrix[row][col] = column[col];
            }
        }
        return matrix;
    }
}

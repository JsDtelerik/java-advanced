import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.*;
import java.util.stream.Collectors;

public class TerrorEndsQuest {
    public static String heroName = "";
    public static int heroHealth = 1,  heroMana = 1, heroDamage = 1;
    public static int heroRow = 0, heroCol = 0;
    public static LinkedHashMap<String, String> monsterMap = new LinkedHashMap<>();
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int matrixSize = Integer.parseInt(reader.readLine());
        fillHeroInitialStatus(reader);

        monsterMap = fillMonsterMap(reader);
        List<String> directions = Arrays.stream(reader.readLine().split(", ")).collect(Collectors.toList());
        String[][] matrix = fillMatrix(matrixSize, reader);

        for (int i = 0; i < directions.size(); i++) {
            String[] direction = directions.get(i).split("\\s+");

            if (direction[0].equals("Save")){
                break;
            }

            boolean teleport = false;
            if(direction.length>1){
                if(direction[1].equals("teleport") && heroMana>=5){
                    teleport = true;
                    heroMana -=5;
                }
            }
            switch (direction[0]){
                case "up":
                    moveHero(matrix, teleport ? heroRow-3 : heroRow-1 , heroCol);
                    break;
                case "down":
                    moveHero(matrix, teleport ? heroRow+3 : heroRow+1, heroCol);
                    break;
                case "left":
                    moveHero(matrix, heroRow, teleport ? heroCol-3 : heroCol-1);
                    break;
                case "right":
                    moveHero(matrix, heroRow, teleport ? heroCol+3 : heroCol+1);
                    break;
            }

            if(monsterMap.isEmpty() || heroHealth <= 0 || !monsterMap.containsKey("Diablo")){
                break;
            }


        }


        if(monsterMap.size()>0){
            System.out.printf("The following %s still alive in the Chaos Sanctuary %n", monsterMap.size()>2 ? "monsters are" : "monster is");
            monsterMap.entrySet().stream().forEach(e -> System.out.printf(" %s%n",e.getKey()));
            if(!monsterMap.containsKey("Diablo")){
                System.out.printf("Despite that fact you have successfully completed \"Terror`s end\" quest%n");
            }
        }else {

            System.out.printf("Congratulation you have successfully completed \"Terror`s end\" quest%n");
        }

        System.out.printf("%s is at position %d %d, health status %d, mana status %d%n", heroName, heroRow, heroCol, heroHealth, heroMana);
        printMatrix(matrix);
    }

    private static void moveHero(String[][] matrix, int heroRowNew, int heroColNew) {
        int[] verify = verifyNewPosition(heroRowNew, heroColNew, matrix);
        matrix[heroRow][heroCol] = "*";
         if (matrix[verify[0]][verify[1]].equals("R")){

            heroHealth +=50;
            heroMana +=50;

        }else if (matrix[verify[0]][verify[1]].equals("M")){
            printFightResult();
        }
        matrix[verify[0]][verify[1]] = "H";
        heroRow = verify[0];
        heroCol = verify[1];

    }

    private static void printFightResult() {
        String monsterName = "";
        List<String> monsterStats = new ArrayList<>();

        for (Map.Entry<String, String> entry : monsterMap.entrySet()) {
            monsterName = entry.getKey();
            monsterStats = Arrays.stream(entry.getValue().split(", ")).collect(Collectors.toList());
            monsterMap.remove(monsterName);
            break;
        }


        int currentMonsterHealth = Integer.parseInt(monsterStats.get(0));
        int currentMonsterDamage = Integer.parseInt(monsterStats.get(1));
        String possibleItemDrop = monsterStats.size() > 2 ? monsterStats.get(2) : null;

        while (currentMonsterHealth > 0 && heroHealth > 0 ){
            if(heroMana >= heroDamage){
                currentMonsterHealth -= heroDamage;
                heroMana -= heroDamage;
            }else {
                currentMonsterHealth -=1;
            }
            if(currentMonsterHealth > 0){
                heroHealth -= currentMonsterDamage;

            }

        }
        if(heroHealth <= 0){
            heroHealth = 0;
            System.out.printf("Your character %s has been slain by %s%n", heroName, monsterName);


        }else if ( currentMonsterHealth <= 0){
            System.out.printf("Monster %s has been slain by %s%n", monsterName, heroName);
            if(possibleItemDrop != null){
                System.out.printf("Amazing, you found %s, now your character %s is stronger%n", possibleItemDrop, heroName);
            }
        }


    }

    private static int[] verifyNewPosition(int heroRowNew, int heroColNew, String[][] matrix) {
        int[] verification = new int[2];
        if(heroRowNew >= matrix.length){
            verification[0] = matrix.length-1;
        } else if (heroRowNew < 0){
            verification[0] = 0;
        }else{
            verification[0] = heroRowNew;
        }


        if(heroColNew >= matrix.length){
            verification[1] = matrix.length-1;
        }else if (heroColNew < 0){
            verification[1] = 0;
        }else {
            verification[1] = heroColNew;
        }
        return verification;
    }

    private static String[][] fillMatrix(int matrixSize, BufferedReader reader) throws IOException {
        String[][] filledMatrix = new String[matrixSize][matrixSize];
        for (int i = 0; i < matrixSize; i++) {
            String[] line = reader.readLine().split("\\s+");
            for (int j = 0; j < line.length; j++) {
                filledMatrix[i][j] = line[j];
                if(line[j].equals("H")){
                    heroRow = i;
                    heroCol = j;
                }
            }
        }
        return filledMatrix;
    }

    private static LinkedHashMap<String, String> fillMonsterMap(BufferedReader reader) throws IOException {
        LinkedHashMap<String, String> monsters = new LinkedHashMap<>();
        List<String> monstersStats = Arrays.stream(reader.readLine().split("@")).collect(Collectors.toList());

        List<String> innerHelpList = new ArrayList<>();
        while(!monstersStats.isEmpty()){
            innerHelpList = Arrays.asList(monstersStats.get(0).split(" - "));

            monsters.put(innerHelpList.get(0), innerHelpList.get(1));

            monstersStats.remove(0);

        }
        return  monsters;
    }


    private static void fillHeroInitialStatus(BufferedReader reader) throws IOException {
        String[] heroStats = reader.readLine().split(", ");
        heroName = heroStats[0];
        heroHealth = (Integer.parseInt(heroStats[1].split("\\s+")[1])) <=0 ? 1 : (Integer.parseInt(heroStats[1].split("\\s+")[1]));
        heroMana = Integer.parseInt(heroStats[2].split("\\s+")[1]) <=0 ? 1 : (Integer.parseInt(heroStats[2].split("\\s+")[1]));
        heroDamage = (Integer.parseInt(heroStats[3].split("\\s+")[1])) <= 0 ? 1 : (Integer.parseInt(heroStats[3].split("\\s+")[1]));
    }


    public static void printMatrix(String[][] matrix) {
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {
                System.out.print(matrix[row][col]+ " ");
            }
            System.out.println();
        }
    }


}

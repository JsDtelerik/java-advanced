package day_05_Sets_and_Maps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class AcademyGraduation {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader( new InputStreamReader(System.in));
        
        
        int n = Integer.parseInt(reader.readLine());

        TreeMap<String, List<Double>> students = new TreeMap<>();


        for (int i = 0; i < n; i++) {
            String name = reader.readLine();
            List<Double> grades =
                    Arrays.stream(reader.readLine().split("\\s+")).map(Double::parseDouble)
                    .collect(Collectors.toList());

      
            students.put(name, grades);

        }

        students.entrySet().stream().forEach(s->{
            System.out.println(s.getKey() + " is graduated with " + getAverage(s.getValue()));
        });
    }


    private static Double getAverage(List<Double> value) {
        double average = 0.0;
        for (Double aDouble : value) {
            average += aDouble;
        }
        return average / value.size();
    }
}

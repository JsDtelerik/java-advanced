package day_6_Sets_and_Maps;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.TreeMap;

public class LogsAggregator {
    public static int count = 0;
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        TreeMap<String, TreeMap<String, Long>> data = new TreeMap<>();

        int n = Integer.parseInt(reader.readLine());

        for (int i = 0; i < n; i++) {
            String[] command = reader.readLine().split("\\s+");
            if(!data.containsKey(command[1])){
                data.put(command[1], new TreeMap<>());
                data.get(command[1]).put(command[0], Long.parseLong(command[2]));
            }else{
                if(!data.get(command[1]).containsKey(command[0])){
                    data.get(command[1]).put(command[0], Long.parseLong(command[2]));
                }else{
                    data.get(command[1]).put(command[0], data.get(command[1]).get(command[0])+Long.parseLong(command[2]));
                }
            }
        }

         /* data.entrySet().stream()
                .sorted((country1, country2) -> country2.getValue().values().stream().reduce(0L, Long::sum)
                        .compareTo(country1.getValue().values().stream().reduce(0L, Long::sum)))
                .forEach(country -> {
                    System.out.printf("%s (total population: %d)%n", country.getKey(),
                            country.getValue().values().stream().reduce(0L, Long::sum));
                    country.getValue().entrySet().stream()
                            .sorted((city1, city2) -> city2.getValue().compareTo(city1.getValue()))
                            .forEach(city -> System.out.printf("=>%s: %d%n", city.getKey(), city.getValue()));
                });*/


      data.entrySet().stream().forEach(k -> {
          System.out.printf("%s: %d [", k.getKey(), k.getValue().values().stream().reduce(0L, Long::sum) );
          count = k.getValue().size();
          k.getValue().entrySet().stream().forEach(e -> {

              if(count>1){
                  System.out.print(e.getKey() + ", ");
              }else{
                  System.out.println(e.getKey() + "]");
              }
                count--;
          });
      });
    }
}

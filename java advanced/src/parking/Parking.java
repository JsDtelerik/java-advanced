package parking;

import java.util.ArrayList;
import java.util.List;

public class Parking {

    private List<Car> data;
    private String type;
    private int capacity;


    public Parking(String type, int capacity) {
        this.type = type;
        this.capacity = capacity;
        this.data = new ArrayList<>();

    }

    public void add(Car car) {
        if (this.data.size() < this.capacity) {
            this.data.add(car);
        }
    }

    public boolean remove(String manufacturer, String model) {
        boolean removed = false;
        for (Car cars : data) {
            if (cars.getManufacturer().equals(manufacturer) && cars.getModel().equals(model)) {
                data.remove(cars);
                return true;
            }
        }

        return removed;
    }

    public Car getLatestCar() {
        int newestCar = 0;
        Car car = null;
        for (Car cars : data) {
            if (cars.getYear() > newestCar) {
                newestCar = cars.getYear();
                car = cars;
            }
        }

        return car;
    }

    public Car getCar(String manufacturer, String model) {

        for (Car cars : data) {
            if (cars.getManufacturer().equals(manufacturer) && cars.getModel().equals(model)) {
                return cars;
            }
        }
        return null;
    }

    public int getCount(){
        return this.data.size();
    }

//•	getStatistics() – returns a String in the following format:
//o	"The cars are parked in {parking type}:
//{Car1}
//{Car2}

    public String getStatistics(){
        StringBuilder builder = new StringBuilder();

        builder.append("The cars are parked in ").append(this.type).append(":").append(System.lineSeparator());
        for (Car cars : data) {
            builder.append(cars).append(System.lineSeparator());
        }

        return builder.toString();
    }


}

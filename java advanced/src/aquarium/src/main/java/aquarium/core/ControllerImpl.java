package aquarium.core;

import  aquarium.entities.aquariums.Aquarium;
import aquarium.entities.aquariums.FreshwaterAquarium;
import aquarium.entities.aquariums.SaltwaterAquarium;
import aquarium.entities.decorations.Decoration;
import aquarium.entities.decorations.Ornament;
import aquarium.entities.decorations.Plant;
import aquarium.entities.fish.Fish;
import aquarium.entities.fish.FreshwaterFish;
import aquarium.entities.fish.SaltwaterFish;
import aquarium.repositories.DecorationRepository;
import aquarium.repositories.Repository;

import java.util.ArrayList;

import java.util.List;


import static aquarium.common.ConstantMessages.*;
import static aquarium.common.ExceptionMessages.*;

public class ControllerImpl implements Controller{
    //•	decorations - DecorationRepository
    //•	aquariums - collection of Aquarium
  //  private static DecorationRepository decorationRepositoryList;
    private Repository decorations;
    private List<Aquarium> aquariums;


    public ControllerImpl(){
        this.decorations = new DecorationRepository();
        this.aquariums = new ArrayList<>();
    }

    @Override
    public String addAquarium(String aquariumType, String aquariumName) {
        Aquarium aquariumToBeAdded = null;
        if(aquariumType.equals("FreshwaterAquarium")){
        aquariumToBeAdded = new FreshwaterAquarium(aquariumName);
        }else if(aquariumType.equals("SaltwaterAquarium")){
        aquariumToBeAdded = new SaltwaterAquarium(aquariumName);
        }else{
            throw new NullPointerException(INVALID_AQUARIUM_TYPE);
        }

        aquariums.add(aquariumToBeAdded);

        return String.format(SUCCESSFULLY_ADDED_AQUARIUM_TYPE, aquariumType);
    }

    @Override
    public String addDecoration(String type) {


        Decoration decorationToBeAdded = null;
        if(type.equals("Ornament")){
            decorationToBeAdded = new Ornament();
        }else if(type.equals("Plant")){
            decorationToBeAdded = new Plant();
        }else{
            throw new IllegalArgumentException (INVALID_DECORATION_TYPE);
        }

        decorations.add(decorationToBeAdded);

        return String.format(SUCCESSFULLY_ADDED_DECORATION_TYPE, type);
    }

    @Override
    public String insertDecoration(String aquariumName, String decorationType) {
       Decoration keepDecoration =  decorations.findByType(decorationType);
       if(keepDecoration == null){
           throw new IllegalArgumentException (String.format(NO_DECORATION_FOUND, decorationType));
       }


        for (Aquarium aquarium : aquariums) {
            if(aquarium.getName().equals(aquariumName)){
                aquarium.addDecoration(keepDecoration);

            }

        }

        decorations.remove(keepDecoration);

        return String.format(SUCCESSFULLY_ADDED_DECORATION_IN_AQUARIUM, decorationType, aquariumName);
    }

    @Override
    public String addFish(String aquariumName, String fishType, String fishName,
                          String fishSpecies, double price) {
        if(!fishType.equals("FreshwaterFish") && !fishType.equals("SaltwaterFish")){
            throw new IllegalArgumentException(INVALID_FISH_TYPE);
        }
        String fishCheker = fishType.replaceAll("Fish", "");
        boolean hmm = true;
        for (Aquarium aquarium : aquariums) {
            if(aquarium.getName().equals(aquariumName)){
                if(!aquarium.getClass().getSimpleName().contains(fishCheker)){
                    return String.format(WATER_NOT_SUITABLE);
                }

                Fish fish = null;

                if(fishType.equals("FreshwaterFish")){
                    fish = new FreshwaterFish(fishName, fishSpecies, price);
                }else if (fishType.equals("SaltwaterFish")){
                    fish = new SaltwaterFish(fishName, fishSpecies, price);
                }
                aquarium.addFish(fish);
                hmm = false;
            }

        }
        if(hmm){
            return String.format(INVALID_AQUARIUM_TYPE);
        }
        return String.format(SUCCESSFULLY_ADDED_FISH_IN_AQUARIUM, fishType, aquariumName);
    }

    @Override
    public String feedFish(String aquariumName) {
        int fishFed = 0;
        for (Aquarium aquarium : aquariums) {
            if(aquarium.getName().equals(aquariumName)){
                /*for (Fish fish : aquarium.getFish()) {
                    fish.eat();
                    fishFed++;
                }*/
                aquarium.feed();
                fishFed = aquarium.getFish().size();
            }
        }
        return String.format(FISH_FED, fishFed);
    }

    @Override
    public String calculateValue(String aquariumName) {

        Aquarium values = null;
        for (Aquarium aquarium : aquariums) {
            if(aquarium.getName().equals(aquariumName)){
                values = aquarium;

            }

        }
        double value =  values.getFish().stream().mapToDouble(Fish::getPrice).sum()
                    + values.getDecorations().stream().mapToDouble(Decoration::getPrice).sum();



        return String.format(VALUE_AQUARIUM, aquariumName, value);
    }

    @Override
    public String report() {
        StringBuilder builder = new StringBuilder();
        for (Aquarium aquarium : aquariums) {
            builder.append(aquarium.getInfo()).append(System.lineSeparator());
        }


        return builder.toString().trim();
    }
}

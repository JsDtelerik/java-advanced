public enum State {

    INPROGRESS("inProgress"),
    FINISHED("finished");

   private String value;


    State(String value) {
        this.value = value;
    }


    public String getValue() {
        return value;
    }
}

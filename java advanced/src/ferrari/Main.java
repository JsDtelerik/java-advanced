import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String driversName = scan.nextLine();

        Ferrari ferrari = new Ferrari(driversName);
        System.out.println(ferrari.brakes());
        System.out.println(ferrari.gas());
        System.out.println(ferrari.toString());
    }
}

import javax.xml.crypto.dom.DOMCryptoContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));




        String[] carInput = reader.readLine().split("\\s+");

            Vehicle car = new Car("Car", Double.parseDouble(carInput[1]), Double.parseDouble(carInput[2]),
                    Double.parseDouble(carInput[3]));



        String[] truckInput = reader.readLine().split("\\s+");

           Vehicle  truck = new Truck("Truck", Double.parseDouble(truckInput[1]), Double.parseDouble(truckInput[2]),
                    Double.parseDouble(truckInput[3]));


        String[] busInput = reader.readLine().split("\\s+");

            Vehicle  bus = new Bus("Bus", Double.parseDouble(busInput[1]), Double.parseDouble(busInput[2]),
                    Double.parseDouble(busInput[3]));

        int n = Integer.parseInt(reader.readLine());

        for (int i = 0; i < n; i++) {
            String[] input = reader.readLine().split("\\s+");
            try {
                switch (input[1]) {
                    case "Car":
                        driveOrRefuel(input, car);
                        break;
                    case "Truck":
                        driveOrRefuel(input, truck);
                        break;
                    case "Bus":
                        driveOrRefuel(input, bus);
                        break;
                }
            } catch (IllegalArgumentException ex) {
                System.out.println(ex.getMessage());
            }


        }


        System.out.printf("Car: %.2f%n", car.getFuel());
        System.out.printf("Truck: %.2f%n", truck.getFuel());
        System.out.printf("Bus: %.2f", bus.getFuel());

    }

    private static void driveOrRefuel(String[] input, Vehicle vehicle) {

        if (input[0].equals("Drive")) {

            boolean traveled = vehicle.drive(Double.parseDouble(input[2]), "on");
            if (traveled) {
                DecimalFormat df = new DecimalFormat("#.##");
                String formatted = df.format(Double.parseDouble(input[2]));
                System.out.printf("%s travelled %s km%n", vehicle.getType(), formatted);
            } else {
                System.out.printf("%s needs refueling%n", vehicle.getType());
            }
        } else if (input[0].equals("Refuel")) {
            vehicle.refuel(Double.parseDouble(input[2]));
        } else if (input[0].equals("DriveEmpty")) {
            boolean traveled = vehicle.drive(Double.parseDouble(input[2]), "off");
            if (traveled) {
                DecimalFormat df = new DecimalFormat("#.##");
                String formatted = df.format(Double.parseDouble(input[2]));
                System.out.printf("%s travelled %s km%n", vehicle.getType(), formatted);
            } else {
                System.out.printf("%s needs refueling%n", vehicle.getType());
            }

        }
    }
}

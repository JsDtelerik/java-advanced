public class Tiger extends Felime{


    protected Tiger(String animalName, String animalType, Double animalWeight, Integer foodEaten, String livingRegion) {
        super(animalName, animalType, animalWeight, foodEaten, livingRegion);
    }

    @Override
    protected void makeSound() {
        System.out.println("ROAAR!!!");
    }

    @Override
    protected void eat(Food food) {
        if(food.getClass().getSimpleName().equals("Meat")){
            setFoodEaten(this.getFoodEaten()+food.getQuantity());
        }else{
            System.out.println("Tigers are not eating that type of food!");
        }
    }
}

package day_02_Stack;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Scanner;

public class InfixToPostfix {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] input = scan.nextLine().split("\\s+");
        ArrayDeque<String> queue = new ArrayDeque<>();
        ArrayDeque<String> stack = new ArrayDeque<>();


        for (int i = 0; i < input.length; i++) {
            String symbol = input[i];
            if (!"+-/*()".contains(symbol)) {
                queue.offer(symbol);
            } else if ("+-/*".contains(symbol)) {
               if(!stack.isEmpty()){
                   if("+-".contains(symbol) && (stack.peek().equals("-") || stack.peek().equals("+"))){

                       queue.offer(stack.pop());
                   }else if ("/*".contains(symbol) && (stack.peek().equals("/") || stack.peek().equals("*"))){
                       queue.offer(stack.pop());
                   }
               }

                stack.push(symbol);
            } else if (symbol.equals("(")) {
                stack.push(symbol);
            } else if (symbol.equals(")")) {
                while (!stack.peek().equals("(")) {
                    queue.offer(stack.pop());
                }
                stack.pop();
                if(!stack.isEmpty()){
                    if(stack.peek().equals("*") || stack.peek().equals("/")){
                        queue.offer(stack.pop());
                    }
                }

            }

        }
        while (!stack.isEmpty()) {
            queue.offer(stack.pop());
        }
        System.out.println(String.join(" ", queue));

    }


}




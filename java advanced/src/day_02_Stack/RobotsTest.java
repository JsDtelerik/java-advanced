package day_02_Stack;

import java.util.ArrayDeque;
import java.util.Scanner;

public class RobotsTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] input = scanner.nextLine().split(";");
        String[] robots = new String[input.length];
        int[] processTime = new int[input.length];
        int[] worktime = new int[input.length];

        for (int i = 0; i < input.length; i++) {
            String[] data = input[i].split("-");
            String name = data[0];
            int time = Integer.parseInt(data[1]);
            robots[i] = name;
            processTime[i] = time;
        }

        String startTime = scanner.nextLine();

        ArrayDeque<String> products = new ArrayDeque<>();

        String inputProduct = scanner.nextLine();

        while (!inputProduct.equals("End")) {
            products.offer(inputProduct);

            inputProduct = scanner.nextLine();
        }

        String[] timeData = startTime.split(":");
        int hours = Integer.parseInt(timeData[0]);
        int minutes = Integer.parseInt(timeData[1]);
        int seconds = Integer.parseInt(timeData[2]);

        int beginSeconds = hours * 3600 + minutes * 60 + seconds;

        while (!products.isEmpty()) {
            beginSeconds++;

            String product = products.poll();

            boolean isWorking = false;
            for (int i = 0; i < robots.length; i++) {
                if (worktime[i] == 0 && !isWorking) {
                    worktime[i] = processTime[i];
                    isWorking = true;
                    printRobotData(robots[i], product, beginSeconds);
                }
                if (worktime[i] > 0) {
                    worktime[i]--;
                }
            }

            if (!isWorking) { //ako ima svoboden
                products.offer(product);
            }
        }


     //ROB - detail [08:00:01]
        //SS2 - glass [08:00:02]
        //NX8000 - wood [08:00:03]
        //rob40 - apple [08:00:04]
        //tifki - rrrr [08:00:05]
        //ROB - ttttt [08:00:06]
        //rob40 - y [08:00:07]




    }

    private static void printRobotData(String robot, String product, int beginSeconds) {
        long s = beginSeconds % 60;
        long m = (beginSeconds / 60) % 60;
        long h = (beginSeconds / (60 * 60)) % 24;
        System.out.println(robot + " - " + product +
                String.format(" [%02d:%02d:%02d] ", h, m, s));
    }
}
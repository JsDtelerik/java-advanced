package glacialExpedition.models.explorers;

public class AnimalExplorer extends BaseExplorer{



    private static final double INITIAL_ENERGY = 100;
    public AnimalExplorer(String name) {
        super(name, INITIAL_ENERGY);
    }

    @Override
    public void search() {
        if(getEnergy() >= 15){
            setEnergy(getEnergy()-15);
        }else {
            setEnergy(0);
        }

    }
    @Override
    public boolean canSearchReal() {
        if(this.getEnergy() >=15 ){
            return true;
        }
        return false;
    }

    @Override
    public boolean canSearchSet() {
        if(this.getEnergy() >= 15){
            return true;
        }else{
            setEnergy(0);
            return false;
        }

    }

}

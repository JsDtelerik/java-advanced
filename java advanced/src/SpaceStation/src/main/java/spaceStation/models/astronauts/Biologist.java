package spaceStation.models.astronauts;

public class Biologist extends BaseAstronaut{

    private final double INITIAL_OXYGEN = 70;

    protected Biologist(String name) {
        super(name);
        setOxygen(INITIAL_OXYGEN);

    }

    @Override
    public void breath() {
        double decrease = INITIAL_OXYGEN - 5;
        setOxygen(decrease);
    }


}

package day_05_Sets_and_Maps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashSet;

public class ParkingLot {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        LinkedHashSet<String> parking = new LinkedHashSet<>();

        String[] command = reader.readLine().split(", ");

        while (!command[0].equals("END")){
            switch (command[0]) {
                case "IN": parking.add(command[1]); break;
                case "OUT": parking.remove(command[1]); break;
            }

            command = reader.readLine().split(", ");
        }


        if(parking.isEmpty()){
            System.out.println("Parking Lot is Empty");
        }else{
            parking.forEach(System.out::println);
        }
    }
}

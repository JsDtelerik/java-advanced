package day_03_Multidimensional_Arrays;

import java.util.Scanner;

public class PrintDiagonalsOfSquareMatrix {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int rowsAndCols = Integer.parseInt(scan.nextLine());
        String[][] matrix = fillMatrix(rowsAndCols, scan);
        printDiagonals (matrix);
    }

    public static void printDiagonals(String[][] matrix) {
        for (int row = 0; row < matrix.length; row++) {
            System.out.print(matrix[row][row] + " ");
        }
        System.out.println();
        int index = 0;
        for (int row1 = matrix.length-1; row1 >= 0 ; row1--) {
            System.out.print(matrix[row1][index++] + " ");
        }
    }

    public static String[][] fillMatrix(int n, Scanner scan) {
        String[][] matrix = new String[n][n];

        for (int row = 0; row < matrix.length; row++) {
            String[] elements = scan.nextLine().split("\\s+");
            for (int col = 0; col < elements.length; col++) {
                matrix[row][col] = elements[col];
            }

        }
        return matrix;
    }
}

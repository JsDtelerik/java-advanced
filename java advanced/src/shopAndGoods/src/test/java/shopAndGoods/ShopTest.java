package shopAndGoods;


import org.junit.Assert;
import org.junit.Test;


import javax.naming.OperationNotSupportedException;
import java.util.LinkedHashMap;
import java.util.Map;

public class ShopTest {
    @Test
    public void testCreateNewGood() {
        Goods newGood = new Goods("waffle", "1234");

        Assert.assertEquals("waffle", newGood.getName());
        Assert.assertEquals("1234", newGood.getGoodsCode());

    }

    @Test
    public void testreateShopShelves() {
        Shop shop = new Shop();


        Map<String, Goods> original = shop.getShelves();
        int index = 1;
        for (String s : original.keySet()) {
            Assert.assertEquals("Shelves" + index++, s);
        }

    }
    @Test(expected = IllegalArgumentException.class )
    public void testAddGoodsDoesntExist() throws OperationNotSupportedException {
        Shop shop = new Shop();

       Goods goods = new Goods("Waffle", "1234");
       shop.addGoods("Shelves1", goods);
        Goods goods1 = new Goods("Waffl1e", "12314");

       String test = shop.addGoods("Shelves1", goods1);


    }

    @Test(expected = IllegalArgumentException.class )
    public void testAddGoodsDoesntExist2() throws OperationNotSupportedException {
        Shop shop = new Shop();

        Goods goods = new Goods("Waffle", "1234");
        shop.addGoods("Shelves111", goods);
    }

    @Test(expected = IllegalArgumentException.class )
    public void testItemExixst() throws OperationNotSupportedException {
        Shop shop = new Shop();

        Goods goods = new Goods("Waffle", "1234");
        shop.addGoods("Shelves1", goods);
        shop.addGoods("Shelves1", goods);
    }

    @Test
    public void testNormaladding() throws OperationNotSupportedException {
        Shop shop = new Shop();

        Goods goods = new Goods("Waffle", "1234");
        shop.addGoods("Shelves1", goods);
        Assert.assertTrue(shop.getShelves().values().contains(goods));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemoveGoods() throws OperationNotSupportedException {
        Shop shop = new Shop();

        Goods goods = new Goods("Waffle", "1234");

        shop.addGoods("Shelves1", goods);

        shop.removeGoods("Shelves15", goods);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemoveGoodsNotEntered() throws OperationNotSupportedException {
        Shop shop = new Shop();

        Goods goods = new Goods("Waffle", "1234");
        Goods goods1 = new Goods("Waffl1e", "111234");
        shop.addGoods("Shelves1", goods);
        shop.removeGoods("Shelves1", goods1);

    }

    @Test
    public void testRemoveShelf() throws OperationNotSupportedException {
        Shop shop = new Shop();

        Goods goods = new Goods("Waffle", "1234");
        shop.addGoods("Shelves1", goods);
        shop.removeGoods("Shelves1", goods);
        Assert.assertEquals(null, shop.getShelves().get("Shelves1"));

    }
@Test
    public void testGetName(){
        Goods good = new Goods("Waffle", "1234");
        Assert.assertEquals("Waffle", good.getName());
        Assert.assertEquals("1234", good.getGoodsCode());
}


    @Test(expected = OperationNotSupportedException.class )
    public void testItemExixst4() throws OperationNotSupportedException {
        Shop shop = new Shop();
        Goods good = new Goods("Waffle", "1234");
        Goods good1 = new Goods("Waffle", "1234");
        shop.addGoods("Shelves1", good);
        boolean itemExist = shop.getShelves().containsValue(good);

        if (itemExist) {
            throw new OperationNotSupportedException("Goods is already in shelf!");
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testItemExixst5() throws OperationNotSupportedException {
        Shop shop = new Shop();
        Goods good = new Goods("Waffle", "1234");
        Goods good1 = new Goods("Waffle", "1234");
        shop.addGoods("Shelves1", good);
        //Assert.assertTrue( shop.getShelves().containsValue(good));
        shop.addGoods("Shelve2", good);



    }
    @Test(expected = OperationNotSupportedException.class)
    public void testItemExixst6() throws OperationNotSupportedException {
        Shop shop = new Shop();
        Goods good = new Goods("Waffle", "1234");
        Goods good1 = new Goods("Waffle", "1234");
        shop.addGoods("Shelves1", good);
        //Assert.assertTrue( shop.getShelves().containsValue(good1));
        shop.addGoods("Shelves2", good);

    }
    @Test
    public void testItemExixst7() throws OperationNotSupportedException {
        Shop shop = new Shop();
        Goods good = new Goods("Waffle", "1234");
        shop.addGoods("Shelves1", good);


        Assert.assertFalse(shop.getShelves().containsValue(good));

    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddGoodSIllegal() throws OperationNotSupportedException {
        Shop shop = new Shop();
        Goods good = new Goods("Waffle", "1234");
       Goods good1 = new Goods("banana", "882");
        shop.addGoods("Shelves1", good);
        shop.addGoods("Shelves1", good1);

    }
    @Test(expected = IllegalArgumentException.class)
    public void testAddGoodsShelfNull() throws OperationNotSupportedException {
        Shop shop = new Shop();
        Goods good = new Goods("Waffle", "1234");
        Goods good1 = new Goods("banana", "882");
        shop.addGoods(null, good);

    }
    @Test
    public void testRemove() throws OperationNotSupportedException {
        Shop shop = new Shop();
        Goods good = new Goods("Waffle", "1234");
        Goods good1 = new Goods("banana", "882");
        shop.addGoods("Shelves1", good);
        Assert.assertEquals("Goods: 1234 is removed successfully!", shop.removeGoods("Shelves1", good));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemoveGoodsNotExistingShelf() throws OperationNotSupportedException {
        Shop shop = new Shop();
        Goods good = new Goods("Waffle", "1234");
        Goods good1 = new Goods("banana", "882");
        shop.addGoods("Shelves1", good);
        shop.removeGoods("none", good);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testRemoveGoodsNoneExistingGood() throws OperationNotSupportedException {
        Shop shop = new Shop();
        Goods good = new Goods("Waffle", "1234");
        Goods good1 = new Goods("banana", "882");
        shop.addGoods("Shelves1", good);
        shop.removeGoods("Shelves1", good1);
    }

}
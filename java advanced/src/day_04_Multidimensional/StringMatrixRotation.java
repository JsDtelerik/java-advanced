package day_04_Multidimensional;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StringMatrixRotation {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] command = scan.nextLine().split("\\(");

        int keepLastRotationCommand = 0;
        if (command[0].equals("Rotate")) {
            String[] commandLiner = command[1].split("\\)");
            keepLastRotationCommand = Integer.parseInt(commandLiner[0]);
        }
        String input = scan.nextLine();
        List<String> lines = new ArrayList<>();
        while (!input.equals("END")){
            lines.add(input);
            input = scan.nextLine();
        }

        char[][] matrix = fillMatrix(lines);
        int n = (keepLastRotationCommand / 90) % 4 ;
        boolean isUpdated = false;
        for (int i = 1; i <= n; i++) {
            switch (i){
                case 1:
                    matrix = rotateMatrix90(matrix);
                    break;
                case 2:
                    matrix = rotateMatrix180(matrix);
                    break;
                case 3:
                    matrix = rotateMatrix180(matrix);
                  break;
               /*case 4:
                  matrix = rotateMatrix180(matrix);
                  break;*/
            }


        }
        printMatrix(matrix);
    }

    /*private static char[][] rotateMatrix270(char[][] oldMatrix) {
        int colSize = getOldMatrixLongestElement(oldMatrix);
        char[][] matrix = new char[colSize][oldMatrix.length];
        int index = oldMatrix.length-1;
        for (int row = matrix.length-1; row >=0 ; row--) {

            for (int col = 0; col <matrix[row].length ; col++) {

                matrix[row][index--]= oldMatrix[col][row];


            }
            index = oldMatrix.length-1;
        }

        return matrix;
    }*/

    public static char[][] rotateMatrix180(char[][] oldMatrix) {
        int colSize = getOldMatrixLongestElement(oldMatrix);
        char[][] matrix = new char[colSize][oldMatrix.length];
        int index = oldMatrix.length-1;
        for (int row = matrix.length-1; row >=0 ; row--) {

            for (int col = 0; col <matrix[row].length ; col++) {

                matrix[row][index--]= oldMatrix[col][row];


            }
            index = oldMatrix.length-1;
        }

        return matrix;
    }

    private static void printMatrix(char[][] matrix) {
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {
                if (matrix[row][col] != 0) {
                    System.out.print(matrix[row][col]);
                }else{
                    System.out.print(" ");
                }

            }
            System.out.println();
        }
    }

    private static char[][] rotateMatrix90(char[][] oldMatrix) {
        int colSize = getOldMatrixLongestElement(oldMatrix);
        char[][] matrix = new char[colSize][oldMatrix.length];
        int index = 0;
        for (int row = oldMatrix.length-1; row >=0  ; row--) {

            for (int col = 0; col < oldMatrix[row].length; col++) {

                    matrix[col][index]= oldMatrix[row][col];


            }
            index++;
        }

        return matrix;
    }

    private static int getOldMatrixLongestElement(char[][] oldMatrix) {
        int size = 0;
        for (int r = 0; r < oldMatrix.length; r++) {

                if(oldMatrix[r].length > size){
                    size = oldMatrix[r].length;
                }

        }
        return size;
    }

    private static char[][] fillMatrix(List<String> lines) {
        char[][] matrix = new char[lines.size()][];
        for (int row = 0; row < matrix.length; row++) {

            matrix[row] = lines.get(row).toCharArray();
        }
        return  matrix;
    }

}

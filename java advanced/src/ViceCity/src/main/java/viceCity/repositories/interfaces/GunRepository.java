package viceCity.repositories.interfaces;

import viceCity.models.guns.Gun;

import java.util.ArrayList;
import java.util.Collection;


public abstract class GunRepository implements Repository<Gun>{

    private Collection<Gun> models ;

    public GunRepository() {

        this.models = new ArrayList<>();
    }


    @Override
    public Collection getModels() {

        return this.models;
    }

    @Override
    public void add(Gun model) {
        if(!models.contains(model)){
            return;
        }
        this.models.add(model);
    }

    @Override
    public boolean remove(Gun model) {
        return this.models.remove(model);
    }

    @Override
    public Gun find(String name) {
       Gun correctModel = null;
        for (Gun model : models) {
            if(model.getName().equals(name)){
                correctModel = model;
                break;
            }

        }

        return correctModel;
    }

   /*@Override
    public void add(Object model) {
       if(!models.contains((Gun) model)){
           return;
       }
        this.models.add((Gun) model);
    }

    @Override
    public boolean remove(Object model) {
        return models.remove((Gun) model);
    }

    @Override
    public Object find(String name) {


        return models.stream().map(g ->g.getName().equals(name)).findFirst().orElse(null);
    }*/
}

package groomingSalon;

import java.util.ArrayList;
import java.util.List;

public class GroomingSalon {
    private List<Pet> data;
    private int capacity;

    public GroomingSalon(int capacity) {
        this.capacity = capacity;
        this.data = new ArrayList<>();
    }

    //•	Method add(Pet pet) – adds an entity
    // to the data if there is an empty place in the grooming salon for the pet.

    public void add(Pet pet){
        if( this.data.size()<capacity){
            this.data.add(pet);
        }
    }

    //remove(String name)
    public boolean remove(String name){
        boolean removed = false;

        for (Pet pet : data) {
            if(pet.getName().equals(name)){
                this.data.remove(pet);
                return true;
            }

        }
        return removed;
    }

    //getPet(String name, String owner) –
    // returns the pet with the given name and owner or null if no such pet exists.

    public Pet getPet(String name, String owner){
        Pet pets = null;
        for (Pet pet : data) {
            if(pet.getName().equals(name) && pet.getOwner().equals(owner)){
               pets = pet;
               break;
            }
        }
        return pets;
    }

    public int getCount(){
        return this.data.size();
    }

    //•	getStatistics() – returns a String in the following format:
    //o	" The grooming salon has the following clients:
    //{name} {owner}
    //{name} {owner}

    public String getStatistics(){
        StringBuilder builder = new StringBuilder();
        builder.append("The grooming salon has the following clients:").append(System.lineSeparator());
        for (Pet pet : data) {
            builder.append(pet.getName()).append(" ").append(pet.getOwner()).append(System.lineSeparator());

        }
        return builder.toString();
    }

}

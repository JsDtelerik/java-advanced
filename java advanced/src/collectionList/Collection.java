import java.util.List;

public abstract class Collection {
    //maxSize: int = 100
    //items: List<String>
    //Collection()
    private final int MAX_SIZE = 100;
    private List<String> listCollection;

    protected Collection(List<String> listCollection) {

       this.setListCollection(listCollection);
    }

    public void setListCollection(List<String> listCollection) {
        if(listCollection.size() > MAX_SIZE){
            throw new IllegalArgumentException();
        }
        this.listCollection = listCollection;
    }


    //  private void setListCollection(List<String> listCollection) {
  //      this.listCollection = listCollection;
  // }


}

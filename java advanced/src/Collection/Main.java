import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
    /*    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] input  = reader.readLine().split("\\s+");
        int operations = Integer.parseInt(reader.readLine());
        //AddCollection, AddRemoveCollection and MyListImpl.

        AddCollection addCollection =  new AddCollection();


        AddRemoveCollection addRemoveCollection = new AddRemoveCollection();

        MyListImpl myList = new MyListImpl();
        //System.out.println(myList.add("carrot"));
        List<Collection> collectionList = new ArrayList<>();
       // System.out.println(myList.getUsed());

        collectionList.add(addCollection);
        collectionList.add(addRemoveCollection);
        collectionList.add(myList);
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < 3; i++) {
            for (int inputIn = 0; inputIn < input.length; inputIn++) {
                if(inputIn < input.length-1){
                    builder.append(collectionList.get(i).add(input[inputIn])).append(" ");
                } else{
                    builder.append(collectionList.get(i).add(input[inputIn]));
                }

            }

                builder.append(System.lineSeparator());

        }

      //  System.out.println(collectionList.get(2).getUsed());


        for (int i = 1; i < collectionList.size(); i++) {
            for (int index = 0; index < operations; index++) {
                if(index < input.length-1){
                    builder.append(collectionList.get(i).remove()).append(" ");
                } else{
                    builder.append(collectionList.get(i).remove());
                }

            }
            if(i<2){
                builder.append(System.lineSeparator());
            }

        }
        System.out.println(builder);

        }






}*/
        AddCollection addCollection = new AddCollection();
        AddRemoveCollection addRemoveCollection = new AddRemoveCollection();
        MyListImpl myList = new MyListImpl();

        Scanner scan = new Scanner(System.in);

        String[] tokens = scan.nextLine().split("\\s+");
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        StringBuilder sb3 = new StringBuilder();

        for (String item : tokens) {
            int index1 = addCollection.add(item);
            sb1.append(index1).append(" ");
            int index2 = addRemoveCollection.add(item);
            sb2.append(index2).append(" ");
            int index3 = myList.add(item);
            sb3.append(index3).append(" ");
        }

        int amountOperation = Integer.parseInt(scan.nextLine());

        StringBuilder sb4 = new StringBuilder();
        StringBuilder sb5 = new StringBuilder();

        while (amountOperation-- > 0) {
            String removeItem1 = addRemoveCollection.remove();
            sb4.append(removeItem1).append(" ");

            String removeItem2 = myList.remove();
            sb5.append(removeItem2).append(" ");
        }
        System.out.println(sb1.toString().trim());
        System.out.println(sb2.toString().trim());
        System.out.println(sb3.toString().trim());
        System.out.println(sb4.toString().trim());
        System.out.println(sb5.toString().trim());
    }
}
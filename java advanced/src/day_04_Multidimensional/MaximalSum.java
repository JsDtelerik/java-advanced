package day_04_Multidimensional;

import java.util.Scanner;

public class MaximalSum {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int rows = scan.nextInt();
        int cols = scan.nextInt();
        int[][] matrix = fillMatrix(rows, cols, scan);
        int[][] subMatrix = findBestSubMatrix(matrix);

        printMatrix(subMatrix);
    }

    public static int[][] findBestSubMatrix(int[][] matrix) {
        int maxSum = 0;
        int bestRow = 0;
        int bestCol = 0;

        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {
                if (row < matrix.length - 2 && col < matrix[row].length - 2) {


                    int sum = matrix[row][col] + matrix[row][col + 1] + matrix[row][col + 2]
                            + matrix[row + 1][col] + matrix[row + 1][col + 1] + matrix[row + 1][col + 2]
                            + matrix[row + 2][col] + matrix[row + 2][col + 1] + matrix[row + 2][col + 2];
                    if (sum > maxSum) {
                        maxSum = sum;
                        bestRow = row;
                        bestCol = col;
                    }
                }
            }
        }
        System.out.printf("Sum = %d%n", maxSum);
        return new int[][]{{matrix[bestRow][bestCol], matrix[bestRow][bestCol + 1], matrix[bestRow][bestCol + 2]},
                {matrix[bestRow + 1][bestCol], matrix[bestRow + 1][bestCol + 1], matrix[bestRow + 1][bestCol + 2]},
                {matrix[bestRow + 2][bestCol], matrix[bestRow + 2][bestCol + 1], matrix[bestRow + 2][bestCol + 2]}};
    }

    private static int[][] fillMatrix(int rows, int cols, Scanner scan) {
        int[][] matrix = new int[rows][cols];
        scan.nextLine();
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                matrix[row][col] = scan.nextInt();
            }
            scan.nextLine();
        }
        return matrix;
    }

    public static void printMatrix(int[][] matrix) {
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {
                System.out.print(matrix[row][col] + " ");
            }
            System.out.println();
        }
    }

}

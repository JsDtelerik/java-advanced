package day_02_Stack;

import java.sql.Array;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class MaximumElement {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = Integer.parseInt(scan.nextLine());

        ArrayDeque<Integer> stack = new ArrayDeque<>();
        for (int i = 1; i <= n; i++) {
            int[] command = Arrays.stream(scan.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();

            switch (command[0]){
                case 1:
                    stack.push(command[1]);
                    break;
                case 2:
                    stack.pop();
                    break;
                case 3:
                    if(stack.isEmpty()){
                        System.out.println(0);
                    }else{
                        System.out.println(Collections.max(stack));
                    }


                   // System.out.println(printMax(stack));
                    break;
            }


        }
    }

    private static int printMax(ArrayDeque<Integer> stack) {
        int max = Integer.MIN_VALUE;
      while(!stack.isEmpty()) {
            int currentValue = stack.pop();
            if (currentValue>max){
                max = currentValue;
            }
        }
        return max;
    }
}

package day_01_StackAndQueues;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Scanner;

public class HotPotato {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] names = scan.nextLine().split("\\s+");
        int rotations = Integer.parseInt(scan.nextLine());
        ArrayDeque<String> players = new ArrayDeque<>();
        for (int i = 0; i < names.length; i++) {
            players.offer(names[i]);
        }

        for (int i = 1; i <= rotations; i++) {
            if(i == rotations){
                if(players.size()>1){
                    System.out.println("Removed " + players.poll());
                    i = 0;
                }else if (players.size() == 1){
                    System.out.println("Last is " + players.poll());
                }
            }else{
                String movePlayerAtTheEnd = players.poll();
                players.offer(movePlayerAtTheEnd);
            }
        }
    }
}

import java.util.List;

public class AddRemoveCollection extends Collection implements AddRemovable {


    protected AddRemoveCollection(List<String> listCollection) {
        super(listCollection);
    }

    @Override
    public String remove() {
        return null;
    }

    @Override
    public int add(String element) {
        return 0;
    }
}

public enum  AirConditioner {
    ON("on"),
    OFF("off");


    private String value;

    AirConditioner(String value) {

        this.value = value;
    }

    public String getValue(){

        return value;
    }

}



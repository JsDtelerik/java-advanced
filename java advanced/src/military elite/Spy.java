public class Spy extends Soldier implements SpyImpl{


//Spy <id> <firstName> <lastName> <codeNumber>
   // private int id;
    //private String firstName;
    //private String lastName;
    private String codeNumber;

    public Spy(int id, String firstName, String lastName, String codeNumber) {
        super(id, firstName, lastName);
        this.setCodeNumber(codeNumber);
    }


    public void setCodeNumber(String codeNumber) {
        for (int i = 0; i < codeNumber.length(); i++) {
            if(!Character.isDigit(codeNumber.charAt(i))){
                throw new IllegalArgumentException();
            }
        }
     this.codeNumber = codeNumber;
    }

    @Override
    public String codeNumber() {
        return this.codeNumber;
    }


    //Name: James Bond Id: 7
    //Code Number: 007
    @Override
    public String toString() {
       StringBuilder builder = new StringBuilder();
       builder.append("Name: ").append(getFirstName()).append(" ").append(getLastName()).append(" Id: ").append(getId())
               .append(System.lineSeparator())
               .append("Code Number: ").append(codeNumber());

        return builder.toString();
    }

   /* @Override
    public boolean validation(String name) {

        if(name.length()<2){
            return false;
        }
        if(name.charAt(0)<65 && name.charAt(0)>90){
            return false;
        }
        for (int i = 0; i < name.length(); i++) {
            if(!Character.isAlphabetic(name.charAt(i))){
                return false;
            }

        }
        return true;
    } */
}
